### step by step clone apps
1. git clone git@gitlab.com:sudewo/boilerplate-apps.git yourapp
2. rm -rf .git
3. update package.json title package name etc.
4. yarn && cd api && yarn
5. cp .env.example to .env
6. adonis key:generate
7. mkdir resources/locales
8. adonis migration:run
9. adonis seed --files UserSeeder.js
10. cd .. && yarn dev
11. login user : admin@gmail.com, pass: admin
