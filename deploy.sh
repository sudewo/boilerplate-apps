RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
RESET=`tput sgr0`



read -p "Enter action (push|reload|deploy|push-deploy|codepush) : " ACTION
# echo "${YELLOW} action : $ACTION $RESET"

if [ $ACTION ]; then
  if [ $ACTION != 'reload' ] && [ $ACTION != 'deploy' ]; then
    read -p "Enter commit message : " COMMIT_MESSAGE
    echo "${YELLOW} commit message : $COMMIT_MESSAGE $RESET"
  fi
fi

PUSH="quasar build; git add .; git commit -am '$COMMIT_MESSAGE'; git push;"
DEPLOY="ssh root@167.99.64.70 \"cd ~/dealer-program; git pull; pm2 reload all --update-env\";"
RELOAD="ssh root@167.99.64.70 \"pm2 reload all --update-env\";"
PUSH_DEPLOY="$PUSH $DEPLOY"
CODE_PUSH="quasar build -m android --skip-pkg && cd src-cordova && code-push release-cordova DealerProgram-Android android -d Production --description \"$COMMIT_MESSAGE\" --mandatory true"

if [ "$ACTION" == "push" ]; then
  echo "${GREEN} -- PUSH -- ${RESET}"
  eval ${PUSH}
elif [ "$ACTION" == "deploy" ]; then
  echo "${GREEN} -- DEPLOY -- ${RESET}"
  eval ${DEPLOY}
  echo "deploy to 167.99.64.70"
elif [ "$ACTION" == "reload" ]; then
  echo "${GREEN} -- RELOAD SERVER -- ${RESET}"
  eval ${RELOAD}
elif [ "$ACTION" == "push-deploy" ]; then
  echo "${GREEN} -- PUSH AND DEPLOY -- ${RESET}"
  eval ${PUSH_DEPLOY}
  echo "deploy to 167.99.64.70"
elif [ "$ACTION" == "codepush" ]; then
  echo "${GREEN} -- UPDATE CODEPUSH -- ${RESET}"
  eval ${CODE_PUSH}
  echo "deploy codepush"
else
  echo "${RED} -- ACTION IS REQUIRED -- ${RESET}"
fi
