'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
// eslint-disable-next-line no-undef
const Route = use('Route')
const GraphQLServer = use('GraphQLServer')

Route.get('/', () => {
  return { greeting: 'buatin boilerplate' }
})
Route.post('/auth/test', 'AuthController.test').middleware(['auth'])
Route.get('/auth/testget', 'AuthController.testget').middleware(['auth'])
Route.post('/auth/login', 'AuthController.login')
Route.post(
  '/auth/login-with-social-media',
  'AuthController.loginWithSocialMedia'
)
Route.post('/auth/complete-account', 'AuthController.completeAccount')
Route.post('/auth/logout', 'AuthController.logout')
Route.get('/auth/email', 'AuthController.email').middleware(['auth'])
Route.post('/auth/email-verification', 'AuthController.emailVerification')
Route.post('/auth/reset-password', 'AuthController.resetPassword')
Route.post('/auth/code-verification', 'AuthController.codeVerification')
Route.post('/auth/change-avatar', 'AuthController.changeAvatar').middleware([
  'auth'
])
Route.post('/auth/update-profile', 'AuthController.updateProfile').middleware([
  'auth'
])

Route.post('/location/countries', 'LocationController.countries').middleware([
  'auth'
])
Route.post('/location/states', 'LocationController.states').middleware([
  'auth'
])
Route.post('/location/cities', 'LocationController.cities').middleware([
  'auth'
])

// Route.resource('user', 'UserController')

Route.post('/graphql', context => {
  return GraphQLServer.handle(context)
})

Route.get('/graphiql', context => {
  return GraphQLServer.handleUI(context)
})
