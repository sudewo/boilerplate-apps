const Route = use('Route')
const GraphQLServer = use('GraphQLServer')
Route.post('/graphql', context => GraphQLServer.handle(context))
Route.get('/graphiql', context => GraphQLServer.handleUI(context))
