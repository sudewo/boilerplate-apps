const Route = use('Route')
Route.group('admin', () => {
  Route.get('/', 'DashboardController.index').as('admin')
  Route.post('program/daftar', 'ProgramController.daftar')
  Route.post('program/upgrade', 'ProgramController.upgrade')
  Route.post('program-report/update-report', 'ProgramReportController.updateReport')
  Route.post('notification/count', 'NotificationController.notificationCount')

  Route.resource('user', 'UserController').validator(
    new Map([
      [['admin.user.store'], ['StoreUser']],
      [['admin.user.update'], ['UpdateUser']]
    ])
  )

  Route.resource('token', 'TokenController').validator(
    new Map([
      [['admin.token.store'], ['StoreToken']],
      [['admin.token.update'], ['UpdateToken']]
    ])
  )

  Route.resource('program', 'ProgramController').validator(
    new Map([
      [['admin.program.store'], ['StoreProgram']],
      [['admin.program.update'], ['UpdateProgram']]
    ])
  )

  Route.resource('program-order', 'ProgramOrderController').validator(
    new Map([
      [['admin.program-order.store'], ['StoreProgramOrder']],
      [['admin.program-order.update'], ['UpdateProgramOrder']]
    ])
  )

  Route.resource('notification', 'NotificationController').validator(
    new Map([
      [['admin.notification.store'], ['StoreNotification']],
      [['admin.notification.update'], ['UpdateNotification']]
    ])
  )

  Route.resource('branch', 'BranchController').validator(
    new Map([
      [['admin.branch.store'], ['StoreBranch']],
      [['admin.branch.update'], ['UpdateBranch']]
    ])
  )

  Route.resource('area', 'AreaController').validator(
    new Map([
      [['admin.area.store'], ['StoreArea']],
      [['admin.area.update'], ['UpdateArea']]
    ])
  )

  Route.resource('program-report', 'ProgramReportController').validator(
    new Map([
      [['admin.program-report.store'], ['StoreProgramReport']],
      [['admin.program-report.update'], ['UpdateProgramReport']]
    ])
  )

  Route.resource('program-package', 'ProgramPackageController').validator(
    new Map([
      [['admin.program-package.store'], ['StoreProgramPackage']],
      [['admin.program-package.update'], ['UpdateProgramPackage']]
    ])
  )
})
  .prefix('admin')
  .namespace('App/Controllers/Http/Admin/')
  .middleware(['auth:session,jwt', 'authorization'])
