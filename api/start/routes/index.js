'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
// eslint-disable-next-line no-undef
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'buatin boilerplate' }
})

Route.post('/location/countries', 'LocationController.countries').middleware(['auth'])
Route.post('/location/states', 'LocationController.states').middleware(['auth'])
Route.post('/location/cities', 'LocationController.cities').middleware(['auth'])

// Route.resource('user', 'UserController')

Route.get('/olx', 'OlxController.index')
Route.get('/scrap-all', 'OlxController.scrapAll')
Route.get('/get', 'OlxController.get')
Route.get('/get-brand', 'OlxController.getBrand')
Route.get('/get-model', 'OlxController.getModel')
Route.get('/get-type', 'OlxController.getType')
Route.get('/get-year', 'OlxController.getYear')
Route.get('/get-transmission', 'OlxController.getTransmission')
Route.get('/get-table', 'OlxController.getTable')
Route.delete('/olx/destroy/:id', 'OlxController.destroy')

require('./auth')
require('./admin')
require('./api')
