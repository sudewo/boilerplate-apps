const Route = use('Route')
Route.group('api', () => {
  Route.get('/', () => ['helo world'])

  Route.resource('user', 'UserController').validator(
    new Map([
      [['api.user.store'], ['StoreUser']],
      [['api.user.update'], ['UpdateUser']]
    ])
  ).apiOnly()

  Route.resource('token', 'TokenController').validator(
    new Map([
      [['api.token.store'], ['StoreToken']],
      [['api.token.update'], ['UpdateToken']]
    ])
  ).apiOnly()

  Route.resource('program', 'ProgramController').validator(
    new Map([
      [['api.program.store'], ['StoreProgram']],
      [['api.program.update'], ['UpdateProgram']]
    ])
  ).apiOnly()

  Route.resource('program-order', 'ProgramOrderController').validator(
    new Map([
      [['api.program-order.store'], ['StoreProgramOrder']],
      [['api.program-order.update'], ['UpdateProgramOrder']]
    ])
  ).apiOnly()

  Route.resource('notification', 'NotificationController').validator(
    new Map([
      [['api.notification.store'], ['StoreNotification']],
      [['api.notification.update'], ['UpdateNotification']]
    ])
  ).apiOnly()

  Route.resource('branch', 'BranchController').validator(
    new Map([
      [['api.branch.store'], ['StoreBranch']],
      [['api.branch.update'], ['UpdateBranch']]
    ])
  ).apiOnly()

  Route.resource('program-report', 'ProgramReportController').validator(
    new Map([
      [['api.program-report.store'], ['StoreProgramReport']],
      [['api.program-report.update'], ['UpdateProgramReport']]
    ])
  ).apiOnly()

  Route.resource('program-package', 'ProgramPackageController').validator(
    new Map([
      [['api.program-package.store'], ['StoreProgramPackage']],
      [['api.program-package.update'], ['UpdateProgramPackage']]
    ])
  ).apiOnly()
})
  .prefix('api/v1')
  .namespace('App/Controllers/Http/Api/V1')
