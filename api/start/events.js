const Event = use('Event')
const Mail = use('Mail')

const dayJs = require('dayjs')
const isSameOrAfter = require('dayjs/plugin/isSameOrAfter')
dayJs.extend(isSameOrAfter)
const Logger = use('Logger')
const axios = require('axios')

Event.on('new::cars', async req => {
  let url = `https://www.olx.co.id/api/relevance/search`
  console.log('events : ', req)
  let params = {
    // category: req.category ,
    category: 198,
    facet_limt: 100,
    location: 2000007,
    location_facet_limit: 20,
    m_tipe: `mobil-bekas-${req.brand}-${req.model}`,
    make: `mobil-bekas-${req.brand}`,
    sorting: 'desc-creation'
    // query: req.query
  }
  let queryString = new URLSearchParams(params).toString()

  try {
    let page = req.page ? req.page : 49
    let Cars = use('App/Models/Car')
    for (var i = 1; i <= page; i++) {
      let urls = `${url}?${queryString}&page=${i}`
      Logger.info({ url: urls })
      let res = await axios.get(urls)
      let { data } = res
      if (data.empty) {
        break
      }
      let items = data.data

      items.forEach(async item => {
        let ddate = new Date(item.display_date)
        if (req.date_type === 'after') {
          if (!dayJs(ddate).isAfter(dayJs(req.date_start), 'date')) {
            console.log(
              'sebelum tanggal 30',
              dayJs(ddate).format('YYYY-MM-DD')
            )
            return
          }
        }

        // ex requets : http://localhost:3333/olx?brand=mitsubishi&model=xpander&date_type=only&date_start=2020-01-13
        // scrap data only ddate after 2020-01-13 dan bukan ads
        if (req.date_type === 'only') {
          if (!dayJs(ddate).isAfter(dayJs(req.date_start), 'date')) {
            if (!item.package) {
              return
            }
          }
        }

        try {
          let car = new Cars()
          car.car_id = item.id
          car.user_id = item.user_id
          car.brand = req.brand
          car.model = req.model
          car.status = item.status.status
          car.favorite = item.favorites.count
          car.republish_date = new Date(item.republish_date)
          car.description = item.description
          car.title = item.title
          car.category_id = item.category_id
          car.price = item.price.value.raw
          car.valid_to = new Date(item.valid_to)
          car.views = item.views
          car.images = item.images.map(i => i.url).join(', ')
          car.country_id = item.locations_resolved.COUNTRY_id
          car.country_name = item.locations_resolved.COUNTRY_name
          car.province_id = item.locations_resolved.ADMIN_LEVEL_1_id
          car.province_name = item.locations_resolved.ADMIN_LEVEL_1_name
          car.district_id = item.locations_resolved.ADMIN_LEVEL_3_id
          car.district_name = item.locations_resolved.ADMIN_LEVEL_3_name
          car.city_id = item.locations_resolved.SUBLOCALITY_LEVEL_1_id
          car.city_name = item.locations_resolved.SUBLOCALITY_LEVEL_1_name
          car.display_date = new Date(item.display_date)
          car.created_at_first = new Date(item.created_at_first)
          item.parameters.forEach(i => {
            if (i.key === 'm_tipe_variant') {
              car.type = i.value
            }
            if (i.key === 'm_year') {
              car.year = i.value
            }
            if (i.key === 'mileage') {
              car.mileage = i.value
            }
            if (i.key === 'm_fuel') {
              car.fuel = i.value
            }
            if (i.key === 'm_color') {
              car.color = i.value
            }
            if (i.key === 'm_transmission') {
              car.transmission = i.value
            }
            if (i.key === 'm_drivetrain') {
              car.drivetrain = i.value
            }
            if (i.key === 'm_engine_capacity') {
              car.engine_capacity = i.value
            }
            if (i.key === 'm_feature') {
              car.feature = i.values.map(i => i.value).join(', ')
            }
            if (i.key === 'm_seller_type') {
              car.seller_type = i.value
            }
            if (i.key === 'm_exchange') {
              car.bursa_mobil = i.value
            }
            if (i.key === 'm_body') {
              car.body = i.value
            }
            if (i.key === 'm_tipe_variant') {
              car.varian = i.value
            }
          })
          await car.save()
          Logger.info({
            ...req,
            ads: !!item.package,
            car_id: item.id,
            status: 'success'
          })
        } catch (e) {
          Logger.info({
            ...req,
            ads: !!item.package,
            car_id: item.id,
            status: 'error'
          })
        }
      })
    }
  } catch (e) {
    console.log('errror', e)
    Logger.info({
      ...req,
      status: 'error_when_fetch_url',
      msg: e
    })
  }
})

Event.on('email::verification', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})

Event.on('email::resetPassword', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})

Event.on('email::completeAccount', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})

Event.on('email::daftar_program', async params => {
  await Mail.send(params.template, params.data, message => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})
