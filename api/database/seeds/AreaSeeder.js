
'use strict'
const Factory = use('Factory')

class AreaSeeder {
  async run () {
    let Area = await Factory.model('App/Models/Area').createMany(1)
    console.log(Area)
  }
}

module.exports = AreaSeeder
