
'use strict'
const Factory = use('Factory')

class TokenSeeder {
  async run () {
    let Token = await Factory.model('App/Models/Token').createMany(0)
    console.log(Token)
  }
}

module.exports = TokenSeeder
