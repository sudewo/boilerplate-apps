
'use strict'
const Factory = use('Factory')

class ProgramPackageSeeder {
  async run () {
    let ProgramPackage = await Factory.model('App/Models/ProgramPackage').createMany(10)
    console.log(ProgramPackage)
  }
}

module.exports = ProgramPackageSeeder
