
'use strict'
const Factory = use('Factory')

class ProgramOrderSeeder {
  async run () {
    let ProgramOrder = await Factory.model('App/Models/ProgramOrder').createMany(4)
    console.log(ProgramOrder)
  }
}

module.exports = ProgramOrderSeeder
