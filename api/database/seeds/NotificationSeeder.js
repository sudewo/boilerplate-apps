
'use strict'
const Factory = use('Factory')

class NotificationSeeder {
  async run () {
    let Notification = await Factory.model('App/Models/Notification').createMany(4)
    console.log(Notification)
  }
}

module.exports = NotificationSeeder
