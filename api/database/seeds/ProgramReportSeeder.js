
'use strict'
const Factory = use('Factory')

class ProgramReportSeeder {
  async run () {
    let ProgramReport = await Factory.model('App/Models/ProgramReport').createMany(4)
    console.log(ProgramReport)
  }
}

module.exports = ProgramReportSeeder
