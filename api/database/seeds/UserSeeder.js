
'use strict'
const Factory = use('Factory')

class UserSeeder {
  async run () {
    let User = await Factory.model('App/Models/User').createMany(10)
    console.log(User)
  }
}

module.exports = UserSeeder
