
'use strict'
const Factory = use('Factory')

class BranchSeeder {
  async run () {
    let Branch = await Factory.model('App/Models/Branch').createMany(4)
    console.log(Branch)
  }
}

module.exports = BranchSeeder
