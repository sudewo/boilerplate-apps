
'use strict'
const Factory = use('Factory')

class ProgramSeeder {
  async run () {
    let Program = await Factory.model('App/Models/Program').createMany(10)
    console.log(Program)
  }
}

module.exports = ProgramSeeder
