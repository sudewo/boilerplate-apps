
use('Factory').blueprint('App/Models/Area', async (faker) => {
  return {
    area_name: faker.pickone(['JATIM', 'JABAR']),
    area_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/Branch', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    area_id: (await use('App/Models/Area').query().where({ area_status: true }).select('id').orderByRaw(rand).first()).id,
    branch_name: faker.pickone(['JABODETABEK 01', 'JABODETABEK 02', 'JAWA BARAT 01', 'JAWA BARAT 02']),
    branch_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/Token', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    user_id: (await use('App/Models/User').query().where({ user_status: true }).select('id').orderByRaw(rand).first()).id,
    token: '',
    type: '',
    is_revoked: faker.bool({ likelihood: 80 })

  }
})

use('Factory').blueprint('App/Models/User', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    area_id: (await use('App/Models/Area').query().where({ area_status: true }).select('id').orderByRaw(rand).first()).id,
    branch_id: (await use('App/Models/Branch').query().where({ branch_status: true }).select('id').orderByRaw(rand).first()).id,
    dealer_type: faker.pickone(['RDR', 'RCD']),
    username: faker.first(),
    dealer_name: faker.first(),
    password: 'admin',
    email: faker.email(),
    avatar: faker.avatar({ protocol: 'https' }),
    user_status: faker.pickone([1]),
    user_role: faker.pickone([0, 2, 4]),
    parent: '',
    code: faker.bb_pin(),
    group_code: faker.bb_pin(),
    gender: faker.pickone([0, 1]),
    business_type: faker.pickone([0, 1]),
    birthdate: faker.date({ year: 2018 }),
    phone: faker.pickone(['089641695266']),
    bio: faker.sentence(),
    cover: faker.avatar({ protocol: 'https' }),
    address: faker.sentence()

  }
})

use('Factory').blueprint('App/Models/Program', async (faker) => {
  return {
    program_name: faker.pickone(['REGION JABOBAR']),
    program_area: faker.pickone([1, 3, 3, 4]),
    program_period_start: '',
    program_period_finish: '',
    program_description: faker.sentence(),
    program_term_condition: faker.sentence(),
    program_growth: faker.sentence(),
    program_roa: faker.sentence(),
    program_fid: faker.sentence(),
    program_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/ProgramOrder', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    user_id: (await use('App/Models/User').query().where({ user_status: true }).select('id').orderByRaw(rand).first()).id,
    program_id: (await use('App/Models/Program').query().where({ program_status: true }).select('id').orderByRaw(rand).first()).id,
    program_package_id: (await use('App/Models/ProgramPackage').query().where({ program_package_status: true }).select('id').orderByRaw(rand).first()).id,
    program_order_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/ProgramPackage', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    program_id: (await use('App/Models/Program').query().where({ program_status: true }).select('id').orderByRaw(rand).first()).id,
    program_package_name: '',
    program_package_type: faker.pickone([1]),
    program_package_reward_money: faker.pickone([3000000, 17500000]),
    program_package_reward_trip: faker.sentence(),
    program_package_target: faker.pickone([300, 200]),
    program_package_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/Notification', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    user_id: (await use('App/Models/User').query().where({ user_status: true }).select('id').orderByRaw(rand).first()).id,
    notification_name: faker.pickone(['program hore', 'program apresiasi']),
    notification_description: faker.sentence(),
    notification_status: faker.pickone([1])

  }
})

use('Factory').blueprint('App/Models/ProgramReport', async (faker) => {
  let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()'

  return {
    user_id: (await use('App/Models/User').query().where({ user_status: true }).select('id').orderByRaw(rand).first()).id,
    program_id: (await use('App/Models/Program').query().where({ program_status: true }).select('id').orderByRaw(rand).first()).id,
    program_package_id: (await use('App/Models/ProgramPackage').query().where({ program_package_status: true }).select('id').orderByRaw(rand).first()).id,
    program_order_id: (await use('App/Models/ProgramOrder').query().where({ program_order_status: true }).select('id').orderByRaw(rand).first()).id,
    program_report_date: '',
    program_report_total: faker.pickone([3000000, 17500000]),
    program_report_fid: faker.pickone([3000000, 17500000]),
    program_report_status: faker.pickone([1])

  }
})
