'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CitiesSchema extends Schema {
  up () {
    this.create('cities', (table) => {
      table.increments()
      table.string('name').nullable()
      table.integer('state_id').nullable()
      table.string('state_code', 2).nullable()
      table.integer('country_id').nullable()
      table.string('country_code', 2).nullable()
      table.string('latitude').nullable()
      table.string('longitude').nullable()
      table.tinyint('flag').default(1)
      table.string('wikiDataId').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('cities')
  }
}

module.exports = CitiesSchema
