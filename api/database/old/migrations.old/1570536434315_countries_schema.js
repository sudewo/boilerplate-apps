'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CountriesSchema extends Schema {
  up () {
    this.create('countries', (table) => {
      table.increments()
      table.string('name', 80).nullable()
      table.string('iso3', 5).nullable()
      table.string('iso2', 5).nullable()
      table.string('phonecode', 10).nullable()
      table.string('capital', 50).nullable()
      table.string('currency', 20).nullable()
      table.tinyint('flag', 1).default(1)
      table.string('wikiDataId').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('countries')
  }
}

module.exports = CountriesSchema
