'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatesSchema extends Schema {
  up () {
    this.create('states', (table) => {
      table.increments()
      table.string('name', 80).nullable()
      table.integer('country_id').nullable()
      table.integer('country_code', 5).nullable()
      table.string('fips_code').nullable()
      table.string('iso2').nullable()
      table.tinyint('flag').default(1)
      table.string('wikiDataId').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('states')
  }
}

module.exports = StatesSchema
