'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('code', 60).nullable()
      table.string('status', 60).nullable()
      table.string('gender', 10).nullable()
      table.date('birthdate', 60).nullable()
      table.integer('phone').nullable()
      table.text('bio').nullable()
      table.string('avatar').nullable()
      table.string('cover').nullable()
      table.integer('role').default(0)
      table.integer('country_id').nullable()
      table.integer('state_id').nullable()
      table.integer('city_id').nullable()
      table.text('location_detail').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
