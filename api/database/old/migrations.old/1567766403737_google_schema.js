'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GoogleSchema extends Schema {
  up () {
    this.create('googles', (table) => {
      table.increments()
      table.string('google_id')
      table.string('user_id')
      table.string('google_name')
      table.string('google_email')
      table.string('google_picture')
      table.string('google_url')
      table.timestamps()
    })
  }

  down () {
    this.drop('googles')
  }
}

module.exports = GoogleSchema
