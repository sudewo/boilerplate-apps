'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacebookSchema extends Schema {
  up () {
    this.create('facebooks', (table) => {
      table.increments()
      table.string('facebook_id')
      table.string('user_id')
      table.string('facebook_name')
      table.string('facebook_email')
      table.string('facebook_picture')
      table.string('facebook_url')
      table.timestamps()
    })
  }

  down () {
    this.drop('facebooks')
  }
}

module.exports = FacebookSchema
