'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InstagramSchema extends Schema {
  up () {
    this.create('instagrams', (table) => {
      table.increments()
      table.string('instagram_id')
      table.string('user_id')
      table.string('instagram_name')
      table.string('instagram_username')
      table.string('instagram_picture')
      table.string('instagram_url')
      table.timestamps()
    })
  }

  down () {
    this.drop('instagrams')
  }
}

module.exports = InstagramSchema
