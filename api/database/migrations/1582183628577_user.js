
'use strict'
const Schema = use('Schema')
class UserSchema extends Schema {
  async up () {
    const exists = await this.hasTable('users')
    if (!exists) {
      this.create('users', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('area_id').unsigned()
        table
          .foreign('area_id')
          .references('id')
          .inTable('areas')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('branch_id').unsigned()
        table
          .foreign('branch_id')
          .references('id')
          .inTable('branches')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.string('dealer_type').nullable()
        table.string('username', 64).nullable()
        table.string('dealer_name', 64).nullable()
        table.string('password', 64).nullable()
        table.string('email', 64).notNullable()
        table.string('avatar').nullable()
        table.boolean('user_status').notNullable().defaultTo(1)
        table.string('position_status').notNullable().defaultTo(1)
        table.integer('user_role').notNullable().defaultTo(0).comment('0 =>member, 2=>admin, 4=>superuser')
        table.integer('parent').nullable().defaultTo(0)
        table.string('code', 64).nullable()
        table.string('group_code', 64).nullable()
        table.integer('gender').notNullable().defaultTo(0).comment('0 => male, 1=> female')
        table.integer('business_type').notNullable().defaultTo(0).comment('0 => Dealer Mobil Bekas, 1=> Dealer Mobil Baru, 2 => Dealer Motor Bekas, 3 => Dealer Motor Baru')
        table.date('birthdate').nullable()
        table.date('join_date').nullable()
        table.integer('phone').nullable()
        table.text('bio').nullable()
        table.string('cover').nullable()
        table.text('address').nullable()

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('users')
  }
}
module.exports = UserSchema
