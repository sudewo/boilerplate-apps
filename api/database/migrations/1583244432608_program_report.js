
'use strict'
const Schema = use('Schema')
class ProgramReportSchema extends Schema {
  async up () {
    const exists = await this.hasTable('program_reports')
    if (!exists) {
      this.create('program_reports', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('user_id').unsigned()
        table
          .foreign('user_id')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('program_id').unsigned()
        table
          .foreign('program_id')
          .references('id')
          .inTable('programs')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('program_package_id').unsigned()
        table
          .foreign('program_package_id')
          .references('id')
          .inTable('program_packages')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('program_order_id').unsigned()
        table
          .foreign('program_order_id')
          .references('id')
          .inTable('program_orders')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.date('program_report_date').nullable()
        table.integer('program_report_total').nullable()
        table.integer('program_report_fid').nullable()
        table.boolean('program_report_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('program_reports')
  }
}
module.exports = ProgramReportSchema
