
'use strict'
const Schema = use('Schema')
class ProgramPackageSchema extends Schema {
  async up () {
    const exists = await this.hasTable('program_packages')
    if (!exists) {
      this.create('program_packages', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('program_id').unsigned()
        table
          .foreign('program_id')
          .references('id')
          .inTable('programs')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.string('program_package_name', 64).nullable()
        table.boolean('program_package_type').notNullable().defaultTo(1)
        table.integer('program_package_reward_money').nullable()
        table.string('program_package_reward_trip').nullable()
        table.integer('program_package_target').nullable()
        table.boolean('program_package_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('program_packages')
  }
}
module.exports = ProgramPackageSchema
