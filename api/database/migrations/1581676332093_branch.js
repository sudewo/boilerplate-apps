
'use strict'
const Schema = use('Schema')
class BranchSchema extends Schema {
  async up () {
    const exists = await this.hasTable('branches')
    if (!exists) {
      this.create('branches', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('area_id').unsigned()
        table
          .foreign('area_id')
          .references('id')
          .inTable('areas')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.string('branch_name', 64).notNullable()
        table.unique(['branch_name', 'area_id'])
        table.boolean('branch_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('branches')
  }
}
module.exports = BranchSchema
