
'use strict'
const Schema = use('Schema')
class NotificationSchema extends Schema {
  async up () {
    const exists = await this.hasTable('notifications')
    if (!exists) {
      this.create('notifications', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('user_id').unsigned()
        table
          .foreign('user_id')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.string('notification_name', 64).notNullable()
        table.string('notification_description').nullable()
        table.boolean('notification_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('notifications')
  }
}
module.exports = NotificationSchema
