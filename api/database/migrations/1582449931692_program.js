
'use strict'
const Schema = use('Schema')
class ProgramSchema extends Schema {
  async up () {
    const exists = await this.hasTable('programs')
    if (!exists) {
      this.create('programs', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.string('program_name', 64).notNullable()

        table.integer('area_id').unsigned()
        table
          .foreign('area_id')
          .references('id')
          .inTable('areas')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.date('program_period_start').nullable()
        table.date('program_period_finish').nullable()
        table.text('program_description').nullable()
        table.text('program_term_condition').nullable()
        table.integer('program_semester').nullable()
        table.integer('program_type').nullable()
        table.string('program_dealer_type').nullable()
        table.string('program_picture').nullable()
        table.integer('program_growth').nullable()
        table.string('program_roa').nullable()
        table.string('program_fid').nullable()
        table.boolean('program_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('programs')
  }
}
module.exports = ProgramSchema
