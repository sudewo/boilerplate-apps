
'use strict'
const Schema = use('Schema')
class AreaSchema extends Schema {
  async up () {
    const exists = await this.hasTable('areas')
    if (!exists) {
      this.create('areas', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.string('area_name', 64).notNullable()
        table.boolean('area_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('areas')
  }
}
module.exports = AreaSchema
