
'use strict'
const Schema = use('Schema')
class TokenSchema extends Schema {
  async up () {
    const exists = await this.hasTable('tokens')
    if (!exists) {
      this.create('tokens', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('user_id').unsigned()
        table
          .foreign('user_id')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.string('token').nullable()
        table.string('type').nullable()
        table.boolean('is_revoked').nullable().defaultTo(0)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('tokens')
  }
}
module.exports = TokenSchema
