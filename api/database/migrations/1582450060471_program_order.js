
'use strict'
const Schema = use('Schema')
class ProgramOrderSchema extends Schema {
  async up () {
    const exists = await this.hasTable('program_orders')
    if (!exists) {
      this.create('program_orders', (table) => {
        table
          .increments()
          .unsigned()
          .primary()

        table.integer('user_id').unsigned()
        table
          .foreign('user_id')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('program_id').unsigned()
        table
          .foreign('program_id')
          .references('id')
          .inTable('programs')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.integer('program_package_id').unsigned()
        table
          .foreign('program_package_id')
          .references('id')
          .inTable('program_packages')
          .onDelete('CASCADE')
          .onUpdate('NO ACTION')

        table.boolean('program_order_status').notNullable().defaultTo(1)

        table.timestamps()
      })
    }
  }

  down () {
    this.drop('program_orders')
  }
}
module.exports = ProgramOrderSchema
