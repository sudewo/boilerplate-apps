
'use strict'

class UpdateProgramPackage {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      program_id: 'required',
      program_package_name: 'required'

    }
  }

  get messages () {
    return {
      'program_id.required': 'Program is required',
      'program_package_name.required': 'Program Package Name is required'

    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = UpdateProgramPackage
