
'use strict'

class UpdateUser {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      area_id: 'required',
      branch_id: 'required',
      // username: 'required',
      email: 'required',
      user_status: 'required',
      user_role: 'required',
      // gender: 'required',
      business_type: 'required'
      // birthdate: 'required|date',
      // phone: 'required'
    }
  }

  get messages () {
    return {
      'area_id.required': 'Area is required',
      'branch_id.required': 'Branch is required',
      'username.required': 'Username is required',
      'email.required': 'Email is required',
      'user_status.required': 'User Status is required',
      'user_role.required': 'User Role is required',
      'gender.required': 'Gender is required',
      'business_type.required': 'Businnes Type is required',
      'birthdate.required|date': 'Birthdate is required|date',
      'phone.required': 'Phone is required'
    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = UpdateUser
