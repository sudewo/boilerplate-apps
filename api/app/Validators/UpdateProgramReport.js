
'use strict'

class UpdateProgramReport {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      user_id: 'required',
      program_id: 'required',
      program_package_id: 'required',
      program_order_id: 'required',
      program_report_date: 'required'

    }
  }

  get messages () {
    return {
      'user_id.required': 'User is required',
      'program_id.required': 'Program is required',
      'program_package_id.required': 'Program Package is required',
      'program_order_id.required': 'Program Order is required',
      'program_report_date.required': 'Program Report Date is required'

    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = UpdateProgramReport
