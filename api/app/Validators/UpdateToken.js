
'use strict'

class UpdateToken {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      user_id: 'required',
      is_revoked: 'required'

    }
  }

  get messages () {
    return {
      'user_id.required': 'User is required',
      'is_revoked.required': 'Is Revoked is required'

    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = UpdateToken
