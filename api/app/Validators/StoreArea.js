
'use strict'

class StoreArea {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      area_name: 'required'

    }
  }

  get messages () {
    return {
      'area_name.required': 'Area is required'

    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = StoreArea
