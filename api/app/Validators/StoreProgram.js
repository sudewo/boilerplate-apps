
'use strict'

class StoreProgram {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      program_name: 'required',
      program_period_start: 'required',
      program_period_finish: 'required'

    }
  }

  get messages () {
    return {
      'program_name.required': 'Program Name is required',
      'program_area.required': 'Program Area is required',
      'program_period_start.required': 'Program Period Start is required',
      'program_period_finish.required': 'Program Period Start is required'

    }
  }

  async fails (errorMessages) {
    let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
    let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
    return this.ctx.response.json({
      __status: `failed_${action}_${className}`,
      __message: `failed ${action} ${className}`,
      __error: null,
      __validation: errorMessages })
  }
}

module.exports = StoreProgram
