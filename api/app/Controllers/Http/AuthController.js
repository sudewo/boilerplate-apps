'use strict'

class AuthController {
  async test ({ auth, request, response }) {
    response.send(request.all())
  }

  async testget ({ auth, request, response }) {
    // const db = use('Database')
    // // const r = await db.raw(`select branch_id, id from users`
    // let x = []
    // dd.forEach(async (i, j) => {
    //   // try {
    //   let k = Object.keys(i)
    //   let b = Object.values(i)
    //   let sql = `select id from branches where branch_name='${b[0]}'`
    //   let bn = await db.raw(sql)
    //   // if (bn[0]) {
    //   //   // console.log(bn[0])
    //   //   x.push(bn[0].id)
    //   // }
    //   // } catch (e) {
    //   if (bn[0]) {
    //     console.log(`update users set branch_id='${bn[0].id}' where group_code='${k[0]}'`)
    //   }
    //   // x.push({ code: k[0], name: b[0], id: bn[0] })
    //   // i.id = bn
    //   // i.sql = `update users set branch_id='${}' where id=${i.id}`
    //   // i.sql = `update users set username='${username}', email='${email}', birthdate=CURRENT_TIMESTAMP, phone='888', password='$2a$10$w2a8l/cTZCS9NoqEj79df.U7XyNBeQ/2R/mfrxhTa1hHUKulJQSAG', user_status='1', created_at=CURRENT_TIMESTAMP where id=${i.id}`
    //   // await db.raw(i.sql)
    // })
    response.send(['test'])
  }

  async login ({ auth, request, response }) {
    try {
      let { email, phone, password } = request.all()
      let userEmail = email || ''
      let userPhone = phone || ''
      let usr = (await use('App/Models/User').query().where({ 'phone': userPhone }).orWhere({ 'email': userEmail }).first()).toJSON()
      let jwt = await auth.withRefreshToken().attempt(String(usr.id), password)
      let user = await use('App/Models/User').query().with('Area').where('id', usr.id).where('user_status', 1).first()

      if (user) {
        if (user.country_id) {
          let getCountry = await user.country().fetch()
          user.country = { id: getCountry.id, name: getCountry.name }
        }

        if (user.state_id) {
          let getState = await user.state().fetch()
          user.state = { id: getState.id, name: getState.name }
        }

        if (user.city_id) {
          let getCity = await user.city().fetch()
          user.city = { id: getCity.id, name: getCity.name }
        }
      }
      user = user.toJSON()
      delete user.password
      console.log('user ', user)
      response.send({
        __status: 'success',
        loginWith: 'email',
        __message: 'login success',
        ...{ ...user, ...jwt }
      })
    } catch (e) {
      response.send({
        __status: 'error',
        loginWith: 'email',
        __message: 'login failed',
        __error: e.message
      })
    }
  }

  async changeAvatar ({ auth, request, response }) {
    try {
      const Helpers = use('Helpers')
      const validationOptions = {
        types: ['image'],
        size: '20Mb',
        extnames: ['png', 'gif', 'jpeg', 'jpg', 'JPG']
      }
      const avatar = request.file('file', validationOptions)
      await avatar.move(Helpers.appRoot('public/uploads'), {
        overwrite: true
      })

      if (!avatar.moved()) {
        response.send({
          __status: 'failed_upload',
          __message: avatar.error().message,
          __error: avatar.error()
        })
      } else {
        let user = await use(`App/Models/User`)
        let res = await user.find(request.input('id'))
        console.log('type : ', request.input('type'))
        if (request.input('type') === 'avatar') {
          res.avatar = avatar.fileName
        } else {
          res.cover = avatar.fileName
        }
        res.save()
        let dd = {}
        if (request.input('type') === 'avatar') {
          dd.avatar = avatar.fileName
        } else {
          dd.cover = avatar.fileName
        }
        response.send({
          __status: 'success_upload',
          __message: 'upload success',
          ...dd
        })
      }
    } catch (e) {
      response.send({
        __status: 'error_upload',
        __message: e.message,
        __error: e.message
      })
    }
  }

  async email ({ response }) {
    const Event = use('Event')
    Event.fire('email::verification', {
      template: 'emails.welcome',
      subject: 'Testing Gembel',
      data: { username: 'Dewo' },
      from: 'foo@bar.com',
      to: 'bar@baz.com'
    })
    response.send('Email Send')
  }

  async resetPassword ({ request, response }) {
    const Event = use('Event')
    let user = await use(`App/Models/User`)
    let res = await user.findBy('email', request.input('email'))
    if (!res) {
      response.send({
        __status: 'error',
        __message: 'Email Not Registered',
        __error: '',
        resetPasswordStep: 'reset_password',
        ...request.all()
      })
    } else {
      if (!res.code) {
        response.send({
          __status: 'error',
          __message: 'Use verification code step before change password',
          __error: '',
          resetPasswordStep: 'reset_password',
          ...request.all()
        })
      } else {
        res.password = request.input('password')
        res.code = null
        res.save()
        Event.fire('email::resetPassword', {
          template: 'emails.resetPassword',
          subject: 'Reset Password Success',
          data: {
            name: res.username,
            email: res.email,
            password: res.password
          },
          from: 'help@noname.co.id',
          to: res.email
        })
        response.send({
          __status: 'success',
          __message: 'update password success, try login again',
          __error: '',
          resetPasswordStep: '',
          ...request.all()
        })
      }
    }
  }

  async emailVerification ({ request, response }) {
    const Event = use('Event')
    let user = await use(`App/Models/User`)
    let res = await user.findBy(`email`, request.input('email'))
    if (!res) {
      response.send({
        __status: 'error',
        __message: 'Email Not Registered',
        __error: '',
        resetPasswordStep: 'send_code_verification',
        ...request.all()
      })
    } else {
      const generateRandomString = [...Array(8)]
        .map(i => (~~(Math.random() * 36)).toString(36))
        .join('')
      res.code = generateRandomString
      res.save()
      Event.fire('email::verification', {
        template: 'emails.emailVerification',
        subject: 'Verification Code',
        data: { name: res.username, code: generateRandomString },
        from: 'help@noname.co.id',
        to: res.email
      })
      response.send({
        __status: 'success',
        __message: 'Check verification code in your email',
        __error: '',
        resetPasswordStep: 'send_code_verification',
        ...request.all()
      })
    }
  }

  async codeVerification ({ request, response }) {
    let user = await use(`App/Models/User`)
    let res = await user.findBy(`email`, request.input('email'))

    if (request.input('code') === res.code) {
      response.send({
        __status: 'success',
        __message: 'verification code success.',
        __error: '',
        resetPasswordStep: 'check_code_verification',
        ...request.all()
      })
    } else {
      response.send({
        __status: 'error',
        __message: 'code verification is wrong, try again.',
        __error: '',
        resetPasswordStep: 'check_code_verification',
        ...request.all()
      })
    }
  }

  async logout ({ auth, response }) {
    const apiToken = auth.getAuthHeader()
    const user = await auth.authenticator('jwt').getUser(apiToken)
    await auth.authenticator('jwt').revokeTokensForUser(user)
  }

  capitalizeFirstLetter (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  async loginWithSocialMedia ({ auth, request, response }) {
    let field = request.all()
    let mdlName = this.capitalizeFirstLetter(field.loginWith)
    try {
      let res = await use(`App/Models/${mdlName}`).findBy(
        `${field.loginWith}_id`,
        field[field.loginWith].id
      )

      if (!res) {
        response.send({
          ...field,
          __status: 'error',
          loginWith: field.loginWith,
          __message: `${mdlName} account not found, please complete registration process`
        })
      } else {
        const user = await use('App/Models/User').find(res.user_id)
        if (user) {
          if (user) {
            if (user.country_id) {
              let getCountry = await user.country().fetch()
              user.country = { id: getCountry.id, name: getCountry.name }
            }

            if (user.state_id) {
              let getState = await user.state().fetch()
              user.state = { id: getState.id, name: getState.name }
            }

            if (user.city_id) {
              let getCity = await user.city().fetch()
              user.city = { id: getCity.id, name: getCity.name }
            }
          }

          let jwt = await auth.generate(user)
          let getUser = user.toJSON()
          delete getUser.password
          response.send({
            ...field,
            __status: 'success',
            loginWith: field.loginWith,
            __message: 'Login Success',
            ...getUser,
            ...jwt,
            __error: ''
          })
        } else {
          response.send({
            ...field,
            __status: 'error',
            loginWith: field.loginWith,
            __message: `we found ${field.loginWith} account with id ${
              field[field.loginWith].id
            }-${
              field[field.loginWith].email
            }, but user not registered, please complete registration process`
          })
        }
      }
    } catch (e) {
      console.log('error : ', e)
      response.send({
        __status: 'error',
        loginWith: field.loginWith,
        __message: e.message
      })
    }
  }

  generateParams (field, saveUser) {
    let params = {}
    let type = field.loginWith.toLowerCase()
    params[`${type}_id`] = field[field.loginWith][`id`]
    params[`user_id`] = saveUser.id
    params[`${type}_name`] = field[field.loginWith][`name`]
    if (field.loginWith === 'instagram') {
      params[`${type}_username`] = field[field.loginWith][`username`]
    } else {
      params[`${type}_email`] = field[field.loginWith][`email`]
    }
    params[`${type}_picture`] = field[field.loginWith][`picture`]
    console.log('REGISTER PARAMS ', params)
    return params
  }

  async completeAccount ({ auth, request, response }) {
    const Event = use('Event')
    let field = request.all()
    try {
      let user = await use('App/Models/User')
      let res = await user.findBy(`email`, field.email)

      if (res) {
        response.send({
          ...field,
          __status: 'error',
          __message: `user ${res.email} already registered`,
          __error: ''
        })
        return false
      }

      let saveUser = await user.create({
        username: field.username,
        email: field.email,
        password: field.password
      })

      if (field.loginWith && field.loginWith !== 'email') {
        let mdlName = this.capitalizeFirstLetter(field.loginWith)
        let socmed = await use(`App/Models/${mdlName}`)
        let params = this.generateParams(field, saveUser)
        await socmed.create(params)

        Event.fire('email::completeAccount', {
          template: 'emails.completeAccount',
          subject: `Welcome ${this.capitalizeFirstLetter(saveUser.username)}`,
          data: {
            username: saveUser.username,
            email: field.email,
            password: field.password,
            loginWith: mdlName
          },
          from: 'help@noname.co.id',
          to: saveUser.email
        })
      } else {
        Event.fire('email::completeAccount', {
          template: 'emails.completeAccount',
          subject: `Welcome ${this.capitalizeFirstLetter(saveUser.username)}`,
          data: {
            username: saveUser.username,
            email: field.email,
            password: field.password,
            loginWith: 'Email'
          },
          from: 'help@noname.co.id',
          to: saveUser.email
        })
      }

      response.send({
        ...field,
        __status: 'login_success',
        __message: 'Register Success, Please login again',
        __error: ''
      })
    } catch (e) {
      console.log(' error : ', e)
      response.send({
        ...field,
        __status: 'login_error',
        __message: 'Register Failed',
        __error: e.message
      })
    }
  }

  async updateProfile ({ auth, request, response }) {
    let usr = await auth.getUser()
    await auth.authenticator('jwt').revokeTokens()

    await auth.authenticator('jwt').revokeTokensForUser(usr)

    console.log('list token ', await auth.listTokens())
    console.log('update profile ', await auth.authenticator('jwt').check())
    // const Event = use('Event')
    let field = request.all()
    try {
      let user = await use('App/Models/User')
      let res = await user.find(field.id)

      res.email = field.email
      res.username = field.username
      res.phone = field.phone
      res.birthdate = field.birthdate
      res.country_id = field.country.id
      res.state_id = field.state.id
      res.city_id = field.city.id
      res.bio = field.bio
      res.gender = field.gender

      await res.save()

      if (field.country.id) {
        let getCountry = await res.country().fetch()
        res.country = { id: getCountry.id, name: getCountry.name }
      }

      if (field.state.id) {
        let getState = await res.state().fetch()
        res.state = { id: getState.id, name: getState.name }
      }

      if (field.city.id) {
        let getCity = await res.city().fetch()
        res.city = { id: getCity.id, name: getCity.name }
      }

      if (res) {
        let result = res.toJSON()
        delete result.password
        response.send({
          ...field,
          ...result,
          __status: 'success_update_profile',
          __message: 'Update Profile Success',
          __error: '',
          error: null
        })
      } else {
      }

      //   let saveUser = await user.create({ username: field.username, email: field.email, password: field.password })
      //   let mdlName = this.capitalizeFirstLetter(field.loginWith)
      //   let socmed = await use(`App/Models/${mdlName}`)
      //   let params = this.generateParams(field, saveUser)
      //   console.log('params ', params)
      //   await socmed.create(params)
      //   response.send({ ...field, '__status': 'success', '__message': 'Register Success', '__error': '', 'error': null })
      //   Event.fire('email::completeAccount', { template: 'emails.completeAccount', subject: `Welcome ${this.capitalizeFirstLetter(saveUser.username)}`, data: { username: saveUser.username, email: field.email, password: field.password, loginWith: mdlName }, from: 'help@noname.co.id', to: saveUser.email })
    } catch (e) {
      console.log(' error : ', e)
      response.send({
        ...field,
        __status: 'error_update_profile',
        __message: 'Update Profile Error',
        __error: e.message
      })
    }
  }
}

module.exports = AuthController
