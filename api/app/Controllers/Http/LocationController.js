'use strict'

class LocationController {
  async countries ({ request, response }) {
    try {
      let country = await use(`App/Models/Country`)
      let res = await country.all()
      if (!res) {
        response.send({ '__status': 'failed', '__message': 'failed get countries', '__error': '' })
      } else {
        response.send({ '__status': 'success', '__message': 'success get countries', '__error': '', 'countries': res.toJSON() })
      }
    } catch (e) {
      response.send({ '__status': 'error', '__message': e.message })
    }
  }

  async states ({ request, response }) {
    try {
      let state = await use(`App/Models/State`)
      // console.log('country_id ', request.all())
      let res = await state.query().where('country_id', request.input('country_id')).fetch()
      // console.log('res ', res.toJSON())
      if (!res) {
        response.send({ '__status': 'failed', '__message': 'failed get states', '__error': '' })
      } else {
        response.send({ '__status': 'success', '__message': 'success get states', '__error': '', 'states': res.toJSON() })
      }
    } catch (e) {
      response.send({ '__status': 'error', '__message': e.message })
    }
  }

  async cities ({ request, response }) {
    try {
      let city = await use(`App/Models/City`)
      let res = await city.query().where('state_id', request.input('state_id')).fetch()
      if (!res) {
        response.send({ '__status': 'failed', '__message': 'failed get cities', '__error': '' })
      } else {
        response.send({ '__status': 'success', '__message': 'success get cities', '__error': '', 'cities': res.toJSON() })
      }
    } catch (e) {
      response.send({ '__status': 'error', '__message': e.message })
    }
  }

  async emailVerification ({ request, response }) {
    const Event = use('Event')
    let user = await use(`App/Models/User`)
    let res = await user.findBy(`email`, request.input('email'))
    if (!res) {
      response.send({ '__status': 'error', '__message': 'Email Not Registered', '__error': '', 'resetPasswordStep': 'send_code_verification', ...request.all() })
    } else {
      const generateRandomString = [...Array(8)].map(i => (~~(Math.random() * 36)).toString(36)).join('')
      res.code = generateRandomString
      res.save()
      Event.fire('email::verification', { template: 'emails.emailVerification', subject: 'Verification Code', data: { name: res.username, code: generateRandomString }, from: 'help@noname.co.id', to: res.email })
      response.send({ '__status': 'success', '__message': 'Check verification code in your email', '__error': '', 'resetPasswordStep': 'send_code_verification', ...request.all() })
    }
  }

  async codeVerification ({ request, response }) {
    let user = await use(`App/Models/User`)
    let res = await user.findBy(`email`, request.input('email'))

    if (request.input('code') === res.code) {
      response.send({ '__status': 'success', '__message': 'verification code success.', '__error': '', 'resetPasswordStep': 'check_code_verification', ...request.all() })
    } else {
      response.send({ '__status': 'error', '__message': 'code verification is wrong, try again.', '__error': '', 'resetPasswordStep': 'check_code_verification', ...request.all() })
    }
  }

  async logout ({ auth, response }) {
    const apiToken = auth.getAuthHeader()
    const user = await auth.authenticator('jwt').getUser(apiToken)
    await auth.authenticator('jwt').revokeTokensForUser(user)
  }

  capitalizeFirstLetter (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  async loginWithSocialMedia ({ auth, request, response }) {
    let field = request.all()
    let mdlName = this.capitalizeFirstLetter(field.loginWith)
    try {
      let res = await use(`App/Models/${mdlName}`).findBy(`${field.loginWith}_id`, field[field.loginWith].id)

      if (!res) {
        response.send({ ...field, '__status': 'error', loginWith: field.loginWith, '__message': `${mdlName} account not found, please complete registration process` })
      } else {
        const user = await use('App/Models/User').find(res.user_id)
        if (user) {
          let jwt = await auth.generate(user)
          let getUser = user.toJSON()
          delete getUser.password
          response.send({ ...field, '__status': 'success', loginWith: field.loginWith, '__message': 'Login Success', ...getUser, ...jwt, '__error': '' })
        } else {
          response.send({ ...field, '__status': 'error', loginWith: field.loginWith, '__message': `we found ${field.loginWith} account with id ${field[field.loginWith].id}-${field[field.loginWith].email}, but user not registered, please complete registration process` })
        }
      }
    } catch (e) {
      console.log('error : ', e)
      response.send({ '__status': 'error', loginWith: field.loginWith, '__message': e.message })
    }
  }

  generateParams (field, saveUser) {
    let params = {}
    let type = (field.loginWith).toLowerCase()
    params[`${type}_id`] = field[field.loginWith][`id`]
    params[`user_id`] = saveUser.id
    params[`${type}_name`] = field[field.loginWith][`name`]
    if (field.loginWith === 'instagram') {
      params[`${type}_username`] = field[field.loginWith][`username`]
    } else {
      params[`${type}_email`] = field[field.loginWith][`email`]
    }
    params[`${type}_picture`] = field[field.loginWith][`picture`]
    console.log('REGISTER PARAMS ', params)
    return params
  }

  async completeAccount ({ auth, request, response }) {
    const Event = use('Event')
    let field = request.all()
    try {
      let user = await use('App/Models/User')
      let res = await user.findBy(`email`, field.email)

      if (res) {
        response.send({ ...field, '__status': 'error', '__message': `user ${res.email} already registered`, '__error': '' })
        return false
      }

      let saveUser = await user.create({ username: field.username, email: field.email, password: field.password })
      let mdlName = this.capitalizeFirstLetter(field.loginWith)
      let socmed = await use(`App/Models/${mdlName}`)
      let params = this.generateParams(field, saveUser)
      console.log('params ', params)
      await socmed.create(params)
      response.send({ ...field, '__status': 'success', '__message': 'Register Success', '__error': '', 'error': null })
      Event.fire('email::completeAccount', { template: 'emails.completeAccount', subject: `Welcome ${this.capitalizeFirstLetter(saveUser.username)}`, data: { username: saveUser.username, email: field.email, password: field.password, loginWith: mdlName }, from: 'help@noname.co.id', to: saveUser.email })
    } catch (e) {
      console.log(' error : ', e)
      response.send({ ...field, '__status': 'error', '__message': 'Register Failed', '__error': e.message })
    }
  }
}

module.exports = LocationController
