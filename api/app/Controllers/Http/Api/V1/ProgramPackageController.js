
'use strict'
const CrudController = use('CrudController')
class ProgramPackageController extends CrudController {
  constructor () {
    super('ProgramPackage', 'api')
  }
  async field () {
    return [
      { name: 'program_packages.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'program_name', format: `(val) => val && val.program_name`, field: 'Program', type: 'string', label: 'Program', sortable: true, search: true },
      { name: 'program_packages.program_package_name', field: 'program_package_name', type: 'string', label: 'Program Package Name', sortable: true, search: true },
      { name: 'program_packages.program_package_type', field: 'program_package_type', type: 'integer', label: 'Program Type', sortable: true, search: true },
      { name: 'program_packages.program_package_reward_money', field: 'program_package_reward_money', type: 'integer', label: 'Uang', sortable: true, search: true },
      { name: 'program_packages.program_package_reward_trip', field: 'program_package_reward_trip', type: 'string', label: 'Trip', sortable: true, search: true },
      { name: 'program_packages.program_package_target', field: 'program_package_target', type: 'integer', label: 'Program Package Target', sortable: true, search: true },
      { name: 'program_packages.program_package_status', field: 'program_package_status', type: 'integer', label: 'Program Status', sortable: true, search: true },
      { name: 'program_packages.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'program_packages.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const programPackage = await use('App/Models/ProgramPackage')
    let query = programPackage.query()
      .from('program_packages')
      .select(['program_packages.id', 'program_packages.program_id', 'program_packages.program_package_name', 'program_packages.program_package_type', 'program_packages.program_package_reward_money', 'program_packages.program_package_reward_trip', 'program_packages.program_package_target', 'program_packages.program_package_status', 'program_packages.created_at', 'program_packages.updated_at'])
      .with('Program', builder => {
        return builder.select(['id', 'program_name'])
      })
      .leftJoin('programs', 'program_packages.program_id', 'programs.id')
      .where((builder) => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('program_packages.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'program_packages.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return programPackage;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = ProgramPackageController
