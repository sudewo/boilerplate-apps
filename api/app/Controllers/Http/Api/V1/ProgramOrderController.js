
'use strict'
const CrudController = use('CrudController')
class ProgramOrderController extends CrudController {
  constructor () {
    super('ProgramOrder', 'api')
  }
  async field () {
    return [
      { name: 'program_orders.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'username', format: `(val) => val && val.username`, field: 'User', type: 'string', label: 'User', sortable: true, search: true },
      { name: 'program_name', format: `(val) => val && val.program_name`, field: 'Program', type: 'string', label: 'Program', sortable: true, search: true },
      { name: 'program_package_name', format: `(val) => val && val.program_package_name`, field: 'ProgramPackage', type: 'string', label: 'Program Package', sortable: true, search: true },
      { name: 'program_orders.program_order_status', field: 'program_order_status', type: 'integer', label: 'Order Status', sortable: true, search: true },
      { name: 'program_orders.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'program_orders.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const programOrder = await use('App/Models/ProgramOrder')
    let query = programOrder.query()
      .from('program_orders')
      .select(['program_orders.id', 'program_orders.user_id', 'program_orders.program_id', 'program_orders.program_package_id', 'program_orders.program_order_status', 'program_orders.created_at', 'program_orders.updated_at'])
      .with('User', builder => {
        return builder.select(['id', 'username'])
      })
      .with('Program', builder => {
        return builder.select(['id', 'program_name'])
      })
      .with('ProgramPackage', builder => {
        return builder.select(['id', 'program_package_name'])
      })
      .leftJoin('users', 'program_orders.user_id', 'users.id')
      .leftJoin('programs', 'program_orders.program_id', 'programs.id')
      .leftJoin('program_packages', 'program_orders.program_package_id', 'program_packages.id')
      .where((builder) => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('program_orders.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'program_orders.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return programOrder;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = ProgramOrderController
