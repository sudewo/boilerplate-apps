
'use strict'
const CrudController = use('CrudController')
class UserController extends CrudController {
  constructor () {
    super('User', 'api')
  }
  async field () {
    return [
      { name: 'users.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'area_name', format: `(val) => val && val.area_name`, field: 'Area', type: 'string', label: 'Area', sortable: true, search: true },
      { name: 'branch_name', format: `(val) => val && val.branch_name`, field: 'Branch', type: 'string', label: 'Branch', sortable: true, search: true },
      { name: 'users.dealer_type', field: 'dealer_type', type: 'string', label: 'Dealer Type', sortable: true, search: true },
      { name: 'users.username', field: 'username', type: 'string', label: 'Username', sortable: true, search: true },
      { name: 'users.dealer_name', field: 'dealer_name', type: 'string', label: 'Dealer Name', sortable: true, search: true },
      { name: 'users.password', field: 'password', type: 'string', label: 'Password', sortable: true, search: true },
      { name: 'users.email', field: 'email', type: 'string', label: 'Email', sortable: true, search: true },
      { name: 'users.avatar', field: 'avatar', type: 'string', label: 'Avatar', sortable: true, search: true },
      { name: 'users.user_status', field: 'user_status', type: 'integer', label: 'User Status', sortable: true, search: true },
      { name: 'users.user_role', field: 'user_role', type: 'enu', label: 'User Role', enumData: [{ 'label': 'Dealer', 'value': 0 }, { 'label': 'Admin', 'value': 2 }, { 'label': 'Superuser', 'value': 4 }], sortable: true, search: true },
      { name: 'users.parent', field: 'parent', type: 'integer', label: 'User Parent', sortable: true, search: true },
      { name: 'users.code', field: 'code', type: 'string', label: 'Verification Code', sortable: true, search: true },
      { name: 'users.group_code', field: 'group_code', type: 'string', label: 'Group Code', sortable: true, search: true },
      { name: 'users.gender', field: 'gender', type: 'enu', label: 'Gender', enumData: [{ 'label': 'Male', 'value': 0 }, { 'label': 'Female', 'value': 1 }], sortable: true, search: true },
      { name: 'users.business_type', field: 'business_type', type: 'enu', label: 'Businnes Type', enumData: [{ 'label': 'Dealer Mobil Bekas', 'value': 0 }, { 'label': 'Dealer Mobil Baru', 'value': 1 }, { 'label': 'Dealer Motor Bekas', 'value': 2 }, { 'label': 'Dealer Motor Baru', 'value': 3 }], sortable: true, search: true },
      { name: 'users.birthdate', field: 'birthdate', type: 'date', label: 'Birthdate', sortable: true, search: true },
      { name: 'users.phone', field: 'phone', type: 'integer', label: 'Phone', sortable: true, search: true },
      { name: 'users.bio', field: 'bio', type: 'text', label: 'Bio', sortable: true, search: true },
      { name: 'users.cover', field: 'cover', type: 'string', label: 'Cover', sortable: true, search: true },
      { name: 'users.address', field: 'address', type: 'text', label: 'Address', sortable: true, search: true },
      { name: 'users.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'users.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const user = await use('App/Models/User')
    let query = user.query()
      .from('users')
      .select(['users.id', 'users.area_id', 'users.branch_id', 'users.dealer_type', 'users.username', 'users.dealer_name', 'users.password', 'users.email', 'users.avatar', 'users.user_status', 'users.user_role', 'users.parent', 'users.code', 'users.group_code', 'users.gender', 'users.business_type', 'users.birthdate', 'users.phone', 'users.bio', 'users.cover', 'users.address', 'users.created_at', 'users.updated_at'])
      .with('Area', builder => {
        return builder.select(['id', 'area_name'])
      })
      .with('Branch', builder => {
        return builder.select(['id', 'branch_name'])
      })
      .leftJoin('areas', 'users.area_id', 'areas.id')
      .leftJoin('branches', 'users.branch_id', 'branches.id')
      .where((builder) => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('users.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'users.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return user;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = UserController
