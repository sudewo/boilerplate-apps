
'use strict'
const CrudController = use('CrudController')
class BranchController extends CrudController {
  constructor () {
    super('Branch', 'api')
  }
  async field () {
    return [
      { name: 'branches.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'area_name', format: `(val) => val && val.area_name`, field: 'Area', type: 'string', label: 'Area', sortable: true, search: true },
      { name: 'branches.branch_name', field: 'branch_name', type: 'string', label: 'ASM Area', sortable: true, search: true },
      { name: 'branches.branch_status', field: 'branch_status', type: 'integer', label: 'Area Status', sortable: true, search: true },
      { name: 'branches.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'branches.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const branch = await use('App/Models/Branch')
    let query = branch.query()
      .from('branches')
      .select(['branches.id', 'branches.area_id', 'branches.branch_name', 'branches.branch_status', 'branches.created_at', 'branches.updated_at'])
      .with('Area', builder => {
        return builder.select(['id', 'area_name'])
      })
      .leftJoin('areas', 'branches.area_id', 'areas.id')
      .where((builder) => {
        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'branches.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return branch;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = BranchController
