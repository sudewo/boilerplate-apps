
'use strict'
const CrudController = use('CrudController')
class NotificationController extends CrudController {
  constructor () {
    super('Notification', 'api')
  }
  async field () {
    return [
      { name: 'notifications.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'username', format: `(val) => val && val.username`, field: 'User', type: 'string', label: 'User', sortable: true, search: true },
      { name: 'notifications.notification_name', field: 'notification_name', type: 'string', label: 'Notification name', sortable: true, search: true },
      { name: 'notifications.notification_description', field: 'notification_description', type: 'string', label: 'Notification', sortable: true, search: true },
      { name: 'notifications.notification_status', field: 'notification_status', type: 'integer', label: 'Notification Type Status', sortable: true, search: true },
      { name: 'notifications.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'notifications.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const notification = await use('App/Models/Notification')
    let query = notification.query()
      .from('notifications')
      .select(['notifications.id', 'notifications.user_id', 'notifications.notification_name', 'notifications.notification_description', 'notifications.notification_status', 'notifications.created_at', 'notifications.updated_at'])
      .with('User', builder => {
        return builder.select(['id', 'username'])
      })
      .leftJoin('users', 'notifications.user_id', 'users.id')
      .where((builder) => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('notifications.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'notifications.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return notification;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = NotificationController
