'use strict'
const axios = require('axios')
const Database = use('Database')
const dayJs = require('dayjs')
const isSameOrAfter = require('dayjs/plugin/isSameOrAfter')
dayJs.extend(isSameOrAfter)
const Logger = use('Logger')
const Event = use('Event')

class OlxController {
  async scrapAll ({ request, response }) {
    if (!request.input('a')) {
      response.send('something si required')
    }

    let cars = [
      { brand: 'toyota', model: 'avanza' },
      { brand: 'toyota', model: 'innova' },
      { brand: 'toyota', model: 'fortuner' },
      { brand: 'toyota', model: 'yaris' },
      { brand: 'toyota', model: 'camry' },
      { brand: 'toyota', model: 'alphard' },
      { brand: 'toyota', model: 'rush' },
      { brand: 'toyota', model: 'agya' },
      { brand: 'toyota', model: 'calya' },
      { brand: 'toyota', model: 'kijang' },
      { brand: 'toyota', model: 'vios' },
      { brand: 'toyota', model: 'sienta' },
      { brand: 'toyota', model: 'velfire' },
      { brand: 'toyota', model: 'harrier' },
      { brand: 'toyota', model: 'altis' },
      { brand: 'toyota', model: 'corolla' },
      { brand: 'toyota', model: 'CorollaAltis' },
      { brand: 'toyota', model: 'soluna' },
      { brand: 'toyota', model: 'nav1' },
      { brand: 'toyota', model: 'voxy' },
      { brand: 'toyota', model: 'etios' },
      { brand: 'toyota', model: 'EtiosValco' },
      { brand: 'honda', model: 'cr-v' },
      { brand: 'honda', model: 'jazz' },
      { brand: 'honda', model: 'hr-v' },
      { brand: 'honda', model: 'mobilio' },
      { brand: 'honda', model: 'brio' },
      { brand: 'honda', model: 'freed' },
      { brand: 'honda', model: 'city' },
      { brand: 'honda', model: 'civic' },
      { brand: 'honda', model: 'accord' },
      { brand: 'honda', model: 'br-v' },
      { brand: 'daihatsu', model: 'xenia' },
      { brand: 'daihatsu', model: 'terios' },
      { brand: 'daihatsu', model: 'ayla' },
      { brand: 'daihatsu', model: 'sigra' },
      { brand: 'daihatsu', model: 'granmax' },
      { brand: 'daihatsu', model: 'sirion' },
      { brand: 'daihatsu', model: 'luxio' },
      { brand: 'daihatsu', model: 'granmaxpick-up' },
      { brand: 'nissan', model: 'grandlivina' },
      { brand: 'nissan', model: 'serena' },
      { brand: 'nissan', model: 'x-trail' },
      { brand: 'nissan', model: 'march' },
      { brand: 'nissan', model: 'livina' },
      { brand: 'nissan', model: 'juke' },
      { brand: 'nissan', model: 'evalia' },
      { brand: 'nissan', model: 'teana' },
      { brand: 'suzuki', model: 'ertiga' },
      { brand: 'suzuki', model: 'apv' },
      { brand: 'suzuki', model: 'swift' },
      { brand: 'suzuki', model: 'ignis' },
      { brand: 'suzuki', model: 'sx4' },
      { brand: 'suzuki', model: 'karimun' },
      { brand: 'suzuki', model: 'baleno' },
      { brand: 'suzuki', model: 'carrypick-up' },
      { brand: 'suzuki', model: 'wagon-r' },
      { brand: 'suzuki', model: 'grandvitara' },
      { brand: 'suzuki', model: 'splash' },
      { brand: 'suzuki', model: 'carry' },
      { brand: 'mitsubishi', model: 'pajerosport' },
      { brand: 'mitsubishi', model: 'xpander' },
      { brand: 'mitsubishi', model: 'outlander' },
      { brand: 'mitsubishi', model: 'mirage' },
      { brand: 'mitsubishi', model: 'l300' },
      { brand: 'mitsubishi', model: 'pajero' },
      { brand: 'mitsubishi', model: 'strada_triton' }
    ]

    cars.forEach(req => {
      Event.fire('new::cars', {
        brand: req.brand,
        model: req.model,
        date_type: req.date_type || '',
        date_start: req.date_start || ''
      })
    })

    response.send(['scrapping all'])
  }

  async index ({ request, response }) {
    // response.send('hi')
    // http://localhost:3333/olx?brand=mitsubishi&model=mirage
    // http://localhost:3333/olx?brand=honda&model=mobilio&m_tipe=mobil-bekas-honda-mobilio&make=mobil-bekas-honda
    let req = request.all()
    if (!req.brand) {
      return response.send('please provide brand')
    }
    if (!req.model) {
      return response.send('please provide model')
    }

    Event.fire('new::cars', {
      brand: req.brand,
      model: req.model,
      date_type: req.date_type || '',
      date_start: req.date_start || ''
    })
    // let result = await this.scrap({ brand: req.brand, model: req.model, date_type: '', date_start: '' })
    response.send(['done'])
  }

  async scrap (req) {
    let url = `https://www.olx.co.id/api/relevance/search`
    let params = {
      // category: req.category ,
      category: 198,
      facet_limt: 100,
      location: 2000007,
      location_facet_limit: 20,
      m_tipe: `mobil-bekas-${req.brand}-${req.model}`,
      make: `mobil-bekas-${req.brand}`,
      sorting: 'desc-creation'
      // query: req.query
    }
    let queryString = new URLSearchParams(params).toString()

    try {
      let page = req.page ? req.page : 49
      let Cars = use('App/Models/Car')
      // let today = new Date()
      for (var i = 1; i <= page; i++) {
        let urls = `${url}?${queryString}&page=${i}`
        // console.log('loop ke', i, urls)
        Logger.info({ url: urls })
        let res = await axios.get(urls)
        let { data } = res
        if (data.empty) {
          break
        }
        let items = data.data
        // console.log(items)
        // let cars = []
        items.forEach(async item => {
          // if (item.package) return

          let ddate = new Date(item.display_date)

          // console.log(
          //   'same : ',
          //   dayJs().format('YYYY-MM-DD'),
          //   '===',
          //   dayJs(ddate).format('YYYY-MM-DD'),
          //   dayJs(ddate).isSame(dayJs(), 'date')
          // )

          // if (!dayJs(ddate).isSameOrAfter('2019-12-30', 'year')) {
          if (req.date_type === 'after') {
            if (!dayJs(ddate).isAfter(dayJs(req.date_start), 'date')) {
              console.log(
                'sebelum tanggal 30',
                dayJs(ddate).format('YYYY-MM-DD')
              )
              return
            }
          }

          // ex requets : http://localhost:3333/olx?brand=mitsubishi&model=xpander&date_type=only&date_start=2020-01-13
          // scrap data only ddate after 2020-01-13 dan bukan ads
          if (req.date_type === 'only') {
            if (!dayJs(ddate).isAfter(dayJs(req.date_start), 'date')) {
              if (!item.package) {
                return
              }
            }
            // console.log(
            //   `${item.package}, is ${dayJs(ddate).format(
            //     'YYYY-MM-DD'
            //   )} is after ${req.date_start} `,
            //   dayJs(ddate).isAfter(dayJs(req.date_start))
            // )
          }

          try {
            let car = new Cars()
            car.car_id = item.id
            car.brand = req.brand
            car.model = req.model
            car.status = item.status.status
            car.favorite = item.favorites.count
            car.republish_date = new Date(item.republish_date)
            car.description = item.description
            car.title = item.title
            car.category_id = item.category_id
            car.price = item.price.value.raw
            car.valid_to = new Date(item.valid_to)
            car.views = item.views
            car.images = item.images.map(i => i.url).join(', ')
            car.country_id = item.locations_resolved.COUNTRY_id
            car.country_name = item.locations_resolved.COUNTRY_name
            car.province_id = item.locations_resolved.ADMIN_LEVEL_1_id
            car.province_name = item.locations_resolved.ADMIN_LEVEL_1_name
            car.district_id = item.locations_resolved.ADMIN_LEVEL_3_id
            car.district_name = item.locations_resolved.ADMIN_LEVEL_3_name
            car.city_id = item.locations_resolved.SUBLOCALITY_LEVEL_1_id
            car.city_name = item.locations_resolved.SUBLOCALITY_LEVEL_1_name
            car.display_date = new Date(item.display_date)
            car.created_at_first = new Date(item.created_at_first)
            item.parameters.forEach(i => {
              if (i.key === 'm_tipe_variant') {
                car.type = i.value
              }
              if (i.key === 'm_year') {
                car.year = i.value
              }
              if (i.key === 'mileage') {
                car.mileage = i.value
              }
              if (i.key === 'm_fuel') {
                car.fuel = i.value
              }
              if (i.key === 'm_color') {
                car.color = i.value
              }
              if (i.key === 'm_transmission') {
                car.transmission = i.value
              }
              if (i.key === 'm_drivetrain') {
                car.drivetrain = i.value
              }
              if (i.key === 'm_engine_capacity') {
                car.engine_capacity = i.value
              }
              if (i.key === 'm_feature') {
                car.feature = i.values.map(i => i.value).join(', ')
              }
            })
            await car.save()
            Logger.info({
              ...req,
              ads: !!item.package,
              car_id: item.id,
              status: 'success'
            })
          } catch (e) {
            Logger.info({
              ...req,
              ads: !!item.package,
              car_id: item.id,
              status: 'error'
            })
            // console.log('error when insert database ', item.id)
          }
        })
      }

      return { ...req, status: 'done', msg: '' }
    } catch (e) {
      return { ...req, status: 'error', msg: e.message }
    }
  }

  async getBrand ({ request, response }) {
    let g = await Database.raw(
      `select brand as label, brand as value
      from cars
      group by brand
      order by brand asc
    `
    )
    // console.log(g)
    response.send(g[0])
  }

  async getModel ({ request, response }) {
    let field = request.all()
    let g = await Database.raw(
      `select model as label, model as value
      from cars
      where brand=?
      and model is not null
      group by model
      order by model asc
    `,
      [field.brand]
    )
    // console.log(g)
    response.send(g[0])
  }

  async getType ({ request, response }) {
    let field = request.all()
    let g = await Database.raw(
      `select type as label, type as value
      from cars
      where model=?
      and type is not null
      group by type
      order by type asc
    `,
      [field.model]
    )
    // console.log(g)
    response.send(g[0])
  }

  async getYear ({ request, response }) {
    let field = request.all()
    let g = await Database.raw(
      `select year as label, year as value
      from cars
      where type=?
      group by year
      order by year asc
    `,
      [field.type]
    )
    // console.log(g)
    response.send(g[0])
  }

  async getTransmission ({ request, response }) {
    let field = request.all()
    let g = await Database.raw(
      `select transmission as label, transmission as value
      from cars
      where type=? and year=?
      group by transmission
      order by transmission asc
    `,
      [field.type, field.year]
    )
    response.send(g[0])
  }

  async get ({ request, response }) {
    let field = request.all()
    // group by DATE(display_date)
    let g = await Database.raw(
      `select id, car_id, seller_type, bursa_mobil, varian, color, user_id, DATE_FORMAT(display_date,'%d %b %y') as display_date, type, type as showtype, year, created_at_first, COUNT(id) as total_row, price, round(avg(price)) as price_avg1, price as price_average
      from cars
      where type=?
      and year=?
      and transmission=?
      and DATE(display_date) >= ?
      and DATE(display_date) <= ?
      group by id
      order by DATE(display_date) asc
    `,
      [
        field.type,
        field.year,
        field.transmission,
        field.startDate,
        field.endDate
      ]
    )
    // return response.send(g)

    let label = []
    let groupBy = function (data, keyFunction) {
      var groups = {}
      data.forEach(function (el) {
        el.showtype =
          el.showtype.replace(/\d/gi, '').replace(/-{1,2}/gi, ' ') +
          ' ' +
          el.year
        var key = el[keyFunction]
        if (key in groups === false) {
          groups[key] = []
        }
        // groups[key].push({ name: el.type, price: el.price, date: el.display_date })
        groups[key].push({ price: Number(el.price), total: el.total_row })
      })
      return Object.keys(groups).map(function (key) {
        // console.log(groups[key])
        let getPrice = groups[key].map(i => i.price)
        // let getTotal = groups[key].map(i => i.total)
        return {
          name: `${key.replace(/\d/gi, '').replace(/-{1,2}/gi, ' ')}`,
          chartType: 'line',
          values: getPrice
          // total: getTotal
        }
      })
    }

    g[0].forEach(i => {
      label.push(i.display_date)
    })

    if (label.length === 1) {
      label.push(' ')
    }

    let result = {}
    result.visibleColumns = [
      'car_id',
      'user_id',
      // 'total_row',
      'seller_type',
      'showtype',
      'display_date',
      'price_average',
      'action'
    ]
    result.columns = [
      {
        name: 'car_id',
        label: 'ID',
        field: 'car_id',
        sortable: false
      },
      {
        name: 'user_id',
        label: 'User ID',
        field: 'user_id',
        sortable: false
      },
      {
        name: 'seller_type',
        label: 'Tipe Penjual',
        field: 'seller_type',
        sortable: false
      },
      {
        name: 'total_row',
        label: 'Jumlah Penjual',
        field: 'total_row',
        sortable: false,
        align: 'center'
      },
      {
        name: 'showtype',
        label: 'Tipe Mobil',
        field: 'showtype',
        align: 'center'
      },
      {
        name: 'display_date',
        label: 'Tanggal Jual',
        field: 'display_date',
        align: 'center',
        sortable: true
      },
      {
        name: 'price_average',
        label: 'Harga Jual',
        field: 'price_average',
        sortable: true
      },
      { name: 'action', label: 'Action', field: 'id', align: 'center' }
    ]
    result.data = g[0]
    result.pagination = {
      rowsPerPage: 20
      // rowsNumber: xx if getting data from a server
    }

    let info = await Database.raw('select MAX(created_at) as last_data, MIN(created_at) as first_data from cars')
    // result.info = info[0][0]

    response.send({
      info: info[0][0],
      label,
      data: groupBy(g[0], 'type'),
      tables: result
    })
  }

  async getTable ({ request, response }) {
    try {
      let Car = await use(`App/Models/Car`)
      let res = await Car.query()
        .select(['id', 'brand', 'model', 'type', 'year', 'price'])
        .from('cars AS c')
        .orderBy('c.model', 'desc')
        .where('c.model', '=', 'agya')
        // .andWhere(function() {
        //   if (request.input("channel_category_id")) {
        //     this.where(
        //       "channel_category_id",
        //       request.input("channel_category_id")
        //     );
        //   }
        // })
        .paginate()
      let result = res.toJSON()
      result.visibleColumns = [
        'id',
        'brand',
        'model',
        'type',
        'year',
        'price',
        'action'
      ]
      result.columns = [
        { name: 'id', label: 'ID', field: 'id', align: 'center' },
        { name: 'brand', label: 'Brand', field: 'brand' },
        { name: 'model', label: 'Model', field: 'model' },
        { name: 'type', label: 'type', field: 'type' },
        { name: 'year', label: 'year', field: 'year' },
        { name: 'price', label: 'price', field: 'price' },
        { name: 'created_at', label: 'Created At', field: 'created_at' },
        { name: 'updated_at', label: 'Updated At', field: 'updated_at' }
        // { name: 'action', label: 'Action', field: 'id', align: 'center' }
      ]
      if (!res) {
        response.send({
          __status: 'failed_load_margin_rate',
          __message: 'failed load Margin Rate',
          __error: ''
        })
      } else {
        response.send({
          __status: 'success_load_margin_rate',
          __message: 'berhasil memuat Margin Rate',
          __error: '',
          result: result
        })
      }
    } catch (e) {
      response.send({
        __status: 'error_load_margin_rate',
        __message: e.message
      })
    }
  }

  async destroy ({ request, response }) {
    try {
      let car = await use(`App/Models/car`)
      let res = await car.find(request.input('id'))
      res.delete()
      if (!res) {
        response.send({
          __status: 'failed_destroy_car',
          __message: 'gagal menghapus car',
          __error: ''
        })
      } else {
        response.send({
          __status: 'success_destroy_car',
          __message: 'berhasil mengapus car',
          __error: ''
        })
      }
    } catch (e) {
      response.send({
        __status: 'error_destroy_car',
        __message: e.message
      })
    }
  }
}

module.exports = OlxController
