
'use strict'
const CrudController = use('CrudController')
const Event = use('Event')
const Env = use('Env')
const moment = require('moment')
const Database = use('Database')
class ProgramController extends CrudController {
  constructor () {
    super('Program')
    this.dealerType = [
      { label: 'Brotherhood', value: 'RDR' },
      { label: 'Non Brotherhood', value: 'RCD' }
    ]
  }
  async field () {
    let area = use('App/Models/Area')
    let Area = (await area.query().where({ area_status: 1 }).fetch()).toJSON()
    console.log(Area)
    this.areaDropdown = Area.map(i => {
      return {
        label: i.area_name,
        value: i.id
      }
    })
    let GET_URL =
      process.env.NODE_ENV === 'production' ? process.env.IMG_URL : `/api`

    return [
      {
        name: 'programs.id',
        field: 'id',
        label: 'ID',
        type: 'number',
        sortable: true,
        search: true
      },
      {
        name: 'programs.program_status',
        field: 'program_status',
        type: 'integer',
        label: 'Program Status',
        sortable: true,
        search: true,
        dropdown: [
          { label: 'Active', value: 1 },
          { label: 'Inactive', value: 0 }
        ]
      },
      {
        name: 'programs.program_picture',
        field: 'program_picture',
        type: 'string',
        label: 'Program Picture',
        format: `(val) => val && \`<img class="show-img" data-img=\${JSON.stringify(val)} src="\${val.indexOf('http://') !== -1 ? val : \`${GET_URL}/uploads/\${val.split(',')[0]}\`}" width="30px" />\``,
        sortable: false,
        search: false
      },
      {
        name: 'programs.program_name',
        field: 'program_name',
        type: 'string',
        label: 'Program Name',
        sortable: true,
        search: true
      },
      {
        name: 'programs.area_id',
        format: `(val) => val && val.area_name`,
        field: 'Area',
        type: 'string',
        label: 'Area',
        sortable: true,
        search: true,
        dropdown: this.areaDropdown
      },
      // {
      //   name: 'program_package',
      //   format: `(val) => val && val`,
      //   field: 'area_id',
      //   type: 'integer',
      //   label: 'Program Package',
      //   sortable: true,
      //   search: true
      // },
      {
        name: 'programs.program_period_start',
        field: 'program_period_start',
        type: 'date',
        label: 'Program Period Start',
        sortable: true,
        search: false
      },
      {
        name: 'programs.program_period_finish',
        field: 'program_period_finish',
        type: 'date',
        label: 'Program Period Start',
        sortable: true,
        search: false
      },
      // {
      //   name: 'programs.program_description',
      //   field: 'program_description',
      //   type: 'text',
      //   label: 'Program Description',
      //   sortable: true,
      //   search: false
      // },
      // {
      //   name: 'programs.program_term_condition',
      //   field: 'program_term_condition',
      //   type: 'text',
      //   label: 'Term and Condition',
      //   sortable: true,
      //   search: false
      // },
      {
        name: 'programs.program_growth',
        field: 'program_growth',
        type: 'string',
        label: 'program growth',
        sortable: true,
        search: false
      },
      {
        name: 'programs.program_type',
        field: 'program_type',
        type: 'string',
        label: 'Program type',
        sortable: true,
        search: true,
        dropdown: [
          { label: 'Apresiasi', value: 1 },
          { label: 'Trip', value: 2 }
        ]
      },
      {
        name: 'programs.program_dealer_type',
        field: 'program_dealer_type',
        type: 'string',
        label: 'Program dealer type',
        sortable: true,
        search: true,
        dropdown: this.dealerType,
        format: `(val) => \`<span class="text-green text-bold">\${${JSON.stringify(this.dealerType)}.filter(i => i.value === val)[0].label}</span>\``
      },
      {
        name: 'programs.program_semester',
        field: 'program_semester',
        type: 'string',
        label: 'program semester',
        sortable: true,
        search: false
      },
      // {
      //   name: 'program_roa',
      //   field: 'program_roa',
      //   type: 'string',
      //   label: 'program roa',
      //   sortable: true,
      //   search: false
      // },
      // {
      //   name: 'program_fid',
      //   field: 'program_fid',
      //   type: 'string',
      //   label: 'program fid',
      //   sortable: true,
      //   search: false
      // },
      {
        name: 'programs.created_at',
        field: 'created_at',
        label: 'Date Created',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'programs.updated_at',
        field: 'updated_at',
        label: 'Last Updated',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'action',
        field: 'action',
        label: 'Action',
        type: 'action',
        sortable: false,
        search: false
      }
    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const Program = await use('App/Models/Program')
    let query = Program.query()
      .from('programs')
      .select([
        'programs.id',
        'programs.area_id',
        'programs.program_name',
        'programs.program_picture',
        'programs.program_period_start',
        'programs.program_period_finish',
        'programs.program_period_finish as finish',
        'programs.program_description',
        'programs.program_term_condition',
        'programs.program_growth',
        'programs.program_type',
        'programs.program_dealer_type',
        'programs.program_semester',
        'programs.program_roa',
        'programs.program_fid',
        'programs.program_status',
        'programs.program_status as status',
        'programs.created_at',
        'programs.updated_at'
      ])
      .with('Area')
      .with('ProgramPackage')
      .leftJoin('areas', 'programs.area_id', 'areas.id')
      // .leftJoin('program_packages', 'programs.id', 'program_packages.program_id')
      .where(builder => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('programs.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'in') {
                builder.whereIn(p[0], p[1].split('-'))
              } else if (p[2] === 'month') {
                // builder.whereRaw(`DATE_FORMAT(NOW(),'%Y-%m') >= DATE_FORMAT(program_period_start, '%Y-%m')`)
                // builder.whereRaw(`DATE_FORMAT(program_period_finish, '%Y-%m') >= DATE_FORMAT(NOW(),'%Y-%m')`)
                builder.whereRaw(`DATE_FORMAT(NOW(),'%Y-%m') >= DATE_FORMAT(program_period_start, '%Y-%m')`)
                builder.whereRaw(`DATE_FORMAT(NOW(),'%Y-%m') <= DATE_FORMAT(program_period_finish, '%Y-%m')`)
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'programs.updated_at desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return Program;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }

  async index ({ request, params, auth, response }) {
    try {
      let res = await this.data({ request, params, auth })
      let result = res.toJSON()

      let orderStatus = ['inactive', 'active']
      if (request.input('show') === 'all') {
        result = result.map(i => {
          if (request.input('export')) {
            i.program_status = orderStatus[i.program_status]
            // i.program.Area = i.program.Area.area_name
            i.Area = i.Area.area_name
            i.program_growth = i.program_growth === 1 ? 'pesimis' : 'agresif'
            i.program_type = i.program_type === 2 ? 'trip' : 'apresiasi'
            i.program_period_start = moment(i.program_period_start).format('MMM YYYY')
            i.program_period_finish = moment(i.program_period_finish).format('MMM YYYY')

            if (i.program_picture) {
              i.program_picture = Env.get('IMG_URL') + '/uploads/' + i.program_picture
            }

            delete i.area_id
            delete i.program_roa
            delete i.program_fid
            delete i.ProgramPackage
          }
          return i
        })
      } else {
        result.data = result.data.map(i => {
          let colorStatus =
            i.program_status === 1
              ? 'text-green'
              : 'text-red'

          i.program_status = `<span class="${colorStatus} text-bold">${
            orderStatus[i.program_status]
          }</span>`

          i.program_growth = i.program_growth === 1 ? 'pesimis' : 'agresif'
          i.program_type = i.program_type === 2 ? 'trip' : 'apresiasi'
          i.program_period_start = moment(i.program_period_start).format(
            'MMM YYYY'
          )
          i.program_period_finish = moment(i.program_period_finish).format(
            'MMM YYYY'
          )

          return i
        })
      }

      result.pagination = {
        sortBy: request.input('sortBy'),
        page: result.page,
        rowsPerPage: result.perPage,
        rowsNumber: result.total,
        // eslint-disable-next-line no-extra-boolean-cast
        descending: request.input('descending') === 'true'
      }

      result.columns = await this.field({ request })
      result.invisibleColumnRevision = this.invisibleColumnRevision
      result.invisibleColumns = this.invisibleColumns

      if (!res) {
        response.send({
          __status: `failed_load_${this.modelLowerCase}`,
          __message: `failed load ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        let params = {
          __status: `success_load_${this.modelLowerCase}`,
          __message: `berhasil memuat ${this.modelLowerCase}`,
          __error: ``
        }
        params[this.modelPluralize] = result
        response.send(params)
      }
    } catch (e) {
      response.send({
        __status: `error_load_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async edit ({ params, response, auth }) {
    try {
      let res = await use(`App/Models/${this.model}`).find(params.id)
      let result = res.toJSON()

      if (!res) {
        response.send({
          __status: `failed_edit_${this.modelLowerCase}`,
          __message: `failed edit ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        response.send({
          __status: `success_edit_${this.modelLowerCase}`,
          __message: `berhasil edit ${this.modelLowerCase}`,
          __error: ``,
          ...result
        })
      }
    } catch (e) {
      response.send({
        __status: `error_edit_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async store ({ request, response, session }) {
    const Database = use('Database')
    const trx = await Database.beginTransaction()
    try {
      let uploadFile = await use(`App/Models/${this.model}`).fieldForUploadFile
      let data = request.except(['_csrf'])

      // let result = await this.handleUpload(uploadFile, data, 'create', request)
      // if (result.status === 'error') {
      //   return response.send(result.data)
      // } else {
      //   data = result.data
      // }
      let result = await this.handleUpload(uploadFile, data, 'create', request)
      if (result.status === 'error') {
        return response.send(result.data)
      } else {
        data = result.data
      }

      let packages = data.packages
      delete data.packages

      let program = await use(`App/Models/${this.model}`).create(data, trx)
      console.log('packages ', packages)
      packages = packages.map(i => {
        i.program_id = program.id
        return i
      })
      await use(`App/Models/ProgramPackage`).createMany(packages, trx)
      await trx.commit()

      response.send({
        __status: `success_create_${this.modelLowerCase}`,
        __message: `create ${this.modelLowerCase} success`,
        __error: ``,
        ...data
      })
    } catch (e) {
      // if something gone wrong
      await trx.rollback()
      console.log('ERROR', e)
      response.send({
        __status: `error_create_${this.modelLowerCase}`,
        __message: `error when create ${this.modelLowerCase}, please call administrator`,
        __error: e
      })
    }
  }

  async update ({ request, response, session, params }) {
    // const Database = use('Database')
    // const trx = await Database.beginTransaction()
    try {
      let uploadFile = await use(`App/Models/${this.model}`).fieldForUploadFile
      let data = request.except(['_csrf'])

      let result = await this.handleUpload(uploadFile, data, 'update', request)
      if (result.status === 'error') {
        return response.send(result.data)
      } else {
        data = result.data
      }

      let packages = data.packages
      delete data.packages

      let Mdl = await use(`App/Models/${this.model}`)
      // let mdl = await Mdl.firstOrFail(params.id)
      // mdl.merge(...data)
      // await mdl.save()
      await Mdl.query()
        .where({ id: params.id })
        .update(data)

      packages = packages.map(i => {
        if (i.program_package_name === 'null') delete i.program_package_name
        i.program_id = params.id
        return i
      })
      await Database.raw('SET FOREIGN_KEY_CHECKS=0')
      await use(`App/Models/ProgramPackage`)
        .query()
        .delete()
        .where({ program_id: params.id })
      await use(`App/Models/ProgramPackage`).createMany(packages)
      await Database.raw('SET FOREIGN_KEY_CHECKS=1')
      // await trx.commit()]
      response.send({
        __status: `success_update_${this.modelLowerCase}`,
        __message: `update ${this.modelLowerCase} success`,
        ...data
      })
    } catch (e) {
      console.log('error', e)
      // await trx.rollback()
      response.send({
        __status: `error_update_${this.modelLowerCase}`,
        __message: `error when update ${this.modelLowerCase}, please call administrator`,
        __error: e
      })
    }
  }

  async daftar ({ request, auth, response }) {
    let ProgramOrders = await use('App/Models/ProgramOrder')
    let req = request.all()

    let cek = await ProgramOrders.query()
      .with('Program')
      .where('user_id', auth.user.id)
      .where('program_id', req.program_id)
      .first()

    /* validasi jika telah mengikuti program yang sama pada 1 semester */
    if (cek) {
      if (cek.program_order_status === 3) {
        return response.send({
          __status: `failed_daftar_program`,
          __message: `Anda telah terdaftar dalam program ini, status pada saat ini menunggu approval Administrator`,
          ...request.all()
        })
      } else if (cek.program_order_status === 2) {
        return response.send({
          __status: `failed_daftar_program`,
          __message: `Anda telah mengikuti program ini`,
          ...request.all()
        })
      } else if (cek.program_order_status === 4) {
        return response.send({
          __status: `failed_daftar_program`,
          __message: `Program anda tidak disetujui`,
          ...request.all()
        })
      } else {
        return response.send({
          __status: `error_daftar_program`,
          __message:
            'terjadi kesalahan pada saat mendaftar, status tidak diketahui',
          ...request.all()
        })
      }
    }

    /* validasi jika telah mengikuti program lebih dari 2 kali untuk tipe, semeser dan tahun yang sama */
    let getExistingOrder = (await ProgramOrders.query()
      .select('pr.program_semester', 'pr.program_type', Database.raw(`DATE_FORMAT(pr.created_at, '%Y') as tahun`))
      .from('program_orders as po')
      .where('user_id', auth.user.id)
      .whereRaw(`DATE_FORMAT(po.created_at, '%Y') = DATE_FORMAT(NOW(), '%Y')`)
      .leftJoin('programs as pr', 'po.program_id', 'pr.id')
      .fetch()).toJSON()

    let limit = []
    for (let i of getExistingOrder) {
      let getProgram = await use('App/Models/Program').find(req.program_id)
      let year = new Date(getProgram.created_at).getFullYear()

      if (Number(getProgram.program_type) === Number(i.program_type) && Number(getProgram.program_semester) === Number(i.program_semester) && Number(year) === Number(i.tahun)) {
        console.log(getProgram.program_type + '===' + i.program_type + '&&' + getProgram.program_semester + '===' + i.program_semester + '&&' + year + '===' + i.tahun)
        limit.push(i)
      }
    }

    if (limit.length > 0) {
      return response.send({
        __status: `failed_daftar_program`,
        __message: `Anda tidak boleh mengikuti program trip atau apresiasi lebih dari 2 kali pada semester ini`,
        getExistingOrder,
        limit
      })
    }

    let create = await ProgramOrders.findOrCreate(
      {
        user_id: auth.user.id,
        program_id: req.program_id
      },
      {
        user_id: auth.user.id,
        program_id: req.program_id,
        program_package_id: req.id,
        program_order_status: 3
      }
    )
    let i = create.toJSON()
    if (i.program_order_status === 3) {
      let data = (
        await ProgramOrders.query()
          .with('Program')
          .with('User')
          .where('id', i.id)
          .first()
      ).toJSON()

      Event.fire('email::daftar_program', {
        template: 'emails.daftarProgram',
        subject: 'Permintaan Pendaftaran Program',
        data: { data },
        from: Env.get('EMAIL_SUPPORT'),
        to: Env.get('EMAIL_SUPPORT')
      })
      Event.fire('email::daftar_program', {
        template: 'emails.updateDaftarProgram',
        subject: `Penganjuan Pendaftaran Program  ${data.Program.program_name}`,
        data: {
          data: data,
          text: `Hi ${data.User.dealer_name}, Terima kasih telah mengajukan pendaftaran program ${data.Program.program_name},  Kami akan mengirimkan email balasan apabila program anda telah disetujui.`
        },
        from: Env.get('EMAIL_SUPPORT'),
        to: data.User.email
      })
      let admin = await use('App/Models/User').findBy('user_role', 4)
      await use('App/Models/Notification').create({
        user_id: admin.id,
        notification_name: 'Approval Pendaftaran Program',
        notification_description: `Hi Admin, ${data.User.dealer_name} ingin mengikuti program ${data.Program.program_name}, segera lakukan approval`,
        notification_status: 1
      })
      await use('App/Models/Notification').create({
        user_id: data.User.id,
        notification_name: 'Approval Pendaftaran Program',
        notification_description: `Hi ${data.User.dealer_name}, Terima kasih telah mengajukan pendaftaran program ${data.Program.program_name}, kami akan segera mengirimkan email balasan untuk persetujuan program ini.`,
        notification_status: 1
      })
      response.send({
        __status: `success_daftar_program`,
        __message: `Terima kasih telah mengisi program, Kami akan mengirimkan email balasan apabila program anda telah disetujui.`,
        ...request.all()
      })
    } else if (i.program_order_status === 2) {
      response.send({
        __status: `success_daftar_program`,
        __message: `anda telah mengikuti program ini`,
        ...request.all()
      })
    } else if (i.program_order_status === 4) {
      response.send({
        __status: `failed_daftar_program`,
        __message: `Program anda tidak disetujui`,
        ...request.all()
      })
    } else {
      response.send({
        __status: `failed_daftar_program`,
        __message: `gagal datar program`,
        ...request.all()
      })
    }
  }

  async upgrade ({ request, auth, response }) {
    // let ProgramOrders = use('App/Models/ProgramOrder')
    // let ProgramReport = use('App/Models/ProgramReport')
    let req = request.all()
    let od = await use('App/Models/ProgramOrder')
      .query()
      .update({ program_package_id: req.id })
      .where('user_id', auth.user.id)
      .where('program_id', req.program_id)

    console.log('CEK', od)
    await use('App/Models/ProgramReport')
      .query()
      .where('program_order_id', od)
      .where('user_id', auth.user.id)
      .where('program_id', req.program_id)
      .update({
        program_package_id: req.id
      })

    response.send({
      __status: `success_update_program`,
      __message: `sukses update paket program`,
      ...request.all()
    })
  }
}

module.exports = ProgramController
