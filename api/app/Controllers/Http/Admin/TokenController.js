
'use strict'
const CrudController = use('CrudController')
class TokenController extends CrudController {
  constructor () {
    super('Token')
  }
  async field () {
    return [
      { name: 'tokens.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'username', format: `(val) => val && val.username`, field: 'User', type: 'string', label: 'User', sortable: true, search: true },
      { name: 'tokens.token', field: 'token', type: 'string', label: 'Token', sortable: true, search: true },
      { name: 'tokens.type', field: 'type', type: 'string', label: 'Type', sortable: true, search: true },
      { name: 'tokens.is_revoked', field: 'is_revoked', type: 'integer', label: 'Is Revoked', sortable: true, search: true },
      { name: 'tokens.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'tokens.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const token = await use('App/Models/Token')
    let query = token.query()
      .from('tokens')
      .select(['tokens.id', 'tokens.user_id', 'tokens.token', 'tokens.type', 'tokens.is_revoked', 'tokens.created_at', 'tokens.updated_at'])
      .with('User', builder => {
        return builder.select(['id', 'username'])
      })
      .leftJoin('users', 'tokens.user_id', 'users.id')
      .where((builder) => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('tokens.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'tokens.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return token;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }
}

module.exports = TokenController
