
'use strict'
const CrudController = use('CrudController')
const Event = use('Event')
const Env = use('Env')
const moment = require('moment')
class ProgramOrderController extends CrudController {
  constructor () {
    super('ProgramOrder')
  }
  async field () {
    return [
      {
        name: 'program_orders.id',
        field: 'id',
        label: 'ID',
        type: 'number',
        sortable: true,
        search: true
      },
      // {
      //   name: 'users.username',
      //   field: 'username',
      //   type: 'string',
      //   label: 'User',
      //   sortable: true,
      //   search: true
      // },
      {
        name: 'users.dealer_name',
        field: 'dealer',
        type: 'string',
        label: 'Dealer',
        sortable: true,
        search: true
      },
      {
        name: 'areas.area_name',
        field: 'area_name',
        type: 'string',
        label: 'Area',
        sortable: true,
        search: true
      },
      {
        name: 'program_orders.program_order_status',
        field: 'program_order_status',
        type: 'integer',
        label: 'Order Status',
        sortable: true,
        search: true,
        dropdown: [
          { label: 'Approve', value: 2 },
          { label: 'Pending', value: 3 },
          { label: 'Reject', value: 4 }
        ]
      },
      {
        name: 'programs.program_name',
        field: 'program_name',
        type: 'string',
        label: 'Program',
        sortable: true,
        search: true
      },
      {
        name: 'programs_package.program_package_type',
        field: 'package',
        type: 'string',
        label: 'Program Package',
        sortable: false,
        search: false
      },
      {
        name: 'programs_package.program_package_target',
        field: 'target',
        type: 'string',
        label: 'Target',
        sortable: true,
        search: true
      },
      {
        name: 'programs.program_semester',
        field: 'semester',
        type: 'string',
        label: 'Semester',
        sortable: true,
        search: true
      },

      {
        name: 'programs.program_period_start',
        field: 'period_start',
        type: 'date',
        label: 'Period Start',
        sortable: true,
        search: false
      },
      {
        name: 'programs.program_period_finish',
        field: 'period_finish',
        type: 'date',
        label: 'Period Finish',
        sortable: true,
        search: false
      },
      {
        name: 'program_orders.created_at',
        field: 'created_at',
        label: 'Date Created',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'program_orders.updated_at',
        field: 'updated_at',
        label: 'Last Update',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'action',
        field: 'action',
        label: 'Action',
        type: 'action',
        sortable: false,
        search: false
      }
    ]
  }

  async edit ({ params, response, auth }) {
    try {
      let res = await use(`App/Models/${this.model}`)
        .query()
        .with('ProgramPackage')
        .where('program_orders.id', params.id)
        .first()

      let result = res.toJSON()

      if (!res) {
        response.send({
          __status: `failed_edit_${this.modelLowerCase}`,
          __message: `failed edit ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        response.send({
          __status: `success_edit_${this.modelLowerCase}`,
          __message: `berhasil edit ${this.modelLowerCase}`,
          __error: ``,
          ...result
        })
      }
    } catch (e) {
      response.send({
        __status: `error_edit_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const programOrder = await use('App/Models/ProgramOrder')
    let query = programOrder
      .query()
      .from('program_orders')
      .select([
        'program_orders.id',
        'users.id as user_id',
        'users.username as username',
        'users.dealer_name as dealer',
        'areas.area_name as area_name',
        'programs.program_name as program_name',
        'programs.program_semester as semester',
        'programs.program_period_start',
        'programs.program_period_finish',
        'programs.program_period_start as period_start',
        'programs.program_period_finish as period_finish',
        'program_packages.program_package_target as target',
        'program_packages.program_package_type as package',
        'program_orders.program_id',
        'program_orders.program_package_id',
        'program_orders.program_order_status',
        'program_orders.program_order_status as order_status',
        'program_orders.created_at',
        'program_orders.updated_at'
      ])
      .with('User')
      .with('Program.Area')
      .with('Program.ProgramPackage')
      .with('ProgramPackage')
      .with('ProgramReport')
      .leftJoin('users', 'program_orders.user_id', 'users.id')
      .leftJoin('programs', 'program_orders.program_id', 'programs.id')
      .leftJoin('areas', 'programs.area_id', 'areas.id')
      .leftJoin(
        'program_packages',
        'program_orders.program_package_id',
        'program_packages.id'
      )
      .where(builder => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('program_orders.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'month') {
                builder.whereRaw(`DATE_FORMAT(NOW(),'%Y-%m') >= DATE_FORMAT(program_period_start, '%Y-%m')`)
                builder.whereRaw(`DATE_FORMAT(program_period_finish, '%Y-%m') >= DATE_FORMAT(NOW(),'%Y-%m')`)
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          // : 'FIELD(program_orders.program_order_status, 3, 1, 2), program_orders.updated_at desc'
          : 'program_orders.updated_at desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return programOrder;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }

  async index ({ request, params, auth, response }) {
    try {
      let res = await this.data({ request, params, auth })
      let result = res.toJSON()

      let orderStatus = ['inactive', 'active', 'approve', 'pending', 'reject']
      if (request.input('show') === 'all') {
        result = result.map(i => {
          i.program_order_status = orderStatus[i.program_order_status]
          if (request.input('export')) {
            i.program_order_status = orderStatus[i.program_order_status]
            i.period_start = moment(i.period_start).format('MMM YYYY')
            i.period_finish = moment(i.period_finish).format('MMM YYYY')
            i.package = i.ProgramPackage.program_package_type === 2 ? `Program trip : ${i.ProgramPackage.program_package_reward_trip}` : `Program apresiasi : ${i.ProgramPackage.program_package_reward_money}`
            delete i.program_period_start
            delete i.program_period_finish
            delete i.ProgramReport
            delete i.ProgramPackage
            delete i.User
            delete i.Program
            delete i.user_id
            delete i.program_id
            delete i.program_package_id
            delete i.package
          }
          return i
        })
      } else {
        result.data = result.data.map(i => {
          let colorStatus = i.program_order_status === 2 ? 'text-green' : i.program_order_status === 3 ? 'text-orange' : i.program_order_status === 4 ? 'text-red' : 'text-blue'
          i.program_order_status = `<span class="${colorStatus} text-bold">${orderStatus[i.program_order_status]}</span>`
          i.period_start = moment(i.period_start).format('MMM YYYY')
          i.period_finish = moment(i.period_finish).format('MMM YYYY')
          i.package =
            i.ProgramPackage.program_package_type === 2
              ? `Program trip : ${i.ProgramPackage.program_package_reward_trip}`
              : `Program apresiasi : ${i.ProgramPackage.program_package_reward_money}`
          return i
        })
      }

      result.pagination = {
        sortBy: request.input('sortBy'),
        page: result.page,
        rowsPerPage: result.perPage,
        rowsNumber: result.total,
        // eslint-disable-next-line no-extra-boolean-cast
        descending: request.input('descending') === 'true'
      }

      result.columns = await this.field({ request })
      result.invisibleColumnRevision = this.invisibleColumnRevision
      result.invisibleColumns = this.invisibleColumns

      if (!res) {
        response.send({
          __status: `failed_load_${this.modelLowerCase}`,
          __message: `failed load ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        let params = {
          __status: `success_load_${this.modelLowerCase}`,
          __message: `berhasil memuat ${this.modelLowerCase}`,
          __error: ``
        }
        params[this.modelPluralize] = result
        if (request.input('myProgram')) {
          // builder.whereRaw(`DATE_FORMAT(NOW(),'%Y-%m') >= DATE_FORMAT(program_period_start, '%Y-%m')`)
          // builder.whereRaw(`DATE_FORMAT(program_period_finish, '%Y-%m') >= DATE_FORMAT(NOW(),'%Y-%m')`)

          params['program_active'] = []
          params['program_history'] = []
          result.forEach(i => {
            let start = moment(i.period_start)
            let finish = moment(i.period_finish)
            let current = moment()
            if (finish.isBefore(current, 'month')) {
              params.program_history.push(i)
            } else {
              // momentjscom.readthedocs.io/en/latest/moment/05-query/06-is-between/
              if (current.isBetween(start, finish, 'month', '[]')) {
                params.program_active.push(i)
              }
            }
          })
        }
        response.send(params)
      }
    } catch (e) {
      response.send({
        __status: `error_load_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async update ({ request, response, session, params }) {
    try {
      let uploadFile = await use(`App/Models/${this.model}`).fieldForUploadFile
      let data = request.except(['_csrf'])

      let result = await this.handleUpload(uploadFile, data, 'update', request)
      if (result.status === 'error') {
        return response.send(result.data)
      } else {
        data = result.data
      }

      let po = (
        await use('App/Models/ProgramOrder').query()
          .with('Program')
          .with('User')
          .where('id', params.id)
          .first()
      ).toJSON()

      // data sudah diapprove
      if (Number(po.program_order_status) === 2) {
        return response.send({
          __status: `failed_update_${this.modelLowerCase}`,
          __message: `data program yang sudah dipprove tidak bisa dirubah`,
          __error: null
        })
      }

      if (Number(po.program_order_status) !== Number(data.program_order_status)) {
        let Mdl = await use(`App/Models/${this.model}`)
        let mdl = await Mdl.find(params.id)
        mdl.fill(data)
        await mdl.save()

        let status = [
          { label: 'Inactive', value: 0 },
          { label: 'Active', value: 1 },
          { label: 'Approve', value: 2 },
          { label: 'Pending', value: 3 },
          { label: 'Reject', value: 4 }
        ]

        let getStatus = status.filter(i => i.value === data.program_order_status)[0].label

        Event.fire('email::daftar_program', {
          template: 'emails.updateDaftarProgram',
          subject: `Update Pendaftaran Program  ${po.Program.program_name} status : ${getStatus}`,
          data: {
            data: po,
            text: `Hi ${po.User.dealer_name}, Terima kasih telah mengajukan pendaftaran program ${po.Program.program_name}, Pendaftaran anda telah di ${getStatus} silahkan login ke dealerprogram.id untuk melihat program anda.`
          },
          from: Env.get('EMAIL_SUPPORT'),
          to: po.User.email
        })
        let admin = await use('App/Models/User').findBy('user_role', 4)
        await use('App/Models/Notification').create({
          user_id: admin.id,
          notification_name: `Update Pendaftaran Program  ${po.Program.program_name} status : ${getStatus}`,
          notification_description: `Hi Admin, Update status : ${getStatus} untuk pendaftaran program ${po.Program.program_name} dealer ${po.User.dealer_name}.`,
          notification_status: 1
        })
        await use('App/Models/Notification').create({
          user_id: po.User.id,
          notification_name: `Update Pendaftaran Program  ${po.Program.program_name} status : ${getStatus}`,
          notification_description: `Hi ${po.User.dealer_name}, Terima kasih telah mengajukan pendaftaran program ${po.Program.program_name}, Pendaftaran anda telah di ${getStatus} silahkan login ke dealerprogram.id untuk melihat program anda.`,
          notification_status: 1
        })
      } else {
        console.log('Email sudah dikirim, parameterr program_status yang dikirim sama dengan yang ada didatabase, jangan kirim email lagi')
        let Mdl = await use(`App/Models/${this.model}`)
        let mdl = await Mdl.find(params.id)
        mdl.fill(data)
        await mdl.save()
      }

      response.send({
        __status: `success_update_${this.modelLowerCase}`,
        __message: `update ${this.modelLowerCase} success`,
        ...data
      })
    } catch (e) {
      response.send({
        __status: `error_update_${this.modelLowerCase}`,
        __message: `error when update ${this.modelLowerCase}, please call administrator`,
        __error: e.toString()
      })
    }
  }
}

module.exports = ProgramOrderController
