
'use strict'
const CrudController = use('CrudController')
class ProgramReportController extends CrudController {
  constructor () {
    super('ProgramReport')
  }
  async field () {
    return [
      { name: 'program_reports.id', field: 'id', label: 'ID', type: 'number', sortable: true, search: true },
      { name: 'username', format: `(val) => val && val.username`, field: 'User', type: 'string', label: 'User', sortable: true, search: true },
      { name: 'program_name', format: `(val) => val && val.program_name`, field: 'Program', type: 'string', label: 'Program', sortable: true, search: true },
      { name: 'id', format: `(val) => val && val.id`, field: 'ProgramOrder', type: 'string', label: 'Program Order', sortable: true, search: true },
      { name: 'program_reports.program_report_date', field: 'program_report_date', type: 'date', label: 'Program Report Date', sortable: true, search: true },
      { name: 'program_reports.program_report_total', field: 'program_report_total', type: 'integer', label: 'Program Report total', sortable: true, search: true },
      { name: 'program_reports.program_report_status', field: 'program_report_status', type: 'integer', label: 'Program Report Status', sortable: true, search: true },
      { name: 'program_reports.created_at', field: 'created_at', label: 'Date Created', type: 'date', sortable: true, search: true },
      { name: 'program_reports.updated_at', field: 'updated_at', label: 'last Update', type: 'date', sortable: true, search: true },
      { name: 'action', field: 'action', label: 'Action', type: 'action', sortable: false, search: false }

    ]
  }

  async data ({ request }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const programReport = await use('App/Models/ProgramReport')
    let query = programReport
      .query()
      .from('program_reports')
      .select([
        'program_reports.id',
        'program_reports.user_id',
        'program_reports.program_id',
        'program_reports.program_order_id',
        'program_reports.program_report_date',
        'program_reports.program_report_total',
        'program_reports.program_report_status',
        'program_reports.created_at',
        'program_reports.updated_at'
      ])
      .with('User', builder => {
        return builder.select(['id', 'username'])
      })
      .with('Program')
      .with('ProgramOrder', builder => {
        return builder.select(['id', 'id'])
      })
      .leftJoin('users', 'program_reports.user_id', 'users.id')
      .leftJoin('programs', 'program_reports.program_id', 'programs.id')
      .leftJoin(
        'program_orders',
        'program_reports.program_order_id',
        'program_orders.id'
      )
      .where(builder => {
        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'program_reports.program_report_date asc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return programReport;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }

  async updateReport ({ request, response }) {
    let req = request.all()
    let programReport = use('App/Models/ProgramReport')
    await programReport.query().where({ program_order_id: req.program_order_id }).delete()
    let res = await programReport.createMany(req.data)

    let params = {
      __status: `success_update_program_order`,
      __message: `Report Berhasil Di Update`,
      __error: ``
    }
    params['result'] = res
    response.send(params)
  }
}

module.exports = ProgramReportController
