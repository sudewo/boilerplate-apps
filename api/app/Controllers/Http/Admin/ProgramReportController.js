
'use strict'
const CrudController = use('CrudController')
class ProgramReportController extends CrudController {
  constructor () {
    super('ProgramReport')
  }
  async field () {
    return [
      {
        name: 'program_reports.id',
        field: 'id',
        label: 'ID',
        type: 'number',
        sortable: true,
        search: true
      },
      {
        name: 'username',
        format: `(val) => val && val.username`,
        field: 'User',
        type: 'string',
        label: 'User',
        sortable: true,
        search: true
      },
      {
        name: 'program_name',
        format: `(val) => val && val.program_name`,
        field: 'Program',
        type: 'string',
        label: 'Program',
        sortable: true,
        search: true
      },
      {
        name: 'program_package_name',
        format: `(val) => val && val.program_package_name`,
        field: 'ProgramPackage',
        type: 'string',
        label: 'Program Package',
        sortable: true,
        search: true
      },
      {
        name: 'id',
        format: `(val) => val && val.id`,
        field: 'ProgramOrder',
        type: 'string',
        label: 'Program Order',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.program_report_date',
        field: 'program_report_date',
        type: 'date',
        label: 'Program Report Date',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.program_report_total',
        field: 'program_report_total',
        type: 'integer',
        label: 'Program Report total',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.program_report_fid',
        field: 'program_report_fid',
        type: 'integer',
        label: 'Program Report fid',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.program_report_status',
        field: 'program_report_status',
        type: 'integer',
        label: 'Program Report Status',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.created_at',
        field: 'created_at',
        label: 'Date Created',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'program_reports.updated_at',
        field: 'updated_at',
        label: 'last Update',
        type: 'date',
        sortable: true,
        search: true
      },
      {
        name: 'action',
        field: 'action',
        label: 'Action',
        type: 'action',
        sortable: false,
        search: false
      }
    ]
  }

  async data ({ request, auth }) {
    const isPivot = []
    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    // console.log('ORDER QUERY', order)
    const programReport = await use('App/Models/ProgramReport')
    let query = programReport
      .query()
      .from('program_reports')
      .select([
        'program_reports.id',
        'program_reports.user_id',
        'program_reports.program_id',
        'program_reports.program_package_id',
        'program_reports.program_order_id',
        'program_reports.program_report_date',
        'program_reports.program_report_total',
        'program_reports.program_report_fid',
        'program_reports.program_report_status',
        'program_reports.created_at',
        'program_reports.updated_at'
      ])
      .with('User')
      .with('Program')
      .with('ProgramPackage')
      .with('ProgramOrder', builder => {
        return builder.select(['id', 'id'])
      })
      .leftJoin('users', 'program_reports.user_id', 'users.id')
      .leftJoin('programs', 'program_reports.program_id', 'programs.id')
      .leftJoin(
        'program_packages',
        'program_reports.program_package_id',
        'program_packages.id'
      )
      .leftJoin(
        'program_orders',
        'program_reports.program_order_id',
        'program_orders.id'
      )
      .where(builder => {
        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('program_reports.id', auth.user.id)
          }
        }

        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (isPivot.indexOf(p[0]) < 0) {
              if (['integer', 'number'].includes(p[2])) {
                builder.where(p[0], p[1])
              } else if (p[2] === 'date') {
                builder.whereRaw(`date(${p[0]})='${p[1]}'`)
              } else {
                builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
              }
            }
          }
        }
      })
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'program_reports.id desc'
      )
    // .paginate(request.input("page") || 1, request.input("limit") || 10)
    // return programReport;

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }

  async updateReport ({ request, response }) {
    let req = request.all()
    let programReport = use('App/Models/ProgramReport')
    await programReport
      .query()
      .where({ program_order_id: req.program_order_id })
      .delete()
    let res = await programReport.createMany(req.data)

    let params = {
      __status: `success_update_program_order`,
      __message: `Report Berhasil Di Update`,
      __error: ``
    }
    params['program_reports'] = res
    response.send(params)
  }
}

module.exports = ProgramReportController
