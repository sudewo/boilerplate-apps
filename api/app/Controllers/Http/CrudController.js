'use strict'

const Database = use('Database')
const pluralize = require('pluralize')

class CrudController {
  constructor (model, mode) {
    this.model = model
    this.mode = mode
    this.modelLowerCase = model
      .split(/(?=[A-Z])/)
      .join('_')
      .toLowerCase()
    this.modelPluralize = pluralize.plural(
      this.model
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()
    )
    this.validateUpload = {
      types: ['image'],
      size: '8mb'
    }

    this.invisibleColumnRevision = 22
    this.invisibleColumns = []
    this.superUser = 4
  }

  getField (obj) {
    var ordered = {}
    Object.keys(obj)
      .sort()
      .forEach(function (key) {
        obj[key]['name'] = key
        obj[key]['field'] = key

        if (['varchar', 'char', 'text'].includes(obj[key]['type'])) {
          obj[key]['type'] = 'string'
        } else if (['int', 'tinyint', 'smallint'].includes(obj[key]['type'])) {
          obj[key]['type'] = 'integer'
        } else if (['datetime', 'date'].includes(obj[key]['type'])) {
          obj[key]['type'] = 'date'
        } else {
          obj[key]['type'] = 'string'
        }

        obj[key]['sortable'] = true
        obj[key]['search'] = true
        obj[key]['label'] = key.replace(/_/gi, ' ')
        ordered[key] = obj[key]
      })

    let field = Object.values(ordered)
    let actionCol = Object.assign({}, field.filter(i => i.name === 'id')[0])
    actionCol.name = 'action'
    actionCol.type = 'action'
    actionCol.order = false
    actionCol.search = false
    actionCol.label = 'Actions'
    field.push(actionCol)

    return field
  }

  async field ({ request }) {
    const fld = await Database.table(this.modelPluralize).columnInfo()
    let field = this.getField(fld)
    return field
  }

  async data ({ request, params, auth }) {
    const data = await use(`App/Models/${this.model}`)

    let order = request.input('descending') === 'true' ? 'desc' : 'asc'
    let query = data
      .query()
      .orderByRaw(
        request.input('sortBy')
          ? `${request.input('sortBy')} ${order}`
          : 'id desc'
      )
      .where(builder => {
        if (request.input('search')) {
          let s = request.input('search').split(',')
          console.log('s', s)
          for (let i = 0; i < s.length; i++) {
            let p = s[i].split(':')
            if (['integer', 'number'].includes(p[2])) {
              builder.whereRaw(`${p[0]}=${p[1]}`)
            } else if (p[2] === 'date') {
              builder.whereRaw(`DATE(${p[0]}) ='${p[1]}'`)
            } else if (p[2] === 'datebetween') {
              let dd = p[1].split('---')
              // sqlite
              // builder.whereRaw(
              //   `strftime("%Y%m", ${p[0]}) >= strftime("%Y%m", "${dd[0]}")`
              // )
              // builder.whereRaw(
              //   `strftime("%Y%m", ${p[0]}) <= strftime("%Y%m", "${dd[1]}")`
              // )
              builder.whereRaw(`DATE_FORMAT(${p[0]}, "%Y-%m") >= "${dd[0]}"`)
              builder.whereRaw(`DATE_FORMAT(${p[0]}, "%Y-%m") <=  "${dd[1]}"`)
            } else {
              builder.whereRaw(`${p[0]} LIKE '%${p[1]}%'`)
            }
          }
        }

        if (this.model === 'User') {
          if (Number(auth.user.user_role) < this.superUser) {
            builder.where('id', auth.user.id)
          }
        }

        if (request.input('show') === 'all') {
          builder.where(`${this.modelLowerCase}_status`, 1)
        }
      })

    if (request.input('show') === 'all') {
      const ab = await query.fetch()
      return ab
    } else {
      const ac = await query.paginate(
        request.input('page') || 1,
        request.input('rowsPerPage') || 10
      )
      return ac
    }
  }

  // async show ({ params, request, view }) {
  //   if (this.mode === 'api') {
  //     const data = await use(`App/Models/${this.modelLowerCase}`).find(params.id)
  //     return data
  //   } else {
  //     return {
  //       data: await this.data({ request }),
  //       fields: await this.field({ request })
  //     }
  //   }
  // }

  async index ({ request, params, auth, response }) {
    try {
      let res = await this.data({ request, params, auth })
      let result = res.toJSON()
      result.pagination = {
        sortBy: request.input('sortBy'),
        page: result.page,
        rowsPerPage: result.perPage,
        rowsNumber: result.total,
        // eslint-disable-next-line no-extra-boolean-cast
        descending: request.input('descending') === 'true'
      }

      result.columns = await this.field({ request })
      result.invisibleColumnRevision = this.invisibleColumnRevision
      result.invisibleColumns = this.invisibleColumns

      if (!res) {
        response.send({
          __status: `failed_load_${this.modelLowerCase}`,
          __message: `failed load ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        let params = {
          __status: `success_load_${this.modelLowerCase}`,
          __message: `berhasil memuat ${this.modelLowerCase}`,
          __error: ``
        }
        params[this.modelPluralize] = result
        response.send(params)
      }
    } catch (e) {
      response.send({
        __status: `error_load_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async edit ({ params, response, auth }) {
    try {
      let res = await use(`App/Models/${this.model}`).find(params.id)
      let result = res.toJSON()

      if (!res) {
        response.send({
          __status: `failed_edit_${this.modelLowerCase}`,
          __message: `gagal edit ${this.modelLowerCase}`,
          __error: ``
        })
      } else {
        response.send({
          __status: `success_edit_${this.modelLowerCase}`,
          __message: `berhasil edit ${this.modelLowerCase}`,
          __error: ``,
          ...result
        })
      }
    } catch (e) {
      response.send({
        __status: `error_edit_${this.modelLowerCase}`,
        __message: e.message,
        __error: e
      })
    }
  }

  async store ({ request, response, session }) {
    try {
      let uploadFile = await use(`App/Models/${this.model}`).fieldForUploadFile
      let data = request.except(['_csrf'])

      let result = await this.handleUpload(uploadFile, data, 'create', request)
      if (result.status === 'error') {
        return response.send(result.data)
      } else {
        data = result.data
      }

      await use(`App/Models/${this.model}`).create(data)

      response.send({
        __status: `success_create_${this.modelLowerCase}`,
        __message: `create ${this.modelLowerCase} success`,
        __error: ``,
        ...data
      })
    } catch (e) {
      response.send({
        __status: `error_create_${this.modelLowerCase}`,
        __message: `error when create ${this.modelLowerCase}, please call administrator`,
        __error: e
      })
    }
  }

  async update ({ request, response, session, params }) {
    try {
      let uploadFile = await use(`App/Models/${this.model}`).fieldForUploadFile
      let data = request.except(['_csrf'])

      let result = await this.handleUpload(uploadFile, data, 'update', request)
      if (result.status === 'error') {
        return response.send(result.data)
      } else {
        data = result.data
      }

      let Mdl = await use(`App/Models/${this.model}`)
      // fill gak bisa update boolean value ex: area_status dan empty value kke ignore.
      if (this.model === 'User') {
        let mdl = await Mdl.firstOrFail(params.id)
        mdl.fill(data)
        await mdl.save()
      } else {
        await Mdl.query().where({ id: params.id }).update(data)
      }

      response.send({
        __status: `success_update_${this.modelLowerCase}`,
        __message: `update ${this.modelLowerCase} berhasil`,
        ...data
      })
    } catch (e) {
      response.send({
        __status: `error_update_${this.modelLowerCase}`,
        __message: `(${e.sqlMessage}) <br /><br /> error when update ${this.modelLowerCase}, please call administrator`,
        __error: e.toString()
      })
    }
  }

  async destroy ({ params, session, response }) {
    // try {
    let mdl = await use(`App/Models/${this.model}`)
    let res = await mdl
      .query(params.id)
      .whereIn('id', params.id.split(','))
      .delete()

    if (!res) {
      response.send({
        __status: `failed_destroy_${this.modelLowerCase}`,
        __message: `gagal menghapus ${this.modelLowerCase}`,
        __error: ``
      })
    } else {
      response.send({
        __status: `success_destroy_${this.modelLowerCase}`,
        __message: `berhasil mengapus ${this.modelLowerCase}`,
        __error: ``
      })
    }
    // } catch (e) {
    //   response.send({
    //     __status: `error_destroy_${this.modelLowerCase}`,
    //     __message: e.message,
    //     __error: e
    //   })
    // }
  }

  async handleUpload (uploadFile, data, op, request) {
    console.log('HANDLE UPLOAD UPLOAD FILE', uploadFile)
    if (uploadFile) {
      let uploadFileError = []
      let newdata = {}
      for (let filename of uploadFile) {
        let combineFilename = []
        let oldFile = request.input(`${filename}_old`)
        // jika ada file yang akan diupload, atau request mengandung files.
        if (Object.keys(request.files()).length > 0) {
          if (request.file(filename)) {
            let getUploadFile = await this.upload(
              request.file(filename, this.validateUpload)
            )
            console.log('GET UPLOADED FILE ', getUploadFile)

            if (!getUploadFile.status) {
              uploadFileError.push({ filename: getUploadFile.error })
            } else {
              combineFilename = getUploadFile.name
              if (oldFile && oldFile.length > 0) {
                combineFilename = combineFilename.concat(oldFile)
              }
            }
          }
        }

        // jika tidak ada filebaru yang akan diupload, update data
        if (combineFilename.length === 0) {
          if (oldFile && oldFile.length > 0) {
            combineFilename = oldFile
          }
        }

        for (let filename of uploadFile) {
          if (data[`${filename}_old`]) delete data[`${filename}_old`]
        }
        if (combineFilename.length > 0) {
          newdata[filename] = combineFilename.join(',')
        } else {
          newdata[filename] = null
        }
      }
      data = { ...data, ...newdata }

      if (uploadFileError.length > 0) {
        return {
          status: 'error',
          data: {
            __status: `failed_${op}_${this.modelLowerCase}`,
            __message: `${uploadFileError[0].filename}`,
            __error: uploadFileError
          }
        }
      }
    }
    return {
      status: 'success',
      data: data
    }
  }

  async upload (profilePics, arg = {}) {
    if (typeof profilePics._files !== 'undefined') {
      return this.uploadMultiple(profilePics, arg)
    }

    if (!profilePics.clientName) return { status: true, name: '' }

    const Helpers = use('Helpers')
    let name = arg.filename
      ? arg.filename
      : `${new Date().getTime()}-${profilePics.clientName}`

    await profilePics.move(
      Helpers.publicPath(arg.location ? arg.location : 'uploads'),
      {
        name
      }
    )

    if (!profilePics.moved()) {
      return { status: false, error: profilePics.error().message }
    }

    return { status: true, name: name }
  }

  async uploadMultiple (profilePics, arg = {}) {
    const Helpers = use('Helpers')
    const fs = require('fs')
    const path = require('path')

    await profilePics.moveAll(
      Helpers.publicPath(arg.location ? arg.location : 'uploads'),
      file => {
        return {
          name: arg.filename
            ? `${new Date().getTime()}-${arg.filename}`
            : `${new Date().getTime()}-${file.clientName}`
        }
      }
    )

    if (!profilePics.movedAll()) {
      // remove all file if one of image is failed to validate
      const movedFiles = profilePics.movedList()
      const removeFile = Helpers.promisify(fs.unlink)
      await Promise.all(
        movedFiles.map(file => {
          console.log('remove File ', file.fileName)
          return removeFile(
            path.join(
              Helpers.publicPath(arg.location ? arg.location : 'uploads'),
              file.fileName
            )
          )
        })
      )

      let errors = profilePics
        .errors()
        .map(i => `${i.clientName} : ${i.message}`)
      return { status: false, error: errors.join('<br/>\n') }
    } else {
      const movedFiles = profilePics.movedList()
      return { status: true, name: movedFiles.map(i => i.fileName) }
    }
  }
}

module.exports = CrudController
