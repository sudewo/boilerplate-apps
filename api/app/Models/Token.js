
'use strict'
const Model = use('Model')

class Token extends Model {
  // @todo loop json file then add dates based on date field
  // static get dates () {
  // return super.dates.concat(['dob'])
  // }

  static get visible () {
    // return ['title', 'body']
  }

  static get hidden () {
    // return ['password']
  }

  // for insert
  // static formatDates(field, value) {
  // if (field === 'created_at') {
  //     value.format('YYYY-MM-DD');
  // }
  // return super.formatDates(field, value);
  // }

  // formating date when displaying data
  static castDates (field, value) {
    if (field === 'created_at') {
      return value ? value.format('MMM, DD YYYY hh:mm a') : value
    } else return value ? value.format('MMM, DD YYYY hh:mm a') : value
  }

  User () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Token
