
'use strict'
const Model = use('Model')

class User extends Model {
  // remmber hook not working when using query().builder to update password
  static boot () {
    super.boot()
    this.addHook('afterUpdate', async userInstance => {
      if (userInstance.dirty.password) {
        userInstance.password = await use('Hash').make(userInstance.password)
      }
    })

    this.addHook('beforeSave', async userInstance => {
      if (userInstance.dirty.password) {
        userInstance.password = await use('Hash').make(userInstance.password)
      }
    })
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  static get fieldForUploadFile () {
    return ['avatar', 'cover']
  }

  // @todo loop json file then add dates based on date field
  // static get dates () {
  // return super.dates.concat(['dob'])
  // }

  static get visible () {
    // return ['title', 'body']
  }

  static get hidden () {
    return ['password']
  }

  // for insert
  // static formatDates(field, value) {
  // if (field === 'created_at') {
  //     value.format('YYYY-MM-DD');
  // }
  // return super.formatDates(field, value);
  // }

  // formating date when displaying data
  static castDates (field, value) {
    if (field === 'created_at') {
      return value ? value.format('MMM, DD YYYY hh:mm a') : value
    } else return value ? value.format('MMM, DD YYYY hh:mm a') : value
  }

  Area () {
    return this.belongsTo('App/Models/Area')
  }
  Branch () {
    return this.belongsTo('App/Models/Branch')
  }
}

module.exports = User
