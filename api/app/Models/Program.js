
'use strict'
const Model = use('Model')

class Program extends Model {
  // @todo loop json file then add dates based on date field
  static get dates () {
    return super.dates.concat(['program_period_start', 'program_period_finish'])
  }

  static get visible () {
    // return ['title', 'body']
  }

  static get hidden () {
    // return ['password']
  }

  static get fieldForUploadFile () {
    return ['program_picture']
  }

  // for insert
  // static formatDates(field, value) {
  // if (field === 'created_at') {
  //     value.format('YYYY-MM-DD');
  // }
  // return super.formatDates(field, value);
  // }
  Area () {
    return this.belongsTo('App/Models/Area')
  }
  ProgramPackage () {
    return this.hasMany('App/Models/ProgramPackage')
  }

  // formating date when displaying data
  static castDates (field, value) {
    if (field === 'created_at') {
      return value ? value.format('MMM, DD YYYY hh:mm a') : value
    } else return value ? value.format('MMM, DD YYYY hh:mm a') : value
  }
}

module.exports = Program
