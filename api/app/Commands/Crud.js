/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
/* eslint-disable no-useless-escape */
/* eslint-disable camelcase */
'use strict'

const { Command } = require('@adonisjs/ace')
const Helpers = use('Helpers')
const Drive = use('Drive')
const pluralize = require('pluralize')
const Database = use('Database')
const util = require('util')
const exec = util.promisify(require('child_process').exec)

const fs = require('fs')

class Crud extends Command {
  static get signature () {
    return 'crud'
  }

  static get description () {
    return 'Tell something helpful about this command'
  }

  capitalizeFirstLetter (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }
  lowercaseFirstLetter (string) {
    return string.charAt(0).toLowerCase() + string.slice(1)
  }

  setFieldPivot (i) {
    return `
     table.integer('${i.name}')${i.attribute ? `.${i.attribute}()` : ''}
     table
       .foreign('${i.name}')
       .references('${i.relations.references}')
       .inTable('${i.relations.inTable}')
       ${i.relations.onDelete ? `.onDelete('${i.relations.onDelete}')` : ''}
       ${i.relations.onUpdate ? `.onUpdate('${i.relations.onUpdate}')` : ''}
   `
  }

  setField (data) {
    let tpl = ''
    data.forEach(i => {
      if (i.relations) {
        if (i.type === 'pivot') return

        tpl += `
                table.${i.type}('${i.name}')${
  i.attribute ? `.${i.attribute}()` : ''
}
                table
                  .foreign('${i.name}')
                  .references('${i.relations.references}')
                  .inTable('${i.relations.inTable}')
                  ${
  i.relations.onDelete
    ? `.onDelete('${i.relations.onDelete}')`
    : ''
}
                  ${
  i.relations.onUpdate
    ? `.onUpdate('${i.relations.onUpdate}')`
    : ''
}
              `
      } else {
        // i.type = i.type === 'enu' && use('Env').get('DB_CONNECTION') === 'mysql' ? 'enu' : 'integer'
        tpl += `\n\t\t\ttable.${
          i.type === 'enu'
            ? use('Env').get('DB_CONNECTION') !== 'mysql'
              ? `integer('${i.name}')`
              : `${i.type}('${i.name}', [${i.enumValue}])`
            : i.length
              ? `${i.type}('${i.name}', ${i.length})`
              : `${i.type}('${i.name}')`
        }${i.nullable ? `.nullable()` : `.notNullable()`}${
          i.default || i.default === 0 ? `.defaultTo(${i.default})` : ''
        }${i.comment ? `.comment(${i.comment})` : ''}`
      }
    })
    return tpl
  }

  setFieldFactory (data) {
    let tpl = ''

    data.forEach(i => {
      if (i.relations) {
        let tableName = i.relations.model
          .split(/(?=[A-Z])/)
          .join('_')
          .toLowerCase()

        if (i.type === 'pivot') return

        tpl += `${i.name}: (await use('App/Models/${this.capitalizeFirstLetter(
          i.relations.model
        )}').query().where({${tableName}_status:true}).select('id').orderByRaw(rand).first()).id,\n\t`
      } else {
        tpl += `${i.name}:${i.faker !== '' ? i.faker : '""'},\n\t`
      }
    })
    return tpl
  }

  setFieldController (props) {
    let tpl = ''
    let alias = ''
    let relations = props.field.filter(i => typeof i.relations !== 'undefined')
    if (relations.length > 0) {
      alias =
        pluralize.plural(
          props.name
            .split(/(?=[A-Z])/)
            .join('_')
            .toLowerCase()
        ) + '.'
    }

    if (props.use_default_id) {
      tpl += `{ name: "${alias}id", field: "id", label: "ID", type: "number", sortable:true, search:true},\n`
    }

    let field
    props.field.forEach(i => {
      let order = true
      let search = true
      if (i.type === 'pivot') {
        order = false
        search = true
      }

      if (i.relations) {
        let k = JSON.parse(i.relations.formKey)
        if (i.type === 'pivot') {
          tpl += `{name:"${k[1]}", format: \`(val) => val && val.map(i => i.${k[1]}).join(', ')\`, field:"${i.relations.model}", type:"string", label: "${i.label}", sortable:${order}, search:${search}},\n`
        } else {
          tpl += `{name:"${k[1]}", format: \`(val) => val && val.${k[1]}\`, field:"${i.relations.model}", type:"string", label: "${i.label}", sortable:${order}, search:${search}},\n`
        }
      } else {
        if (i.type === 'enu') {
          tpl += `{name:"${alias}${i.name}", field:"${i.name}", type:"${
            i.type
          }", label: "${i.label}", enumData:${JSON.stringify(
            i.enumData
          )}, sortable:${order}, search:${search}},\n`
        } else {
          let type = i.type === 'boolean' ? 'integer' : i.type
          tpl += `{name:"${alias}${i.name}", field:"${i.name}", type:"${type}", label: "${i.label}", sortable:${order}, search:${search}},\n`
        }
      }
    })

    if (props.use_default_date) {
      tpl += `{ name: "${alias}created_at", field: "created_at", label: "Date Created", type: "date", sortable:true, search:true},\n`
      tpl += `{ name: "${alias}updated_at", field: "updated_at", label: "last Update", type: "date", sortable:true, search:true},\n`
    }

    tpl += `{ name: "action", field: "action", label: "Action", type: "action",sortable:false, search:false}\n`
    return tpl
  }

  setScopeQuery (data) {
    let tpl = ''
    data.forEach(i => {
      if (i.relations) {
        if (i.type === 'pivot') {
          let k = JSON.parse(i.relations.formKey)
          tpl += `static scopeBy${this.capitalizeFirstLetter(
            i.relations.model
          )}(query, search) {

            if (search) {
              // let s = search.split(',')
              if (search.indexOf('${k[1]}') !== -1) {
                query.whereHas('${this.capitalizeFirstLetter(
    i.relations.model
  )}', builder => {
                  if (search) {
                    let s = search.split(',')
                    for (let i = 0; i < s.length; i++) {
                      let p = s[i].split(':')
                      if (p[0] === '${k[1]}') {
                        builder.whereRaw(\`\${p[0]} LIKE '%\${p[1]}%'\`)
                      }
                    }
                  }
                })
              }
            }
          }\n`
        }
      }
    })
    return tpl
  }

  setRelations (data) {
    let tpl = ''
    data.forEach(i => {
      if (i.relations) {
        if (i.type === 'pivot') {
          tpl += `${this.capitalizeFirstLetter(i.relations.model)}() {
            return this.${
  i.relations.relationName
}('App/Models/${this.capitalizeFirstLetter(
  i.relations.model
)}').pivotModel('App/Models/${i.relations.pivotModel}')
          }\n`
        } else {
          tpl += `${this.capitalizeFirstLetter(i.relations.model)}() {
            return this.${
  i.relations.relationName
}('App/Models/${this.capitalizeFirstLetter(i.relations.model)}')
          }\n`
        }
      }
    })
    return tpl
  }

  migarationTemplate (props) {
    const tableName = pluralize.plural(
      props.name
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()
    )
    return {
      name: props.name,
      content: `
          'use strict'
          const Schema = use('Schema')
          class ${this.capitalizeFirstLetter(props.name)}Schema extends Schema {

            async up () {
              const exists = await this.hasTable('${tableName}');
              if(!exists){
                this.${props.type}('${tableName}', (table) => {
                  ${
  props.use_default_id
    ? `table
                    .increments()
                    .unsigned()
                    .primary();`
    : ''
}
                    ${this.setField(props.field)}
                  ${
  props.use_default_date
    ? `
    table.timestamps();`
    : ''
}
                })
              }
            }

            down () {
              this.drop('${tableName}')
            }
          }
          module.exports = ${this.capitalizeFirstLetter(props.name)}Schema
          `
    }
  }

  seederTemplate (props) {
    return {
      name: props.name,
      content: `
        'use strict'
        const Factory = use('Factory')

        class ${this.capitalizeFirstLetter(props.name)}Seeder {
          async run () {
            let ${this.capitalizeFirstLetter(
    props.name
  )} = await Factory.model('App/Models/${this.capitalizeFirstLetter(
  props.name
)}')${props.createRowSeed ? props.createRowSeed : ''}
            console.log(${this.capitalizeFirstLetter(props.name)});
          }
        }

        module.exports = ${this.capitalizeFirstLetter(props.name)}Seeder`
    }
  }

  bootMethodforUserModel () {
    return `
    //remmber hook not working when using query().builder to update password
    static boot () {
      super.boot()
      this.addHook('afterUpdate', async userInstance => {
        if (userInstance.dirty.password) {
          userInstance.password = await use('Hash').make(
            userInstance.password
          )
        }
      })

      this.addHook('beforeSave', async (userInstance) => {
        if (userInstance.dirty.password) {
          userInstance.password = await use('Hash').make(userInstance.password)
        }
      })
    }

    tokens() {
      return this.hasMany('App/Models/Token');
    }
    `
  }

  generateFieldForUploadFile (file) {
    let uploadFile = file
      .map((i, index) => {
        return `'${i.name}'`
      })
      .join(', ')

    let tpl = ` static get fieldForUploadFile() {
            return [${uploadFile}]
        }`
    return tpl
  }

  modelTemplate (props) {
    let isFile = props.field.filter(i => i.formType === 'file')

    return {
      name: props.name,
      content: `
      'use strict'
        const Model = use('Model')

        class ${this.capitalizeFirstLetter(props.name)} extends Model {


            ${props.name !== 'user' ? '' : this.bootMethodforUserModel()}

            ${isFile.length > 0 ? this.generateFieldForUploadFile(isFile) : ''}

            //@todo loop json file then add dates based on date field
            //static get dates () {
              //return super.dates.concat(['dob'])
            //}

            static get visible () {
              // return ['title', 'body']
            }


            static get hidden () {
              ${
  props.name !== 'user'
    ? `// return ['password']`
    : `return ['password']`
}
            }

            ${this.setScopeQuery(props.field)}

            //for insert
            // static formatDates(field, value) {
              // if (field === 'created_at') {
              //     value.format('YYYY-MM-DD');
              // }
              // return super.formatDates(field, value);
            // }

            //formating date when displaying data
              static castDates (field, value) {
                if (field === 'created_at') {
                  return value ? value.format('MMM, DD YYYY hh:mm a') : value
                } else return value ? value.format('MMM, DD YYYY hh:mm a') : value
              }

            ${this.setRelations(props.field)}
        }

        module.exports = ${this.capitalizeFirstLetter(props.name)}`
    }
  }

  factoryTemplate (props) {
    let tpu = ''
    let isContainRelation = props.field.filter(i => i.relations)

    return {
      name: props.name,
      content: `
use('Factory').blueprint('App/Models/${this.capitalizeFirstLetter(
    props.name
  )}', async (faker) => {
        ${
  isContainRelation.length > 0
    ? `let rand = use('Env').get('DB_CONNECTION') === 'mysql' ? 'RAND()' : 'RANDOM()';`
    : ''
}

        return {
          ${this.setFieldFactory(props.field)}
        }
      })
      `
    }
  }

  async setControllerQuery (props, type) {
    let relations = props.field.filter(i => typeof i.relations !== 'undefined')
    let tpl = ''
    let variables = []
    // var status;
    relations.forEach(i => {
      if (i.type === 'pivot') {
        variables.push(i.relations.model)

        // tpl += `const ${i.relations.model} = (await use('App/Models/${
        //   i.relations.model
        // }')
        // .query()
        // .select(${i.relations.formKey})
        // .where('${pluralize.singular(i.relations.inTable)}_status', '1')
        // .fetch()).toJSON();\n\n`

        if (type === 'edit') {
          tpl += `let Mdl = await use('App/Models/${this.capitalizeFirstLetter(
            props.name
          )}').find(params.id);
          let ${i.relations.model}Selected = await Mdl.${
  i.relations.model
}().fetch()
          Mdl.${i.name} = ${
  i.relations.model
}Selected.toJSON().map(i => i.id);\n\n`

          tpl += `response.send({
              __status: \`success_edit_${props.name}\`,
              __message: \`create ${props.name} success\`,
              __error: null,
              __version: null,
              __validation:null,
              ...Mdl.toJSON()
            })`
          variables.push(`${i.relations.model}Selected`)
        }
      } else {
        // var status = pluralize.singular(i.relations.inTable)
        // variables.push(i.relations.model)
        // tpl += `const ${i.relations.model} = (await use('App/Models/${i.relations.model}')
        //       .query()
        //       .select(${i.relations.formKey})
        //       .where("${status}_status", 1)
        //       .fetch()).toJSON();\n
        //     `
      }
    })

    return {
      query: tpl,
      variables: variables
    }
  }

  async setControllerUpdate (props) {
    const isFile = props.field.filter(i => i.formType === 'file')
    const pivot = props.field.filter(i => i.type === 'pivot')

    return pivot.length === 0
      ? ''
      : `
        async update({ request, session, auth, response, view, params }) {
          let uploadFile = await use(\`App/Models/${this.capitalizeFirstLetter(
    props.name
  )}\`).fieldForUploadFile
          let data = request.except(['${pivot[0].name}', '_csrf', '_method'])
          let result = await this.handleUpload(uploadFile, data, 'update', request)
            if (result.status === 'error') {
              return response.send(result.data)
            } else {
              data = result.data
            }

          const ${this.capitalizeFirstLetter(
    props.name
  )} = await use('App/Models/${this.capitalizeFirstLetter(
  props.name
)}').find(params.id);
          ${this.capitalizeFirstLetter(props.name)}.merge(data);

          // ${this.capitalizeFirstLetter(
    props.name
  )}.merge({...request.except(['${
  pivot[0].name
}', '_csrf', '_method'])});
          // ${
  isFile.length > 0
    ? `if(file.name !== ''){ ${this.capitalizeFirstLetter(
      props.name
    )}.merge({${
      isFile[0].name
    }: typeof file.name === 'string' ? file.name : file.name[0]});}`
    : ''
}

          await ${this.capitalizeFirstLetter(props.name)}.save();
          await ${this.capitalizeFirstLetter(props.name)}.${
  pivot[0].relations.model
}().sync(request.only('${pivot[0].name}').${pivot[0].name});


        response.send({
              __status: \`success_update_${props.name
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase()}\`,
              __message: \`update ${props.name
    .split(/(?=[A-Z])/)
    .join(' ')
    .toLowerCase()} success\`,
              __error: null,
              __validation: null,
              ...${this.capitalizeFirstLetter(props.name)}
            })
        // session.flash({ notification: 'successfully update ${props.name}' });
        // response.route('admin.${props.name
    .split(/(?=[A-Z])/)
    .join('-')
    .toLowerCase()}.index');
        }
    `
  }

  async setControllerStore (props) {
    const isFile = props.field.filter(i => i.formType === 'file')
    const pivot = props.field.filter(i => i.type === 'pivot')

    return pivot.length <= 0
      ? ''
      : `
        async store({ request, session, auth, response, view, params }) {
          let uploadFile = await use(\`App/Models/${this.capitalizeFirstLetter(
    props.name
  )}\`).fieldForUploadFile
          let data = request.except(['${pivot[0].name}', '_csrf', '_method'])
          let result = await this.handleUpload(uploadFile, data, 'create', request)
            if (result.status === 'error') {
              return response.send(result.data)
            } else {
              data = result.data
            }



          const Mdl = await use('App/Models/${this.capitalizeFirstLetter(
    props.name
  )}')
          const ${this.capitalizeFirstLetter(props.name)} = new Mdl();

          // ${this.capitalizeFirstLetter(props.name)}.merge({
          //   ...request.except(['${pivot[0].name}', '_csrf', '_method'])
          // });
          ${this.capitalizeFirstLetter(props.name)}.merge(data);
          await ${this.capitalizeFirstLetter(props.name)}.save();
          await ${this.capitalizeFirstLetter(props.name)}.${
  pivot[0].relations.model
}().sync(request.only('${pivot[0].name}').${pivot[0].name});

          response.send({
              __status: \`success_create_${props.name
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase()}\`,
              __message: \`create ${props.name
    .split(/(?=[A-Z])/)
    .join(' ')
    .toLowerCase()} success\`,
              __error: null,
              __validation: null,
              ...${this.capitalizeFirstLetter(props.name)}
            })
          // session.flash({ notification: 'successfully create new ${
  props.name
}' });
          // response.route('admin.${props.name
    .split(/(?=[A-Z])/)
    .join('-')
    .toLowerCase()}.index');
        }
    `
  }

  async setControllerAction (props) {
    let fc = await this.setControllerQuery(props, 'create')
    let fe = await this.setControllerQuery(props, 'edit')

    // async create({ view}) {
    //   ${fc.query}
    //   // return view.render('admin.${props.name.split(/(?=[A-Z])/).join('_').toLowerCase()}.create', { ${fc.variables} });
    // }
    const addMethod = `


          ${
  fe.variables.length > 0
    ? `async edit({ view, params, request, response }) {
             ${fe.query}

            // const data = (await use('App/Models/${this.capitalizeFirstLetter(
    props.name
  )}').find(params.id)).toJSON();
            // return view.render('admin.${props.name
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase()}.edit', { data, ${fe.variables} });
          }`
    : ``
}

          ${await this.setControllerStore(props)}

          ${await this.setControllerUpdate(props)}
      `
    return addMethod
  }

  async createView (c, op) {
    const result = await this.viewTemplate(c, op)

    // const filename = `admin/${result.name}/${op}.vue`
    const filename = `${__dirname}/../../../src/pages/admin/${result.name}.vue`
    await this.writeFile(filename, result.content)
    console.log(`${filename} succesfully created :) `)
    return true
  }

  async createController (c) {
    const result = await this.controllerTemplate(c)
    const filename = `app/Controllers/Http/Admin/${this.capitalizeFirstLetter(
      result.name
    )}Controller.js`
    await this.writeFile(Helpers.appRoot(filename), result.content)
    console.log(`${filename} succesfully created :) `)
  }

  async createApiController (c) {
    const result = await this.controllerApiTemplate(c)
    const filename = `app/Controllers/Http/Api/V1/${this.capitalizeFirstLetter(
      result.name
    )}Controller.js`
    await this.writeFile(Helpers.appRoot(filename), result.content)
    console.log(`${filename} succesfully created :) `)
  }

  async updateFactory (c, type) {
    const result = this.factoryTemplate(c)
    const filename = `database/factory.js`
    const contents = await this.readFile(Helpers.appRoot(filename), 'utf-8')
    const name = this.capitalizeFirstLetter(result.name)

    if (
      contents.indexOf(`use('Factory').blueprint('App/Models/${name}'`) !== -1
    ) {
      console.log('UPDATE  FACTORY ', name, type)
      if (type === 'remove') {
        // use\('Factory'\)\.blueprint\('App\/Models\/(SpeakerToTag)', async \(?faker\)? => {[\s\S]*?}\;?[\s\S]*?\}?\}\;?[\s\S]*?}\)\;$

        var rgx = new RegExp(
          `use\\('Factory'\\)\\.blueprint\\('App\\/Models\\/(${name})', async \\(?faker\\)? => {[\\s\\S]*?^[\\}\\)]+\\;?$`,
          'gim'
        )
        let x = contents.replace(rgx, '')
        console.log('rgx ', rgx)
        // console.log('read factory ', x);
        await this.writeFile(Helpers.appRoot(filename), x)
        console.log(`successfully remove factory ${name}`)
      }
    } else {
      if (type !== 'remove') {
        // await this.writeFile(Helpers.appRoot(filename), result.content)
        await Drive.disk('local').append(
          Helpers.appRoot(filename),
          result.content
        )
        console.log(`${filename} succesfully updated :) `)
      }
    }
  }

  async createModel (c) {
    const result = this.modelTemplate(c)
    const filename = `app/Models/${this.capitalizeFirstLetter(result.name)}.js`
    await this.writeFile(Helpers.appRoot(filename), result.content)
    console.log(`${filename} succesfully created :) `)
  }

  async createSeeder (c) {
    const result = this.seederTemplate(c)
    const filename = `database/seeds/${this.capitalizeFirstLetter(
      result.name
    )}Seeder.js`

    // let isExist = await fs.existsSync(Helpers.appRoot(filename));
    // if(!isExist){
    await this.writeFile(Helpers.appRoot(filename), result.content)
    console.log(`${filename} succesfully created :) `)
    // }else{
    //   console.log(`${filename} succesfully already created :) `);
    // }
  }

  async createMigration (c) {
    const result = this.migarationTemplate(c)
    let time = new Date().getTime()

    let name = result.name
      .split(/(?=[A-Z])/)
      .join('_')
      .toLowerCase()

    if (name === 'user') {
      let cekFiles = await fs.readdirSync(
        Helpers.appRoot(`database/migrations`)
      )
      if (cekFiles.length > 0) {
        let getTime = cekFiles[0].split('_')[0]
        time = getTime - 1
      }
    }

    const filename = `database/migrations/${time}_${name}.js`

    let file = await fs.readdirSync(Helpers.appRoot(`database/migrations`))
    let cekFile = file.filter(file => file.indexOf(`${name}.js`) > 0)

    if (cekFile.length > 0) {
      console.log(`${cekFile[0]} already created :) `)
    } else {
      await this.writeFile(Helpers.appRoot(filename), result.content)
      console.log(`${filename} succesfully created :) `)
    }
  }

  async setFieldViewEdit (props, type) {
    let name = props.name
      .split(/(?=[A-Z])/)
      .join(' ')
      .toLowerCase()
    var ordered = ''

    if (type === 'edit') {
      ordered += `{type:'hidden',name:'id', label:'id', value: old('id', data.id), error: getErrorFor('id')},\n`
    }

    props.field.forEach(i => {
      if (typeof i.skip !== 'undefined') return

      var value =
        type === 'edit'
          ? i.type !== 'pivot'
            ? `old('${i.name}', data.${i.name})`
            : `old('${i.name}', ${i.relations.model}Selected)`
          : `old('${i.name}')`

      if (i.relations) {
        if (i.type === 'pivot') {
          ordered += `{type:'select', multiple:'true', name:'${
            i.name
          }', label:'${i.label}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', value: ${value}, error: getErrorFor('${i.name}'), key:${
            i.relations.formKey
          }, data : ${i.relations.model}},\n\t\t\t\t\t`
        } else {
          ordered += `{type:'select',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }'), key:${i.relations.formKey}, data : ${
            i.relations.model
          }},\n\t\t\t\t\t`
        }
      } else {
        if (i.formType === 'text') {
          ordered += `{type:'text',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'date') {
          ordered += `{type:'date',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'datetime-local') {
          ordered += `{type:'datetime-local',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'email') {
          ordered += `{type:'email',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'number') {
          ordered += `{type:'number',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'password') {
          ordered += `{type:'password',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ', error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'textarea') {
          ordered += `{type:'textarea',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        } else if (i.formType === 'radio') {
          // @todo bsk bikin pilihan buat enum

          let enuData = ''
          if (i.type === 'enu') {
            const enu =
              typeof i.enumLabel !== 'undefined'
                ? i.enumLabel.split(',')
                : i.enumValue.split(',')
            const eni = i.enumValue.split(',')
            eni.forEach((i, index) => {
              enuData += `\n\t\t\t\t\t{label:${enu[index]},value:${i}},`
            })
          } else {
            enuData += `{label:'active',value:1},{label:'not active',value:0}`
          }

          ordered += `{type:'radio',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }'),
                        key:['value','label'],
                        data: [
                                ${enuData}
                            ]
                        },\n\t\t\t\t\t`
        } else if (i.formType === 'file') {
          ordered += `{type:'file', name:'${i.name}', label:'${i.label}', error: getErrorFor('${i.name}')},\n\t\t\t\t\t\t\t`
        } else {
          ordered += `{type:'text',name:'${i.name}', msg:'${
            i.inputMsg ? i.inputMsg : ''
          }', label:'${i.label}', value: ${value}, error: getErrorFor('${
            i.name
          }')},\n\t\t\t\t\t`
        }
      }
    })

    ordered += `{type:'button', label:'${
      type === 'edit' ? 'update' : 'save'
    } ${name}'},\n\t\t\t`

    const output = ordered.replace(/,\n$/g, '')
    return output
  }
  putSpaceInCamelCase (val) {
    if (val) {
      let rex = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])/g
      return val.replace(rex, '$1$4 $2$3$5').replace('Form', '')
    } else {
      return ''
    }
  }

  createForm (props) {
    let html = ''

    let inputStyle = `outlined square`
    let generateVuexSync = props.field.map(i => `'${i.name}'`)
    generateVuexSync.push(`'${props.use_default_id}'`)
    generateVuexSync.push(`'${props.use_default_date}'`)
    generateVuexSync = generateVuexSync.filter(i => i).join(`, `)
    let isContainPassword = props.field.some(i => i.formType === 'password')
    let generateParams = props.field
      .map(i => !i.skip && `${i.name}:this.${i.name}`)
      .filter(i => i !== false)
      .join(',')
    let generateFormData = props.field
      .map(i => {
        if (!i.skip && i.formType !== 'file' && i.type !== 'pivot') {
          if (i.name === 'password') {
            return `if(this.${i.name}) { params.append('${i.name}', this.${i.name}) }`
          } else {
            return `params.append('${i.name}', this.${i.name})`
          }
        }
      })
      .filter(i => i !== false)
      .join(`\n`)
    let generateFileUpload = []
    let watchFileUpload = []
    let formTypeCheckbox = []
    let generateFormDataPivot = []
    let showOnlyUser = `
         if (this.auth.user_role < 4) {
            this.userRoleOptions = this.userRoleOptions.filter(i => Number(i.value) === Number(this.auth.user_role))
          }
        `

    let showMethodOnlyUser = `
          mergeAuthAfterUserUpdate (val) {
            if (val.__status === 'success_update_user') {
              if (Number(val.id) === Number(this.auth.id)) {
                let newuser = Object.keys(val).filter(i => i.indexOf('__') === -1 && !['users', 'created_at', 'updated_at', 'location_detail', 'user_status', 'code', 'parent'].includes(i))
                newuser.forEach((i) => {
                  this.$store.commit(\`auth/SET_\${i.toUpperCase()}\`, val[i])
                })
              }
            }
          },
        `
    props.field.forEach(i => {
      if (i.type === 'pivot') {
        generateFormDataPivot.push(`
            this.${i.name}.forEach(i => {
                params.append('${i.name}[]', i)
            })
        `)
      }

      if (i.formType === 'file') {
        generateFileUpload.push(`
          if (this.${i.name}) {
            this.${i.name}.forEach(i => {
              if (i.__status) {
                params.append('${i.name}[]', i)
              } else {
                params.append('${i.name}_old[]', i)
              }
            })

             if (this.${i.name}.length === 0 && this.removedFile.length > 0) {
              this.removedFile.forEach(i => {
                params.append('${i.name}[]', i)
              })
            }
          }
        `)

        watchFileUpload.push(`
          ${i.name}: {
            get () {
              let val = this.$store.state.${props.name}
              if (!val.${i.name}) {
                return []
              } else {
                if (typeof val.${i.name} === 'string') {
                  return val.${i.name}.split(',')
                } else {
                  return val.${i.name}
                }
              }
            },
            set () {}
          },
        `)
        // ini untu handle q-uploader yang lama pake watch, trus diganti pake computed seter getter
        //         watchFileUpload.push(`
        //         if (typeof val.${i.name} === 'string') {
        //                   if (val.${i.name}.indexOf(',') !== -1) {
        //                     this.$store.commit('${
        //   props.name
        // }/SET_${i.name.toUpperCase()}', val.${i.name}.split(','))
        //                   } else {
        //                     if (val.${i.name} !== 'null' && val.${i.name}) {
        //                       this.$store.commit('${
        //   props.name
        // }/SET_${i.name.toUpperCase()}', [val.${i.name}])
        //                     }
        //                   }
        //                 }`)
      }

      if (i.formType === 'checkbox') {
        formTypeCheckbox.push(`
            ${i.name}: {
              get () {
                let val = this.$store.state.${props.name}.${i.name}
                if (!val) {
                  return []
                } else {
                  if (typeof val === 'string') {
                    return val.split(',').map(i => Number(i))
                  } else {
                    return val
                  }
                }
              },
              set (val) {
                this.$store.commit('${
  props.name
}/SET_${i.name.toUpperCase()}', val)
              }
            },
        `)
      }
    })

    // \${watchFileUpload.length > 0 ? watchFileUpload.join(\`\n\`) : ''} diremove aja
    let showMergeUpdate =
      props.name === 'user' ? `this.mergeAuthAfterUserUpdate(val)` : ''
    let addWatchFile = `
        ${props.name}: {
            handler (val) {
              if (val.__status !== 'loading') {
                  ${showMergeUpdate}
              }
            },
            deep: true
          },
        `

    let formContainFile = props.field.some(i => i.formType === 'file')
    let getRelationalState = props.field
      .filter(i => i.relations)
      .map(i => `'${this.lowercaseFirstLetter(i.relations.model)}'`)
    let dispatchRelational = props.field
      .filter(i => i.relations)
      .map(i => {
        if (!i.relations.loadDepend) {
          return `this.$store.dispatch('${this.lowercaseFirstLetter(
            i.relations.model
          )}/index', { show: 'all' })`
        }
      })
      .join(`\n`)
    let watchHtml = ``
    let dispatchFirstLoadDepend = []
    props.field
      .filter(i => i.relations)
      .forEach(i => {
        if (i.relations.loadDepend && i.relations.loadDepend.length > 0) {
          if (dispatchFirstLoadDepend.length === 0) {
            dispatchFirstLoadDepend.push(
              `this.$store.dispatch('${this.lowercaseFirstLetter(
                i.relations.model
              )}/index', { show: 'all' })`
            )
          }

          let clearField = i.relations.loadDepend
            .map(i => `this.${i} = ''`)
            .shift()
          // shift() get first element of array
          // .join(`\n`)
          let fetch = i.relations.loadDepend[0].split('_')[0]
          let depend = i.relations

          if (i.relations.loadDependRelation) {
            clearField = i.relations.loadDependRelation.clearField.map(
              i => `this.${i} = ''`
            )
            depend = i.relations.loadDependRelation
            fetch = i.relations.loadDependRelation.store
          }

          watchHtml += `
              ${i.name}: {
                handler (newValue, oldValue) {
                  if(oldValue){ ${clearField} }
                  let search = \`${depend.inTable}.${depend.references}:\${newValue}:integer\`
                  this.$store.dispatch('${fetch}/index', { search: search, show: 'all' })
                },
                deep: true
              },`
        }
      })
    let groupRadio = []
    let resetInputStyle = inputStyle
    props.field.forEach(i => {
      if (i.skip) return

      let labelname = i.label
      let validation = i.validation
        ? `:rules="$validation('${i.label}', '${i.validation}')"`
        : `:rules="[]"`

      if (i.hint) {
        inputStyle = inputStyle + ` hint="${i.hint}" `
      } else {
        inputStyle = resetInputStyle
      }

      if (i.relations) {
        let storeName = this.lowercaseFirstLetter(i.relations.model)
        let k = JSON.parse(i.relations.formKey)
        let multivalue = i.type === 'pivot' ? 'multiple use-chips' : ''
        let validationSelect = i.validation
          ? `:rules="$validation('${k[1].replace(/\_/gi, ' ')}', '${
            i.validation
          }')"`
          : ``
        html += `<q-select ${inputStyle} ${validationSelect} ${multivalue} label="${k[1].replace(
          /\_/gi,
          ' '
        )}" v-model="${i.name}" :options="${storeName}.${
          i.relations.inTable
        }" option-value="${k[0]}" option-label="${
          k[1]
        }" emit-value map-options class="select" :loading="${storeName}.__status === 'loading'" />\n`
      } else {
        if (i.formType === 'text') {
          html += `<q-input ${inputStyle}  ${validation} v-model="${i.name}" label="${labelname}" />\n`
        } else if (i.formType === 'date') {
          html += `<q-input ${inputStyle}  ${validation} v-model="${i.name}" mask="date" label="${labelname}">
                    <template v-slot:append>
                      <q-icon
                        name="event"
                        class="cursor-pointer"
                      >
                        <q-popup-proxy
                          ref="qDateProxy"
                          transition-show="scale"
                          transition-hide="scale"
                        >
                          <q-date
                            v-model="${i.name}"
                            @input="() => $refs.qDateProxy.hide()"
                          />
                        </q-popup-proxy>
                      </q-icon>
                    </template>
                  </q-input>\n`
        } else if (i.formType === 'datetime-local') {
          html += `<q-input ${inputStyle}  ${validation} v-model="${i.name}" mask="date" :rules="['date']" label="${labelname}">
                    <template v-slot:append>
                      <q-icon
                        name="event"
                        class="cursor-pointer"
                      >
                        <q-popup-proxy
                          ref="qDateProxy"
                          transition-show="scale"
                          transition-hide="scale"
                        >
                          <q-date
                            v-model="birthdate"
                            @input="() => $refs.qDateProxy.hide()"
                          />
                        </q-popup-proxy>
                      </q-icon>
                    </template>
                  </q-input>\n`
        } else if (i.formType === 'email') {
          html += `<q-input ${inputStyle}  ${validation} type="email" v-model="${i.name}" label="${labelname}" />\n`
        } else if (i.formType === 'number') {
          html += `<q-input ${inputStyle}  ${validation} type="number" v-model="${i.name}" label="${labelname}" />\n`
        } else if (i.formType === 'password') {
          html += `
          <q-input
          ${inputStyle} ${validation}
          v-model="${i.name}" label="${labelname}"
          :type="isPwd ? 'password' : 'text'"
            >
              <template v-slot:append>
                <q-icon
                  :name="isPwd ? 'visibility_off' : 'visibility'"
                  class="cursor-pointer"
                  @click="isPwd = !isPwd"
                />
              </template>
          </q-input>\n`
        } else if (i.formType === 'textarea') {
          html += `<q-input ${inputStyle}  ${validation} type="textarea" v-model="${i.name}" label="${labelname}" />\n`
        } else if (
          i.formType === 'radio' ||
          i.formType === 'checkbox' ||
          i.formType === 'dropdown'
        ) {
          // let enuData = ''
          // if (i.type === 'enu') {
          //   const enu =
          //     typeof i.enumLabel !== 'undefined'
          //       ? i.enumLabel.split(',')
          //       : i.enumValue.split(',')
          //   const eni = i.enumValue.split(',')
          //   eni.forEach((j, index) => {
          //     enuData += `<q-radio v-model="${i.name}" label="${enu[index].replace(/\'/gi, '')}" val="${j}"  />\n`
          //   })
          // } else {
          // enuData += `<q-radio v-model="${i.name}" label="active" :val="1" />\n`
          // enuData += `<q-radio v-model="${i.name}" label="not active" :val="0" />\n`
          // }

          let camelCase = i.name.replace(/_([a-z])/g, g => g[1].toUpperCase())
          groupRadio.push({
            key: `${camelCase}Options`,
            value: JSON.stringify(i.enumData)
          })

          if (i.formType === 'dropdown') {
            html += `<q-select ${inputStyle}  ${validation} v-model=${i.name} :options="${camelCase}Options" label="${i.label}" emit-value map-options />\n`
          } else {
            html += `<div class="border bg-grey-1 q-mb-lg">
                      <div class="q-px-sm q-mt-sm">
                        ${i.label}
                      </div>\n
                      <q-option-group v-model="${
  i.name
}" :options="${camelCase}Options"
                      ${i.formType === 'checkbox' ? '' : 'inline'}
                      type="${i.formType}"
                      />
                      </div>\n`
          }
        } else if (i.formType === 'file') {
          // html += `
          //         <q-file ${inputStyle}  ${validation} v-model="${i.name}" label="${labelname}">
          //           <template v-slot:prepend>
          //             <q-icon name="attach_file" />
          //           </template>
          //         </q-file>\n

          html += `
                  <q-uploader
                    class="full-width q-mb-lg no-border-radius"
                    :factory="(files) => onChangeFile(files, '${i.name}', '${
  props.name
}')"
                    auto-upload
                    label="Upload ${i.name}"
                    accept=".jpg, image/*"
                    flat
                    bordered
                    color="grey-6"
                  >
                    <template v-slot:header="scope">
                      <div class="q-pa-sm text-right">
                        <q-spinner v-if="scope.isUploading" class="q-uploader__spinner" />
                        <q-btn v-if="scope.canAddFiles" type="a" icon="add_box" dense flat label="upload ${i.name.replace(
    /\_/gi,
    ' '
  )} ">
                          <q-uploader-add-trigger />
                          <q-tooltip>Pick Files</q-tooltip>
                        </q-btn>
                      </div>
                    </template>
                    <template v-slot:list>
                      <q-list separator v-if="typeof ${i.name} ==='object'">
                        <q-item v-for="(file,key) in ${i.name}" :key="key">
                            <q-item-section>
                              <q-item-label class="full-width ellipsis">
                                {{ typeof file === 'string' ? file : file.name }}
                              </q-item-label>
                            </q-item-section>
                            <q-item-section thumbnail >
                              <img style="object-fit:contain;" :src="\`\${(typeof file === 'string') ? file.indexOf('http') !== -1 ? file : \`\${API_URL}/uploads/\${file}\` : file.__img.src}\`"/>
                            </q-item-section>
                            <q-item-section  side>
                              <q-btn
                                class=""
                                size="12px"
                                flat
                                dense
                                round
                                icon="delete"
                                @click="removeFile(file, '${i.name}', '${
  props.name
}')"
                              />
                            </q-item-section>
                        </q-item>
                      </q-list>
                    </template>
                  </q-uploader>\n`
        } else {
          html += `<q-input v-model="${i.name}" ${inputStyle} ${validation} label="${labelname}" />\n`
        }
      }
    })
    let template = `<template>
<q-page padding>\n
  <div class="row flex-center">\n
  <q-form class="col-lg-4 col-xs-12 bg-white rounded-borders q-pa-md q-gutter-y-xs" @submit="onSubmit" @reset="onReset">\n
\n${html}
<div class="q-mt-lg">
          <ul v-if="__validation" class="bg-red-7 text-white q-pa-md list-none" style="list-style-type:none;">
            <li class="q-mb-md">Error when submit form: </li>
            <li v-for="i in __validation" :key="i">
              - {{i.message}}
            </li>
          </ul>
          <div class="row q-gutter-md">
            <q-btn
              label="Submit"
              type="submit"
              color="primary"
              class="col no-border-radius"
              unelevated
              :rules="[ val => val && val.length > 0 || 'Please type something']"
            />
            <!-- <q-btn
              label="Reset"
              type="reset"
              color="primary"
              flat
              class="col-3"
            /> -->
          </div>
        </div>
</q-form>\n
</div>\n
</q-page>\n
</template>\n
<script>
import { mapState, mapActions } from 'vuex'
import { vuexSync } from 'boot/helpers'
export default {
   data () {
    return {
      ${isContainPassword ? `isPwd: true,\n` : ``}
      ${
  groupRadio.length > 0
    ? groupRadio
      .map(i => {
        return `${i.key}: ${i.value}`
      })
      .join(`,\n`)
    : ''
}
    }
  },
  computed: {
    ...mapState(['${props.name}', ${
  getRelationalState.length > 0 ? getRelationalState.join(', ') + ',' : ''
} 'auth']),
    ...vuexSync('${
  props.name
}', [${generateVuexSync}, '__status', '__message', '__error','__validation']),
    ${watchFileUpload.length > 0 ? `${watchFileUpload.join(`\n`)}` : ''}
    ${formTypeCheckbox.length > 0 ? `${formTypeCheckbox.join(`\n`)}` : ''}
  },
  beforeCreate(){
    this.$store.commit('${props.name}/RESET')
  },
  created () {
    Object.assign(this.notify, {
      enable: true,
      success: true,
      redirect: (status) => {
        if (status === 'success_create_${props.name
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase()}' || status === 'success_update_${props.name
  .split(/(?=[A-Z])/)
  .join('_')
  .toLowerCase()}') {
          this.$router.go(-1)
        }
      }
    })

    ${dispatchRelational}
    ${dispatchFirstLoadDepend.join(`\n`)}

    if (this.$route.params.id) {
      this.$store.dispatch('${props.name}/edit', { id: this.$route.params.id })
      // this.$store.commit('${
  props.name
}/SET_RESPONSE', { id: this.$route.params.id })
    }

    ${showOnlyUser}
  },
  watch:{
    ${addWatchFile}
    ${watchHtml}
  },
  methods: {
    ${showMethodOnlyUser}
    ...mapActions({}),
    onSubmit () {
      let action = this.$route.params.id ? 'update' : 'store'
      ${
  formContainFile
    ? `let params = new FormData()\n if (this.$route.params.id) params.append('id', this.$route.params.id) \n${generateFormData} \n ${generateFormDataPivot.join(
      `\n`
    )} ${generateFileUpload.join(`\n`)}`
    : `let params = {id: this.$route.params.id, ${generateParams}}`
}
      this.$store.dispatch('${props.name}/'+action, params)
    },
    onReset () {
      this.$store.commit('${props.name}/RESET')
    }
  }
}
</script>

    `
    // console.log(template);
    return template
    // return html
  }

  customName (name, type = 'plural', prefix = '_') {
    if (type === 'plural') {
      return pluralize.plural(
        name
          .split(/(?=[A-Z])/)
          .join(prefix)
          .toLowerCase()
      )
    } else if (type === 'underscore') {
      return name
        .split(/(?=[A-Z])/)
        .join(prefix)
        .toLowerCase()
    } else {
      return name
    }
  }

  async createStore (props) {
    let pluralName = this.customName(props.name)
    let lowerCaseName = this.customName(props.name, 'underscore')
    let lowerCaseUrlName = this.customName(props.name, 'underscore', '-')

    let generateVuexSync = props.field.map(
      i => `${i.name}: ${typeof i.default !== 'undefined' ? i.default : `null`}`
    )
    generateVuexSync.push(`${props.use_default_id ? `id:null` : ''}`)
    generateVuexSync.push(
      `${props.use_default_date ? `created_at:null,\nupdated_at:null` : ''}`
    )
    generateVuexSync = generateVuexSync.filter(i => i).join(`, `)

    let html = `
    import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    ${generateVuexSync},
    ${pluralName}: [],
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('${lowerCaseName}', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        \`/api/admin/${lowerCaseUrlName}\`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        \`/api/admin/${lowerCaseUrlName}/\${params.id}/edit\`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        \`/api/admin/${lowerCaseUrlName}/\${id}\`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, \`/api/admin/${lowerCaseUrlName}\`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        \`/api/admin/${lowerCaseUrlName}/\${params.id}\`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
    `

    const filename = `${__dirname}/../../../src/store/${props.name}.js`
    console.log('GEMBEL CREATE STORE', filename)
    await this.writeFile(filename, html)
    console.log(`${filename} STORE succesfully created :) `)
  }

  async updateAppRoute (props, type) {
    const filename = `${__dirname}/../../../src/router/adminRoutes.js`
    const adminRoutes = await this.readFile(filename, 'utf-8')
    const name = this.capitalizeFirstLetter(props.name)
    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    // grab match regext pake tanda $&
    // reinsert regex : https://lingohub.com/blog/2015/08/dev-bit-how-to-reuse-matched-value-of-regex-in-javascripts-replace-function/
    if (
      adminRoutes.indexOf(`path: '${routeName}',`) === -1 &&
      type !== 'remove'
    ) {
      const b = `
       ,{
          path: '${routeName}',
          name: '${name}',
          label: '${name}',
          meta: {
            title: '${name}'
          },
          component: () => import('pages/admin/${name}.vue')
        },{
          path: '${routeName}/form/:id?',
          name: '${name}Form',
          label: '${name}',
          meta: {
            title: '${name}'
          },
          component: () => import('pages/admin/${name}Form.vue')
        }
        $&`
      let a = adminRoutes.replace(/[^]\][^]+\;?$/g, b)
      await this.writeFile(filename, a)
      console.log(`successfully update route ${routeName}`)
    } else {
      if (type === 'remove') {
        let pola = `\\{\\s*path:\\s?'${routeName}',\\n.*\\n.*\\n.*\\n.*\\n.*\\n.*\\n.*\\},\\s?\\s?(\\n?|\\n.*)\\{\\n.*,\\n.*,\\n.*,\\n.*\\{\\n.*\\n.*\\}.*\\n.*\\n.*\\}\\,?`
        console.log('pola ', pola)
        var rgx = new RegExp(pola, 'gmi')
        let x = adminRoutes.replace(rgx, '')
        // console.log('RGEX', x)
        await this.writeFile(filename, x)
        console.log(`successfully remove route ${routeName}`)
      } else {
        console.log(`${routeName} route already exist`)
      }
    }
  }
  async updateLayoutMenu (props, type) {
    const filename = `${__dirname}/../../../src/layouts/AdminLayout.vue`
    const adminRoutes = await this.readFile(filename, 'utf-8')
    const name = this.capitalizeFirstLetter(props.name)
    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    // grab match regext pake tanda $&
    // reinsert regex : https://lingohub.com/blog/2015/08/dev-bit-how-to-reuse-matched-value-of-regex-in-javascripts-replace-function/
    if (adminRoutes.indexOf(`name: '${name}',`) < 0 && type !== 'remove') {
      const b = `,{
          name: '${name}',
          icon: '${props.icon}',
          label: '${routeName.replace(/\-/gi, ' ')}',
          separator: false,
          level:1,
        }
        $&`
      let a = adminRoutes.replace(/[^]\][^]+\;?\n$/g, b)
      await this.writeFile(filename, a)
      console.log(`successfully update menu ${routeName}`)
    } else {
      if (type === 'remove') {
        // let pola = `\\{\\s*path:\\s?'${routeName}',\\n.*\\n.*\\n.*\\n.*\\n.*\\n.*\\n.*\\},\\s?\\s?(\\n?|\\n.*)\\{\\n.*,\\n.*,\\n.*,\\n.*\\{\\n.*\\n.*\\}.*\\n.*\\n.*\\}\\,?`
        let pola = `\\{\\n.*name:\\s?'${name}'[\\s\\S]*?\\}\\,?\n?`
        var rgx = new RegExp(pola, 'gmi')
        let x = adminRoutes.replace(rgx, '')
        console.log('RGEX', pola)
        await this.writeFile(filename, x)
        console.log(`successfully remove menu ${routeName}`)
      } else {
        console.log(`${routeName} menu already exist`)
      }
    }
  }

  async updateIndexStore (props, type) {
    const filename = `${__dirname}/../../../src/store/index.js`
    const adminRoutes = await this.readFile(filename, 'utf-8')
    const name = this.capitalizeFirstLetter(props.name)
    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    // grab match regext pake tanda $&
    // reinsert regex : https://lingohub.com/blog/2015/08/dev-bit-how-to-reuse-matched-value-of-regex-in-javascripts-replace-function/
    if (
      adminRoutes.indexOf(`import ${props.name} from`) < 0 &&
      type !== 'remove'
    ) {
      const b = `import ${props.name} from './${props.name}'\nVue.use(Vuex)`
      const c = `\$&\n\t${props.name},`
      let a = adminRoutes
        .replace(/Vue.use\(Vuex\)/gm, b)
        .replace(/modules:\s?{/gm, c)
      await this.writeFile(filename, a)
      console.log(`successfully update route ${routeName}`)
    } else {
      if (type === 'remove') {
        let pola = `import ${props.name} from './${props.name}'\n`
        let pola1 = `${props.name},\n`
        var rgx = new RegExp(pola, 'gmi')
        var rgx1 = new RegExp(pola1, 'gmi')
        let x = adminRoutes.replace(rgx, '').replace(rgx1, '')
        await this.writeFile(filename, x)
        console.log(`successfully remove route ${routeName}`)
      } else {
        console.log(`${routeName} route already exist`)
      }
    }
  }

  async viewTemplate (props, op) {
    if (op === 'index') {
      return this.viewIndexTemplate(props)
    } else {
      let name = this.capitalizeFirstLetter(props.name)
      let form = this.createForm(props)
      const v = {
        name: name + 'Form',
        content: form
      }
      return v
    }
  }

  viewIndexTemplate (props) {
    let name = this.capitalizeFirstLetter(props.name)
    let nameSpace = this.putSpaceInCamelCase(props.name)
    let urlName = this.customName(props.name, 'underscore', '_')
    let urlNameStrip = this.customName(props.name, 'underscore', '-')
    let pluralizeName = this.customName(props.name, 'plural')
    let singularName = pluralize.singular(pluralizeName)
    let generateVuex = props.field.map(i => `'${i.name}'`).join(`, \n`)
    return {
      name: name,
      content: `
   <template>
        <q-page padding>
          <div v-if="__status === 'loading' && firstLoad" class="q-pa-md text-center">
            <q-spinner-facebook color="light-blue" size="2em"/>
          </div>
          <div v-else>
            <div :class="$q.screen.gt.sm ? 'text-right q-mb-md' : 'text-center'">
             <!-- <q-btn
                v-if="selected.length > 0"
                icon="delete"
                :label="$q.screen.gt.md ? \`\${getSelectedString()}\` : ''"
                color="transparent text-black"
                @click="destroy(selected.map(i => i.id).join(','))"
              /> -->
              <q-btn
                v-if="$store.state.auth.user_role >= 2"
                icon="post_add"
                label="create"
                color="red"
                to="/admin/${urlNameStrip}/form"
                dense
                size="md"
                class="q-mr-sm"
              />
              <q-btn
                dense
                icon="search"
                label="search"
                color="blue"
                @click="showFilter = !showFilter"
                size="md"
                class="q-mr-sm"
              >
                <q-badge
                  v-if="filters && filters.filter(i => i.search).length > 0"
                  label=""
                  color="red"
                  class="q-pa-xs text-xs q-ml-xs"
                />
              </q-btn>
              <q-btn label="export" color="green" @click="exportWhere" icon="file_copy" dense class="q-mr-sm" />
              <q-btn-dropdown color="orange" label="hide"   dense size="md">
                <q-option-group
                  :options="
                    ${pluralizeName}.columns.map(i => {
                      return { label: i.label, value: i.name };
                    })
                  "
                  label="Notifications"
                  type="checkbox"
                  v-model="invisibleColumns"
                />
              </q-btn-dropdown>
               <!-- <q-btn
                :icon="tableGrid ? 'view_module' : 'view_list'"
                color="transparent text-black"
                @click="
                  tableGrid === null ? (tableGrid = $q.screen.lt.sm ? false : true) : (tableGrid = !tableGrid)
                "
              /> -->
          </div>

          <q-dialog
            v-model="showFilter"
            persistent
          >
            <q-card
              class="bg-white text-black"
              style="width: 700px; max-width: 100vw;"
            >
              <q-card-section class="row items-center q-pb-none">
                <div class="text-h6">Search </div>
                <q-space />
                <q-btn icon="close" flat round dense v-close-popup />
              </q-card-section>

              <q-card-section class="q-pt-none">
                <filter-table
                  :columns="${pluralizeName}.columns"
                  :values="filters"
                  @filter-result="getResult"
                />
              </q-card-section>
            </q-card>
          </q-dialog>

            <div v-if="$route.query.criteria" class="bg-yellow-2 q-pa-sm q-mx-sm q-mt-md text-lowercase text-red" v-html="getCriteria()"></div>

            <q-table
              class="no-shadow"
              square
              bordered
              no-data-label="I didn't find anything for you"
              :grid="tableGrid !== null ? tableGrid : $q.screen.lt.md"
              :title="$route.label"
              :data="${pluralizeName}.data"
              :columns="getColumns"
              :visible-columns="${pluralizeName}.columns.map(i => i.name).filter(i => this.config.invisibleColumns.${pluralizeName}.invisibleColumns.includes(i))"
              :pagination="${pluralizeName}.pagination"
              @request="request"
              :loading="__status === 'loading'"
              :filter="filter"
              selection="multiple"
              :selected.sync="selected"
              :selected-rows-label="getSelectedString"
            >
              <template v-slot:loading>
                <q-inner-loading showing color="primary" />
              </template>
              <template v-slot:no-data="{ icon, message }">
                <div class="full-width row flex-center text-red q-gutter-sm">
                  <q-icon size="2em" name="sentiment_dissatisfied" />
                  <span>
                    {{ message }}
                  </span>
                </div>
              </template>
              <template v-slot:header="props">
                <q-tr :props="props" class="bg-grey-4" v-if="$q.screen.gt.xs">
                  <q-th v-if="$store.state.auth.user_role > 2">
                    <q-checkbox
                      v-model="props.selected"
                      keep-color
                      :dense="$q.screen.lt.md"
                    />
                  </q-th>
                  <q-th v-for="col in props.cols" :key="col.name" :props="props">
                    {{ col.label }}
                  </q-th>
                </q-tr>
              </template>

              <template v-slot:body="props">
                <q-tr :props="props">
                  <q-td  align="center" v-if="$store.state.auth.user_role > 2">
                    <q-checkbox dense v-model="props.selected" />
                  </q-td>
                  <q-td
                    v-for="col in props.cols"
                    :key="col.name"
                    :align="col.align"
                    :classes="col.classes"
                  >
                    <div v-if="col.name === 'action'" class="q-gutter-sm">
                      <q-btn icon="edit" label="" color="green" flat dense :to="{ path: \`\${$route.path}/form/\${props.row.id}\` }">
                        <q-tooltip>edit</q-tooltip>
                      </q-btn>
                      <q-btn icon="delete" label="" color="red" flat dense @click="destroy(props.row.id)" v-if="$store.state.auth.user_role > 2">
                        <q-tooltip>delete</q-tooltip>
                      </q-btn>
                    </div>
                    <div v-html="col.value" v-else></div>
                  </q-td>
                </q-tr>
              </template>

              <template v-slot:item="props">
                <div
                  class="q-pa-xs col-xs-12 col-sm-6 col-md-4 col-lg-3 grid-style-transition"
                  :style="props.selected ? 'transform: scale(0.95);' : ''"
                >
                  <q-card :class="[props.selected ? 'bg-grey-2' : '', 'no-border-radius']" flat bordered>
                    <q-list dense>
                      <q-item
                        v-for="col in props.cols.filter(col => col.name !== 'action')"
                        :key="col.name" style="border-bottom: 1px dashed #eee;">
                        <q-item-section class="col-5">
                          <q-item-label caption class="text-bold">{{ col.label }}</q-item-label>
                        </q-item-section>
                        <q-item-section side class="col">
                          <q-item-label
                            caption
                            class="text-no-wrap"
                            lines="4"
                            v-html="col.value"
                          ></q-item-label>
                        </q-item-section>
                      </q-item>

                      <q-separator />
                      <q-item class="bg-grey-1">
                        <q-item-section>
                          <q-checkbox
                            v-if="$store.state.auth.user_role > 2"
                            dense
                            v-model="props.selected"
                            :label="props.row.name"
                          />
                        </q-item-section>

                        <q-item-section side>
                          <q-item-label>
                            <q-card-actions align="right">
                              <q-btn
                                outline
                                dense
                                color="teal"
                                label="edit"
                                icon="edit"
                                :to="{ path: \`\${$route.path}/form/\${props.row.id}\` }"
                                size="md"
                              >
                                <q-tooltip>edit</q-tooltip>
                              </q-btn>
                              <q-btn
                                outline
                                dense
                                color="red"
                                label="delete"
                                icon="delete"
                                @click="destroy(props.row.id)"
                                size="md"
                                v-if="$store.state.auth.user_role > 2"
                              >
                                <q-tooltip>delete</q-tooltip>
                              </q-btn>
                            </q-card-actions>
                          </q-item-label>
                        </q-item-section>
                      </q-item>
                    </q-list>
                  </q-card>
                </div>
              </template>
            </q-table>
          </div>
        </q-page>
      </template>

      <script>
      import { mapState } from 'vuex'
      import { vuexSync } from 'boot/helpers'
      import { extend } from 'quasar'

      export default {
        data () {
          return {
            firstLoad: true,
            tableGrid: null,
            showFilter: false,
            filters: '',
            searchString: {},
            pos: {},
            filter: {},
            selected: []
          }
        },
        computed: {
          ...mapState(['${props.name}','config']),
          ...vuexSync('${props.name}', [
            ${generateVuex},
            '${pluralizeName}',
            '__status',
            '__message'
          ]),
          invisibleColumns: {
            get () {
              return this.config.invisibleColumns.${pluralizeName}.invisibleColumns
            },
            set (val) {
              let intersection = this.config.invisibleColumns.${pluralizeName}.invisibleColumns.filter(x => !val.includes(x))
              let vv = (intersection.length === 0) ? val[val.length - 1] : intersection[0]
              this.$store.commit('config/updateVisibility', { table: '${pluralizeName}', data: vv })
            }
          },
          getColumns () {
            let items = extend(true, {}, this.${pluralizeName})
            return items.columns.map(i => {
              if (i.format) {
                // eslint-disable-next-line no-eval
                i.format = eval(i.format)
              }
              return i
            })
          }
        },
        created () {
          Object.assign(this.notify, {
            enable: true,
            success: true,
            showLoading: false,
            redirect: status => {
              if (status === 'success_destroy_${urlName}') {
                this.$store.dispatch('${props.name}/index')
              }
            }
          })

          this.request({
            pagination: {
              sortBy: '${pluralizeName}.id',
              descending: 'true'
            },
            filter: undefined
          })
        },
        methods: {
          getCriteria () {
          let c = this.$route.query.criteria.split('-')
          return \`
            <span class="block q-mb-sm">Search from summary \${c[3]}</span>
            \${c[0]} : <span class="text-bold">\${c[1]}</span><br />
            type : <span class="text-bold">\${c[2]}</span>
          \`
        },
         exportWhere () {
            let search = ''
            if (this.searchString) {
              search = Object.keys(this.searchString).map((i, j) => {
                let a = i.split(':')
                return \`\${a[0]}:\${this.searchString[i]}:\${a[1]}\`
              }).join(',')
            }
            this.exports('${pluralizeName}', 'all', search, '${singularName}')
          },
          updateVisibility (value) {
            this.$store.dispatch('config/update', {
              invisibleColumns: value,
              table: '${pluralizeName}'
            })
          },
         getResult (res) {
            // reset searchString before searrch
            this.searchString = {}

            res
              .filter(i => i.search)
              .forEach(i => {
                this.searchString[i.search.value] = i.value
              })
            this.filters = res

            console.log('gett filter result', this.filters)
            this.showFilter = !this.showFilter

            this.request({
              filter: this.searchString
            })
          },
          getSelectedString () {
            return this.selected.length === 0 ? '' : \`remove \${this.selected.length} record\${this.selected.length > 1 ? 's' : ''}\`
          },
          request (props) {
            let search = ''
            if (props.filter) {
              search = Object.keys(props.filter).map((i, j) => {
                let a = i.split(':')
                return \`\${a[0]}:\${props.filter[i]}:\${a[1]}\`
              }).join(',')
            }
            console.log('search', search, props.filter)
            let ab = { ...props.pagination, ...{ search: search } }
            this.$store.dispatch('${props.name}/index', ab)
          },
          destroy (id) {
            this.$q
              .dialog({
                title: 'Confirm',
                message: 'Apakah anda yakin ingin menghapus data ini ?',
                cancel: true,
                persistent: true
              })
              .onOk(() => {
                this.$store.dispatch('${props.name}/destroy', { id })
              })
              .onCancel(() => {})
          }
        },
        watch: {
          "$store.state.${props.name}": {
            handler (val) {
              if (val.__status !== 'loading') {
                if (val.${pluralizeName}) {
                  this.firstLoad = false
                  if (
                    (this.config.invisibleColumns.${pluralizeName} &&
                      this.config.invisibleColumns.${pluralizeName}.invisibleColumnRevision) !==
                    val.${pluralizeName}.invisibleColumnRevision
                  ) {

                    this.$store.dispatch('config/store', {
                      table: '${pluralizeName}',
                      data: {
                        invisibleColumns: val.${pluralizeName}.invisibleColumns,
                        invisibleColumnRevision: val.${pluralizeName}.invisibleColumnRevision
                      }
                    })
                  }
                }
              }
            },
            deep: true
          },
          pos: {
            handler (val) {
              this.filter = this.clean(val)
            },
            deep: true,
            immediate: true
          }
        }
      }
      </script>

      <style></style>
    `
    }
  }

  async createValidator (c, type) {
    const result = this.validatorTemplate(c, type)
    const filename = `app/Validators/${this.capitalizeFirstLetter(
      type
    )}${this.capitalizeFirstLetter(result.name)}.js`
    await this.writeFile(Helpers.appRoot(filename), result.content)
    console.log(`${filename} succesfully created :) `)
  }

  setValidator (fld) {
    var tpl = ''
    fld.forEach(i => {
      if (i.validation) tpl += `${i.name}:"${i.validation}",\n`
    })

    return tpl
  }

  setValidatorMessage (fld) {
    var tpl = ''
    fld.forEach(i => {
      if (i.validation) {
        tpl += `"${i.name}.${i.validation}":"${i.label} is ${i.validation}",\n`
      }
    })

    return tpl
  }
  validatorTemplate (props, type) {
    return {
      name: props.name,
      content: `
      "use strict";

          class ${this.capitalizeFirstLetter(type)}${this.capitalizeFirstLetter(
  props.name
)} {
            get validateAll() {
              return true;
            }

            get rules() {
              return {
                ${this.setValidator(props.field)}
              };
            }

            get messages() {
              return {
                ${this.setValidatorMessage(props.field)}
              };
            }

            async fails (errorMessages) {
            let className = (this.constructor.name.replace(/Store|Update/gi, '')).toLocaleLowerCase()
            let action = this.constructor.name.indexOf('Store') !== -1 ? 'store' : 'update'
            return this.ctx.response.json({
              __status: \`failed_\${action}_\${className}\`,
              __message: \`failed \${action} \${className}\`,
              __error: null,
              __validation: errorMessages })
          }
          }

          module.exports = ${this.capitalizeFirstLetter(
    type
  )}${this.capitalizeFirstLetter(props.name)};
      `
    }
  }

  async controllerTemplate (props) {
    let relations = props.field.filter(i => typeof i.relations !== 'undefined')
    return {
      name: props.name,
      content: `
        'use strict'
        const CrudController = use('CrudController');
        class ${this.capitalizeFirstLetter(
    props.name
  )}Controller extends CrudController{
            constructor(){
                super('${this.capitalizeFirstLetter(props.name)}')

                this.invisibleColumnRevision = 1
                this.invisibleColumns = []
            }
            ${
  relations.length > 0
    ? `async field(){
                return [
                  ${this.setFieldController(props)}
                ]
            }`
    : `/*async field(){
                      return [
                        ${this.setFieldController(props)}
                      ]
                  }*/
                `
}


            ${
  relations.length > 0
    ? `async data({request, auth}){
                ${this.setDataController(props)}
              }`
    : ''
}
            ${relations.length > 0 ? await this.setControllerAction(props) : ''}

        }

        module.exports = ${this.capitalizeFirstLetter(props.name)}Controller
      `
    }
  }

  async controllerApiTemplate (props) {
    let relations = props.field.filter(i => typeof i.relations !== 'undefined')
    return {
      name: props.name,
      content: `
        'use strict'
        const CrudController = use('CrudController');
        class ${this.capitalizeFirstLetter(
    props.name
  )}Controller extends CrudController{
            constructor(){
                super('${this.capitalizeFirstLetter(props.name)}','api')
            }
            ${
  relations.length > 0
    ? `async field(){
                return [
                  ${this.setFieldController(props)}
                ]
            }`
    : `/*async field(){
                      return [
                        ${this.setFieldController(props)}
                      ]
                  }*/
                `
}


            ${
  relations.length > 0
    ? `async data({request, auth}){
                ${this.setDataController(props)}
              }`
    : ''
}
            ${relations.length > 0 ? await this.setControllerAction(props) : ''}

        }

        module.exports = ${this.capitalizeFirstLetter(props.name)}Controller
      `
    }
  }

  setDataController (props) {
    let tpl = ''
    let alias = ''
    let relations = props.field.filter(i => typeof i.relations !== 'undefined')

    let select = []
    let primaryTable = pluralize.plural(
      props.name
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()
    )
    let wit = []
    let leftJoin = []
    let pivotMdl = []

    if (props.use_default_id) {
      let id = relations ? `'${primaryTable}.id'` : 'id'
      select.push(id)
    }

    props.field.map(i => {
      let name = `'${primaryTable}.${i.name}'`
      if (i.relations) {
        let k = JSON.parse(i.relations.formKey)

        if (i.type === 'pivot') {
          pivotMdl.push(k[1])
          wit.push(`
            .with("${i.relations.model}", builder => {
                return builder.select(${i.relations.formKey})
            })
            .by${i.relations.model}(request.input('search'))
            // .whereHas("${i.relations.model}", builder => {

            //   if(request.input('search')){
            //     let s = request.input('search').split(',')

            //     for(let i=0;i<s.length;i++){
            //       let p = s[i].split(':')
            //       if(p[0] === '${k[1]}'){
            //         builder.whereRaw(\`\${p[0]} LIKE '%\${p[1]}%'\`);
            //       }
            //     }
            //   }
            // })
          `)
        } else {
          wit.push(`
          .with("${this.capitalizeFirstLetter(i.relations.model)}", builder => {
              return builder.select(${i.relations.formKey})
          })`)
          leftJoin.push(
            `.leftJoin('${i.relations.inTable}','${primaryTable}.${i.name}','${i.relations.inTable}.${i.relations.references}')`
          )
        }
      }

      if (i.type !== 'pivot') {
        select.push(name)
      }
    })

    if (props.use_default_date) {
      let created_at = relations
        ? `'${primaryTable}.created_at'`
        : 'created_at'
      let updated_at = relations
        ? `'${primaryTable}.updated_at'`
        : 'updated_at'
      select.push(created_at)
      select.push(updated_at)
    }

    const where = `.where((builder) => {

          if (this.model === 'User') {
            if (Number(auth.user.user_role) < this.superUser) {
              builder.where('${primaryTable}.id', auth.user.id)
            }
          }

          if(request.input('search')){
            let s = request.input('search').split(',')
            console.log('s', s);
            for(let i=0;i<s.length;i++){
              let p = s[i].split(':')
              if(isPivot.indexOf(p[0]) < 0){
                if (['integer','number'].includes(p[2])) {
                  builder.where(p[0],p[1]);
                }else if(p[2] === 'date'){
                  builder.whereRaw(\`date(\${p[0]})='\${p[1]}'\`);
                }else{
                  builder.whereRaw(\`\${p[0]} LIKE '%\${p[1]}%'\`);
                }
              }
            }
          }
        })`

    let queryBuilder = `
        const isPivot = ${JSON.stringify(pivotMdl)}
        let order = request.input('descending') === 'true' ? 'desc' : 'asc'
        // console.log('ORDER QUERY', order)
        const ${
  props.name
} = await use('App/Models/${this.capitalizeFirstLetter(props.name)}')
        let query = ${props.name}.query()
        .from('${primaryTable}')
        .select([${select.join(', ')}])${wit.join('')}
        ${leftJoin.join('\n')}
        ${where}
         .orderByRaw(
        request.input('sortBy')
          ? \`\${request.input('sortBy')} \${order}\`
          : '${primaryTable}.id desc'
      )
        // .paginate(request.input("page") || 1, request.input("limit") || 10)
        // return ${props.name};

      if (request.input('show') === 'all') {
        const ab = await query.fetch()
        return ab
      } else {
        const ac = await query.paginate(
          request.input('page') || 1,
          request.input('rowsPerPage') || 10
        )
        return ac
      }
    `
    return queryBuilder
  }

  async executeMigrationAndSeeder (c) {
    try {
      const { stdout: m } = await exec('adonis migration:run')

      const { stdout: s } = await exec(
        `adonis seed --files ${this.capitalizeFirstLetter(c.name)}Seeder.js`
      )

      console.log('migration:', m)
      console.log('seeder:', s)
      await Database.raw(
        `update users set email="member@gmail.com", user_role=0, username="Member User" where id=1`
      )
      await Database.raw(
        `update users set email="admin@gmail.com", user_role=2, username="Admin User" where id=2`
      )
      await Database.raw(
        `update users set email="superuser@gmail.com", user_role=4, username="Super User" where id=3`
      )
      console.log('updat databas for admin:', s)
    } catch (e) {
      console.log('ERROR MIGRATION AND SEEDER', e)
    }
  }

  async cleanupMigration (field, type) {
    if (type === 'all') {
      var truncate = true
    } else {
      var truncate = await this.confirm('truncate table ?')
    }

    if (truncate) {
      let name = field.name
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()

      let tableName = pluralize.plural(name)
      try {
        if (field.name === 'user') {
          if (use('Env').get('DB_CONNECTION') === 'sqlite') {
            await Database.raw(`drop table IF EXISTS ${tableName}`)
            // await Database.raw(`drop table IF EXISTS tokens`)
            console.log('sqlite just drop table ', tableName)
          } else {
            await Database.raw(`SET FOREIGN_KEY_CHECKS = 0`)
            // await Database.raw(`truncate table tokens`)
            // await Database.raw(`truncate table ${tableName}`)
            await Database.raw(`drop table ${tableName}`)
            await Database.raw(`SET FOREIGN_KEY_CHECKS = 1`)
            console.log('truncate table ', tableName)
          }
        } else {
          await Database.raw(`SET FOREIGN_KEY_CHECKS = 0`)
          await Database.raw(`drop table ${tableName}`)
          await Database.raw(`SET FOREIGN_KEY_CHECKS = 1`)
          console.log('drop table ', tableName)
        }

        await Database.raw(
          `delete from adonis_schema where name like '%_${name}%'`
        )
        console.log(`remove ${tableName} in adonis_schema table `)
      } catch (e) {
        console.log('error drop table : ', e.sqlMessage)
      }

      let file = await fs.readdirSync(Helpers.appRoot(`database/migrations`))
      let cekFile = file.filter(file => file.indexOf(`${name}.js`) > 0)

      if (cekFile.length > 0) {
        const filename = `database/migrations/${cekFile[0]}`
        await this.removeFile(Helpers.appRoot(filename))
        console.log('cleanupView ', filename)
      } else {
        console.log(`${name} migration is not exist`)
      }
    } else {
      console.log('canceled trucate table and cleanup migration')
    }
  }

  async cleanupSeeder (field) {
    const filename = `database/seeds/${this.capitalizeFirstLetter(
      field.name
    )}Seeder.js`
    await this.removeFile(Helpers.appRoot(filename))
    console.log('cleanupSeeder ', filename)
  }

  async cleanupView (field) {
    let name = field.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()
    await this.removeFile(Helpers.viewsPath(`admin/${name}`))
    console.log('cleanupView ', Helpers.viewsPath(`admin/${name}`))
  }

  async cleanupCtrl (field) {
    let name = `app/Controllers/Http/Admin/${this.capitalizeFirstLetter(
      field.name
    )}Controller.js`

    await this.removeFile(Helpers.appRoot(name))
    console.log('cleanupCtrl ', name)
  }

  async cleanupApiCtrl (field) {
    let name = `app/Controllers/Http/Api/V1/${this.capitalizeFirstLetter(
      field.name
    )}Controller.js`

    await this.removeFile(Helpers.appRoot(name))
    console.log('cleanupAPICtrl ', name)
  }

  async cleanupModel (field) {
    if (field.name !== 'user') {
      let name = `app/Models/${this.capitalizeFirstLetter(field.name)}.js`
      await this.removeFile(Helpers.appRoot(name))
      console.log('cleanupModel ', name)
    } else {
      console.log('skip cleanupModel ', field.name)
    }
  }

  async cleanupValidator (field) {
    let store = `app/Validators/Store${this.capitalizeFirstLetter(
      field.name
    )}.js`

    let update = `app/Validators/Update${this.capitalizeFirstLetter(
      field.name
    )}.js`

    await this.removeFile(Helpers.appRoot(store))
    await this.removeFile(Helpers.appRoot(update))

    console.log('cleanupValidator  ', store, update)
  }

  async cleanupAll (field, type, file) {
    console.log('file ', file)
    // await this.cleanupView(field)
    await this.cleanupModel(field)
    await this.cleanupSeeder(field)
    await this.cleanupCtrl(field)
    await this.cleanupApiCtrl(field)
    await this.cleanupValidator(field)
    await this.cleanupMigration(field, type)
    await this.updateMenu(field, 'remove')
    await this.updateRoute(field, 'remove')
    await this.updateApiRoute(field, 'remove')
    await this.updateFactory(field, 'remove')
    await this.updateAuth(field, 'remove', file)
  }

  async updateMenu (props, type) {
    if (props.hiddenMenu) {
      return false
    }

    const menu = await this.readFile(
      Helpers.viewsPath(`admin/layout/partials/menu.edge`),
      'utf-8'
    )

    let label = props.name
      .split(/(?=[A-Z])/)
      .join(' ')
      .toLowerCase()

    let rname = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    const a = menu.match(/(collection\:\[[^]+])/g)
    const b = a[0].replace(/^\s|\s$|collection\:/g, '')
    const c = JSON.parse(b)
    const isExist = c.filter(i => i.name === props.name)
    if (isExist.length === 0 && type !== 'remove') {
      const routeName = `admin.${rname}.index`
      const iconName =
        typeof props.iconMenu !== 'undefined'
          ? props.iconMenu
          : 'fa-chevron-right'

      c.push({
        label: label,
        name: props.name,
        route: routeName,
        icon: 'fa-chevron-right'
      })

      let baru = menu.replace(/(\[[^]+])/, JSON.stringify(c))
      baru = baru
        .replace(/\}\,/g, '},\n\t\t')
        .replace(/\[/g, '[\n\t\t')
        .replace(/\]/g, '\n\t    ]')
      await this.writeFile(
        Helpers.viewsPath(`admin/layout/partials/menu.edge`),
        baru
      )
      console.log(`successfully create menu ${props.name}`)
    } else {
      if (type === 'remove') {
        var removeMenu = c.filter(i => i.name !== props.name)
        let menuStr = JSON.stringify(removeMenu)
        let baru = menu.replace(/(\[[^]+])/, menuStr)
        baru = baru
          .replace(/\}\,/g, '},\n\t \t')
          .replace(/\[/g, '[\n\t\t')
          .replace(/\]/g, '\n\t    ]')
        await this.writeFile(
          Helpers.viewsPath(`admin/layout/partials/menu.edge`),
          baru
        )
        console.log(`successfully remove menu ${props.name}`)
      }
      console.log(`menu ${props.name} is already exist`)
    }
  }

  async updateRoute (props, type) {
    const adminRoutes = await this.readFile(
      Helpers.appRoot(`start/routes/admin.js`),
      'utf-8'
    )
    const name = this.capitalizeFirstLetter(props.name)
    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    if (adminRoutes.indexOf(`${name}Controller`) === -1 && type !== 'remove') {
      const b = `
        Route.resource("${routeName}", "${name}Controller").validator(
          new Map([
            [["admin.${routeName}.store"], ["Store${name}"]],
            [["admin.${routeName}.update"], ["Update${name}"]]
          ])
        );\n$&
        `
      let a = adminRoutes.replace(/\}\)[^]+\;?$/g, b)
      await this.writeFile(Helpers.appRoot(`start/routes/admin.js`), a)
      console.log(`successfully update route ${routeName}`)
    } else {
      if (type === 'remove') {
        var rgx = new RegExp(
          `Route.resource\\((\"${routeName}\")\\,\\s?(\"${name}Controller\")\\).validator(([\\s\\S]*?))\\;+[\\n\\t]+`,
          'gi'
        )
        let x = adminRoutes.replace(rgx, '')
        await this.writeFile(Helpers.appRoot(`start/routes/admin.js`), x)
        console.log(`successfully remove route ${routeName}`)
      }
      // console.log(`${routeName} route already exist`);
    }
  }

  async updateApiRoute (props, type) {
    console.log('start updateApiRoute')
    const adminRoutes = await this.readFile(
      Helpers.appRoot(`start/routes/api.js`),
      'utf-8'
    )

    const name = this.capitalizeFirstLetter(props.name)
    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    if (adminRoutes.indexOf(`${name}Controller`) === -1 && type !== 'remove') {
      const b = `
        Route.resource("${routeName}", "${name}Controller").validator(
          new Map([
            [["api.${routeName}.store"], ["Store${name}"]],
            [["api.${routeName}.update"], ["Update${name}"]]
          ])
        ).apiOnly();\n$&
        `
      let a = adminRoutes.replace(/\}\)[^]+\;?$/g, b)
      console.log('api route ', a)
      await this.writeFile(Helpers.appRoot(`start/routes/api.js`), a)
      console.log(`successfully update api route ${routeName}`)
    } else {
      if (type === 'remove') {
        var rgx = new RegExp(
          `Route.resource\\((\"${routeName}\")\\,\\s?(\"${name}Controller\")\\).validator(([\\s\\S]*?)).apiOnly()\\;+[\\n\\t]+`,
          'gi'
        )
        let x = adminRoutes.replace(rgx, '')
        await this.writeFile(Helpers.appRoot(`start/routes/api.js`), x)
        console.log(`successfully remove api route ${routeName}`)
      }
      // console.log(`${routeName} route already exist`);
    }
  }

  async updateAuth (props, type, lastfile) {
    if (lastfile) {
      let splitStr = lastfile.split('.')
      if (splitStr.length > 1) {
        lastfile = splitStr[1].replace(/\_/g, '-')
      }
    }

    let authFile = await this.readFile(
      Helpers.appRoot(`authorization/admin.json`),
      'utf8'
    )

    let routeName = props.name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()

    if (authFile.indexOf(`"${routeName}":`) === -1 && type !== 'remove') {
      if (authFile.indexOf('delete') === -1) {
        let b = `"${routeName}":{
        "get": true,
        "post": true,
        "put": true,
        "patch": true,
        "delete": true
    }${lastfile === routeName || lastfile === 'nocomma' ? '' : ','}
  }`

        let a = authFile.replace(/\}$/gim, b)
        console.log('new auth ', a)
        await this.writeFile(Helpers.appRoot(`authorization/admin.json`), a)
        console.log(`successfully update auth ${routeName}`)
      } else {
        let b = `"delete": true
      },
      "${routeName}":{
        "get": true,
        "post": true,
        "put": true,
        "patch": true,
        "delete": true
    }${lastfile === routeName || lastfile === 'nocomma' ? '' : ','}
`

        let a = authFile.replace(/"delete": true[\s]*?}\s/gim, b)
        console.log('update auth ', a)
        await this.writeFile(Helpers.appRoot(`authorization/admin.json`), a)
        console.log(`successfully update auth ${routeName}`)
      }
    } else {
      if (type === 'remove') {
        var rgx = new RegExp(
          `"${routeName}":\\s?{[\\s\\S]*?"delete"[\\s\\S]*?\\}\\,?`,
          'gm'
        )
        let x = authFile.replace(rgx, '')

        var rgx1 = new RegExp('\\}\\,[\\s]*\\}$', 'igm')
        let y = x.replace(
          rgx1,
          `}
      }`
        )

        await this.writeFile(Helpers.appRoot(`authorization/admin.json`), y)
        console.log(`successfully remove route ${routeName}`)
      }
    }
  }

  async cleanApp (field) {
    const fileIndex = `${__dirname}/../../../src/pages/admin/${this.capitalizeFirstLetter(
      field.name
    )}.vue`
    const fileForm = `${__dirname}/../../../src/pages/admin/${this.capitalizeFirstLetter(
      field.name
    )}Form.vue`
    const fileStore = `${__dirname}/../../../src/store/${field.name}.js`
    await this.removeFile(fileIndex)
    await this.removeFile(fileForm)
    await this.removeFile(fileStore)
    console.log('cleanup file, form, store ')
  }

  async handle (args, options) {
    const choose_action = await this.choice('mau create apa?', [
      'create-app',
      'create-all-app',
      'clean-app',
      'clean-all-app',
      'create',
      'clean',
      'create-all',
      'clean-all'
    ])

    if (choose_action === 'create-app') {
      let choice = []
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      file.forEach(i => choice.push({ name: i, value: i }))
      const choose_generator_filename = await this.choice(
        'pilih file ?',
        choice
      )
      // const choose_generator_filename = 'user.json';
      const c = JSON.parse(
        await this.readFile(
          Helpers.appRoot(`generator/${choose_generator_filename}`),
          'utf-8'
        )
      )
      await this.createView(c, 'index')
      await this.createView(c, 'form')
      await this.createStore(c)
      await this.updateAppRoute(c)
      await this.updateLayoutMenu(c)
      await this.updateIndexStore(c)
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js,.vue ${__dirname}/../../../src --fix --ignore-path ${__dirname}/../../../.eslintignore`
      )
      return false
    }

    if (choose_action === 'clean-app') {
      let choice = []
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      file.forEach(i => choice.push({ name: i, value: i }))
      const choose_generator_filename = await this.choice(
        'pilih file ?',
        choice
      )
      // const choose_generator_filename = 'user.json';
      const c = JSON.parse(
        await this.readFile(
          Helpers.appRoot(`generator/${choose_generator_filename}`),
          'utf-8'
        )
      )
      await this.cleanApp(c)
      await this.updateAppRoute(c, 'remove')
      await this.updateLayoutMenu(c, 'remove')
      await this.updateIndexStore(c, 'remove')
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js,.vue ${__dirname}/../../../src --fix --ignore-path ${__dirname}/../../../.eslintignore`
      )
      return false
    }

    if (choose_action === 'clean-all') {
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      for (var i = 0; i < file.length; i++) {
        console.log(`execute file generator/${file[i]}`)
        console.log(
          '\n=============================================================\n'
        )
        var bc = JSON.parse(
          await this.readFile(Helpers.appRoot(`generator/${file[i]}`), 'utf-8')
        )
        await this.cleanupAll(bc, 'all', file.slice(file.length - 1)[0])
        console.log(
          '\n=============================================================\n\n'
        )
      }
      console.log('finish cleanup all')
      Database.close()
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js ${__dirname}/../../../api --fix`
      )
      return false
    }

    if (choose_action === 'create-all-app') {
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      for (var i = 0; i < file.length; i++) {
        console.log(`execute file generator/${file[i]}`)
        console.log(
          '\n=============================================================\n'
        )
        var cx = JSON.parse(
          await this.readFile(Helpers.appRoot(`generator/${file[i]}`), 'utf-8')
        )
        await this.createView(cx, 'index')
        await this.createView(cx, 'form')
        await this.createStore(cx)
        await this.updateAppRoute(cx)
        await this.updateLayoutMenu(cx)
        await this.updateIndexStore(cx)

        console.log(
          '\n=============================================================\n\n'
        )
      }
      console.log('finish create  all')
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js,.vue ${__dirname}/../../../src --fix --ignore-path ${__dirname}/../../../.eslintignore`
      )
      Database.close()
      return false
    }
    if (choose_action === 'clean-all-app') {
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      for (var i = 0; i < file.length; i++) {
        console.log(`execute file generator/${file[i]}`)
        console.log(
          '\n=============================================================\n'
        )
        var co = JSON.parse(
          await this.readFile(Helpers.appRoot(`generator/${file[i]}`), 'utf-8')
        )

        await this.cleanApp(co)
        await this.updateAppRoute(co, 'remove')
        await this.updateLayoutMenu(co, 'remove')
        await this.updateIndexStore(co, 'remove')

        console.log(
          '\n=============================================================\n\n'
        )
      }
      console.log('finish create  all')
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js,.vue ${__dirname}/../../../src --fix --ignore-path ${__dirname}/../../../.eslintignore`
      )
      Database.close()
      return false
    }

    if (choose_action === 'create-all') {
      let file = await fs.readdirSync(Helpers.appRoot(`generator`))
      for (var i = 0; i < file.length; i++) {
        console.log(`execute file generator/${file[i]}`)
        console.log(
          '\n=============================================================\n'
        )
        var bc = JSON.parse(
          await this.readFile(Helpers.appRoot(`generator/${file[i]}`), 'utf-8')
        )
        await this.createModel(bc)
        await this.createMigration(bc)
        await this.createSeeder(bc)
        await this.updateFactory(bc)
        await this.createValidator(bc, 'store')
        await this.createValidator(bc, 'update')
        await this.createController(bc)
        await this.createApiController(bc)
        // await this.createView(bc, 'index')
        // await this.createView(bc, 'create')
        // await this.createView(bc, 'edit')
        // await this.updateMenu(bc)
        await this.updateRoute(bc)
        await this.updateApiRoute(bc)
        // await this.updateAuth(bc, "", file.slice(file.length - 1)[0]);
        await this.updateAuth(bc, '', 'nocomma')
        await this.executeMigrationAndSeeder(bc)
        console.log(
          '\n=============================================================\n\n'
        )
      }
      console.log('finish create  all')
      exec(
        `${__dirname}/../../../node_modules/.bin/eslint --ext .js ${__dirname}/../../../api --fix`
      )
      Database.close()
      return false
    }

    let choice = []
    let file = await fs.readdirSync(Helpers.appRoot(`generator`))
    file.forEach(i => choice.push({ name: i, value: i }))
    const choose_generator_filename = await this.choice('pilih file ?', choice)
    // const choose_generator_filename = 'user.json';
    const c = JSON.parse(
      await this.readFile(
        Helpers.appRoot(`generator/${choose_generator_filename}`),
        'utf-8'
      )
    )

    if (choose_action === 'create') {
      // await this.cleanupAll(c);
      await this.createModel(c)
      await this.createMigration(c)
      await this.createSeeder(c)
      await this.updateFactory(c)
      await this.createValidator(c, 'store')
      await this.createValidator(c, 'update')
      await this.createController(c)
      await this.createApiController(c)
      // await this.createView(c, 'index')
      // await this.createView(c, 'create')
      // await this.createView(c, 'edit')
      await this.updateMenu(c)
      await this.updateRoute(c)
      await this.updateApiRoute(c)
      await this.updateAuth(c, '', 'nocomma')

      const builddb = await this.choice(
        'Do you want to running migration and seeder ?',
        ['no', 'only migration', 'only seeder', 'migrate and seed']
      )
      if (builddb === 'only migration') {
        const { stdout: m } = await exec('adonis migration:run')
        console.log('migration:', m)
      } else if (builddb === 'only seeder') {
        const { stdout: s } = await exec(
          `adonis seed --files ${this.capitalizeFirstLetter(c.name)}Seeder.js`
        )
        console.log('seeder:', s)
      } else if (builddb === 'migrate and seed') {
        await this.executeMigrationAndSeeder(c)
      }

      // if (await this.confirm('Do you want to run migration and seeder')) {
      //   await this.executeMigrationAndSeeder(c)
      // }
    }

    if (choose_action === 'clean') {
      await this.cleanupAll(c, '')
    }

    Database.close()
    exec(
      `${__dirname}/../../../node_modules/.bin/eslint --ext .js ${__dirname}/../../../api --fix`
    )
  }
}

module.exports = Crud
