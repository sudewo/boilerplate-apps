'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response, session }) {
    const Config = use('Config')
    const apiVersion = Config.get('app.api_version')
    console.log(error)
    return response
      .status(error.status)
      .send({
        ...request.all(),
        __version: apiVersion,
        __status: 'error',
        __message: 'error from server, please call administrator',
        __error: error.toString(),
        __validation: null
      })

    // if (error.name === 'ValidationException') {
    //   session.withErrors(error.messages).flashAll()
    //   await session.commit()
    //   response.redirect('back')
    //   return
    // }

    // return super.handle(...arguments)
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  // async report (error, { request }) {
  // }
}

module.exports = ExceptionHandler
