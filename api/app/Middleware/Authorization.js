'use strict'
const Helpers = use('Helpers')
const fs = require('fs')
class Authorization {
  async handle ({ request, response, auth }, next) {
    if (parseInt(auth.user.user_role) > 2) {
      await next()
      return false
    }

    let authLabel = ['member', 'admin', 'superuser']

    try {
      const content = await fs.readFileSync(
        Helpers.appRoot(`authorization/${authLabel[auth.user.user_role]}.json`),
        'utf-8'
      )
      const authorizeM = JSON.parse(content)
      const currentMethod = request.method().toLowerCase()
      const page = request
        .originalUrl()
        .split('/')
        .filter(i => i !== '' && i !== 'admin')

      if (page.length > 0) {
        if (page[0] === 'user') {
          if (Number(page[1]) !== parseInt(auth.user.id)) {
            return response.send({
              __status: 'error',
              __message: `you dont have authorize to change account`,
              __error: ''
            })
          }
        }
        if (typeof authorizeM[page[0]] !== 'undefined') {
          const allowedMethod = authorizeM[page[0]]
          if (typeof allowedMethod[currentMethod] !== 'object') {
            if (!allowedMethod[currentMethod]) {
              return response.send({
                __status: 'error',
                __message: `you dont have authorize to access ${currentMethod} method`,
                __error: ''
              })
            }
          } else {
            let lastSegment = page.length === 1 ? 'index' : page.slice(-1)[0]
            lastSegment = lastSegment.replace(/\W/gi, '')
            if (!allowedMethod[currentMethod][lastSegment]) {
              return response.send({
                __status: 'error',
                __message: `you dont have authorize to access ${lastSegment} method`,
                __error: ''
              })
            }
          }
        }
      }
    } catch (e) {
      response.send({
        __status: 'error',
        __message:
     "can't find authorization file, check authorization file in authorization folder",
        __error: ''
      })

      return
    }

    await next()
  }
}

module.exports = Authorization
