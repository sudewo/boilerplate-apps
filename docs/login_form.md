### LAYOUTING

1. login form
2. remove unsed layout qdrawer
    ```html
    <q-layout view="lHh Lpr lFf">
        <q-header elevated>
          <q-toolbar>
            <q-toolbar-title>
              Oauth
            </q-toolbar-title>

            <div>Quasar v{{ $q.version }}</div>
          </q-toolbar>
        </q-header>

        <q-page-container>
          <router-view />
        </q-page-container>
      </q-layout>
    ```

3. create login page
      ```js
      quasar new page Login

      //result
      //app:new Generated page: src/pages/Login.vue +0ms
      //app:new Make sure to reference it in src/router/routes.js +3ms
    ```
4. form layout
   - quasar layout grid seperti bootstrap ada terdiri dari row dan col (xl, lg, md, sm, xs)
   - quasar row pada quasar adalah flexbox based, secara default cssnya (display flex dan wrap)
   - untuk mengatur content didalam row, dapat menggunakan css flexbox class, ex: justify-center, items-center, etc.
   - untuk mengatur panjang lebar sebuah box. dapat menggunakan col, kita biset col-lg-1, col-xs-12.
  ex :
      ```html
        <q-page class="row flex-center">
            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 col-xs-12 q-px-xs-lg">
            </div>
        </q-page>

        <!-- notes
         row: form login center ditengah (row flex-center),
         col: panjang form login responsive, full untuk ukuran layar kecil dengan padding besar(q-px-xs-lg)
        -->
      ```
   ex responsive layout pada quasar :
    ```html
      <template>
          <q-page class="row flex-center">
            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 col-xs-12 q-px-xs-lg">
            <q-form
              @submit="onSubmit"
              @reset="onReset"
            >
              <q-input
                filled
                v-model="email"
                label="Your email *"
                hint="Your email address"
                lazy-rules
                :rules="[ val => val && val.length > 0 || 'Please type something']"
              />

              <q-input v-model="password" filled :type="isPwd ? 'password' : 'text'" hint="Password with toggle" lazy-rules
                :rules="[
                  val => val !== null && val !== '' || 'Please type your password',
                  val => val.length > 3 && val.length < 100 || 'Please type a real password'
                ]"
                >
                <template v-slot:append>
                  <q-icon
                    :name="isPwd ? 'visibility_off' : 'visibility'"
                    class="cursor-pointer"
                    @click="isPwd = !isPwd"
                  />
                </template>
              </q-input>

              <div>
                <q-btn label="Submit" type="submit" color="primary"/>
                <q-btn label="Reset" type="reset" color="primary" flat class="q-ml-sm" />
              </div>
            </q-form>
            </div>
          </q-page>
        </template>
      ```



### TEST LOGIN
1. proxy api to prevent cors, update `quasar.conf.js`
   ```js
   proxy: {
        // proxy all requests starting with /api to jsonplaceholder
        '/api': {
          target: 'http://localhost:3333',
          changeOrigin: true,
          pathRewrite: {
            '^/api': ''
          }
        }
      }
      // `/api/auth` -> `http://localhost:3333/auth`
   ```
2. create login script
    ```js
    fetch(`/api/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({ email: this.email, password: this.password })
      }).then(i => i.json()).then(i => {
        console.log('RESPONSE', i)
        if (Array.isArray(i)) {
          this.$q.notify({
            color: 'red-4',
            textColor: 'white',
            icon: 'fas fa-check-circle',
            message: i[0].message
          })
        } else {
          this.$q.notify({
            color: 'green-4',
            textColor: 'white',
            icon: 'fas fa-check-circle',
            message: i.token
          })
        }
      })
      ```


### VALIDATION

1. install `adonis install @adonisjs/validator`
2. add provider
   ```js
      const providers = [
        '@adonisjs/validator/providers/ValidatorProvider'
      ]
   ```
3. Next create the validator file inside app/Validators directory, or use the ace command.
   ```js
    adonis make:validator Auth
    //app/Validators/Auth.js
   ```
4. adding validator in route
   ```js
      Route
        .post('login', 'AuthController.login')
        .validator('Auth')
   ```
