### testing cordova
- setelah mengimplementasikan simple authtentication kita akan coba testing di ios dan android simulator
- run `quasar dev -m android` jika cordova belum terinstall maka command ini akan menginstall
- handle error `disallowed user agent`
  update `src-cordova/config.xml`
    ```js
    <preference name="OverrideUserAgent" value="Mozilla/5.0 Google" />
    ```
- untuk menghandle error  net:ERR_CLEARTEXT_NOT_PREMITTED (http://123.123.123.109:8080)
  - update `src-cordova/config.xml`

  - tambhakan pada platform android
    ````xml
    <platform name="android">
          <allow-intent href="market:*" />
          <edit-config file="app/src/main/AndroidManifest.xml" mode="merge" target="/manifest/application">
            <application android:usesCleartextTraffic="true" />
        </edit-config>
      </platform>
    ````

  - tambahkan `xmlns:android="http://schemas.android.com/apk/res/android"` pada widget
    ```xml
    <widget id="org.buatin.boilerplate.app" version="0.0.1" xmlns:android="http://schemas.android.com/apk/res/android" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
    ```
  - exit termnial dan re run `quasar dev -m android`

### debuging dengan chrome
  - agar api bisa diakses melalui network, ganti alamat server adonis `.env`
    ganti host menjadi `0.0.0.0`
    dan ganti `quasar.conf.js`
    update ip API menjadi external ip `123.123.123.109:3333`
  - inspect device pada android :  chrome://inspect, done.
  - inspect device pada ios : safari  (develop -> 192.168..)

### add splashscreen plugin
- install splashscreen plugin `cd src-cordova && cordova plugin add cordova-plugin-splashscreen`
  1. bugfixing display `notch iphone x` pada ios emulator
     - install @quasar/icon-genie untuk mengenerate icon dan splashscreen untuk android,ios
       `quasar ext add @quasar/icon-genie`s
       - untuk development pilih `pngquant` untuk mempercepat waktu build
     - create icon menggunakan figma pastikan ukurannya
        ```js
          -> app-icon.png           1240x1240   (with transparency)
          -> app-splashscreen.png   2436x2436   (transparency optional)
        ```
     - jika ukuran tidak sesuai maka display pada notch tetap akan salah, pastikan ukurannya benar.
     - penting jika ingin menganti icon atau splashscreen (app-icon.png, app-splashcreen.png) lalu testing dengan simulator, kita harus mereset setingan device dan clear contents, jika tidak splashscreen tidak akan berubah, karna simulator menyimpan cache launch image.
     - rebuild and testing done.
     - blm tau kenapa notch massih problem, sementara fix pake css
     ```css
      body.cordova header{
        padding-top:constant(safe-area-inset-top);
        padding-top:env(safe-area-inset-top);
      }
     ```
  2. bugfixing crop image ketika launch pertama kali pada android emulator
      - edit file ganti default image menjadi text
      - reload device pada chrome://inspect
  3. fix splasscreen ratio
    ```js android only
      <platform name="android">
        <preference name="SplashMaintainAspectRatio" value="true" />
      </platform>
    ```
### install microsoft code-push

   0. install
       - cordova plugin add cordova-plugin-code-push@latest
   1. add plugin
      - code-push add app ios cordova
      - code-push add app android cordova
   2. show push update and deployment key
      - code-push deployment ls app -k
   3. update config.xml for android and ios platform
       ```js
        <platform name="ios">
         <preference name="CodePushDeploymentKey" value="YOUR DEVELOPMENT KEY" />
        </platform>
       ```
   4. release update
      ```js
        // first update "src" folder to "src-cordova":
        quasar build -m android --skip-pkg
        // then release update :
        code-push release-cordova app android -d Staging
      ```
    1. listen change
        ```js app.vue
          document.addEventListener('deviceready', function() {
            // eslint-disable-next-line no-undef
            codePush.sync()
          })
        ```
    2. docs
       https://github.com/microsoft/cordova-plugin-code-push#how-does-it-work
