### building vuejs plugin
- vuejs oauth plugin adalah plugin yang digunakan untuk connect ke beberapa social media ex: facebook,google,instagram kemudian memberikan response basic profile
  ```js
  //ex response
  {
      status: "success",
      data: {id: "123123123", name: "Jone", email: "xxx@gmail.com"}
      errorMessage: ""
      status: "success"
      type: "fb"
  }
  ```

- plugin ini membutuh kan parameter options sebagi berikut, required paramternya(client_id, client_secret, redirect_uri)
  ```js
    {
      fb: {
        client_id: '',
        client_secret: '',
        redirect_uri: 'http://localhost:8080/login'
      },
      google: {},
      instagram: {},
      twitter: {},
    }
  ```

- create vuejs plugin

  simplenya vuejs plugin hanya memerlukan sebuah object, dengan method `install`
  yang terdapat parameter `Vue` dan `options`

  kemudian kita mengassign `oauth` plugin ke dalam `vue prototype`, agar kita dapat memgunakannya dalam setiap vue compoenent dengan `this.$oauth.`, contohnya seperti vue router `this.$router`.

  ```js
    import oauth from './oauth'
    export default {
      install (Vue, options) {
        console.log('OAUTH PLUGIN LOADED!', options)
        Vue.prototype.$oauth = oauth
        Vue.prototype.$oauth.init(options, new Vue())
      }
    }
  ```

- cara meregistrasikan plugin yang telah kita buat dengan `Vue.use`
  ```js
    import oauth from '../plugins/oauth' //import plugin
    Vue.use(oauth, {
      fb: {
        client_id: '',
        client_secret: '',
        redirect_uri: 'http://localhost:8080/login'
      },
      google: {},
      instagram: {},
      twitter: {},
    }) //inject plugin kedalam vue.use
  ```

- cara menggunakan vue plugins, ex :
  ```js
      //call function oauth login with facebook
      @click="$oauth.loginWith('fb')"

      //listen response
      mounted(){
        this.$oauth.bus.$on('OAUTH_RESPONSE', (val) => {
          console.log('OAUTH_RESPONSE : ', val)
        })
        ...
      }
    })
  ```

### Using Vuejs plugin in quasar
- register vuejs plugin pada quasar framework dapat menggunakan `boot files`, boot files digunakan untuk menginject script yang akan di load pada saat initialiasi, ex vuejs plugin
   - create boot files -> `quasar new boot oauth` This command creates a new file: /src/boot/<name>.js :
   - registrasi oauth vuejs plugins
      ```js
          // import something here
          import oauth from '../plugins/oauth'
          // "async" is optional
          export default async ({ Vue }) => {
            // something to do
            Vue.use(oauth, {
              fb: {
                client_id: '',
                client_secret: '',
                redirect_uri: 'http://localhost:8080/login'
              },
              ...
            })
          }

      ```
   - registrasikan boot files pada `quasar.conf.js`
      ```js
      module.exports = function (ctx) {
          return {
            // app boot file (/src/boot)
            // --> boot files are part of "main.js"
            boot: [
              'oauth'
            ],
            ...
      }
      ```

