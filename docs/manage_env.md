# Manage .env file for development environtment
 1. install `@auasar/dotenv` extension
    ```js
      quasar ext add @quasar/dotenv

      //You will be asked a few questions. Type in your answers:
      ? What is the name of your .env that you will be using for development builds? .env.dev
      ? What is the name of your .env that you will be using for production builds? .env.prod
      ? What name would you like to use for your Common Root Object ('none' means to not use one)? none
      ? Create your .env files for you? Yes
      ? For security, would you like your .env files automatically added to .gitignore? Yes
    ```

1. for development  use `.env.dev` and production use `.env.prod`
    ```js
      #example .env.dev

      FACEBOOK_CLIENT_ID=xxxxx
      FACEBOOK_CLIENT_SECRET=xxxxxx
      FACEBOOK_REDIRECT_URI=http://localhost:8080/login
    ```
2. example of using .env configuration
    ```js
      Vue.use(oauth, {
        fb: {
          client_id: process.env.FACEBOOK_CLIENT_ID,
          client_secret: process.env.FACEBOOK_CLIENT_SECRET,
          redirect_uri: process.env.FACEBOOK_REDIRECT_URI
        },
        ...
      })
    ```
     to using env config we just call `process.env.ENV_NAME`, while quasar app running on development this extensions is automatically call `.env.dev` and for production build `.env.prod`.

