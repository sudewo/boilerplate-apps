### management vuex store
  1. genreate store dengan quasar command
      ```js
          buatin-api ➜ quasar new store auth
          app:new Generated store: src/store/auth +0ms
          app:new Make sure to reference it in src/store/index.js +2ms
      ```
  1. index store
     - untuk development update vuex store menjadi `strict` , berfungsi untuk menampilkan perubahan state dari waktu ke waktu pada `vue debuging tools`
     - import module auth yang sudah dibuat sebelumnya kedalam index store `store/index.js`
     - secara default vuexstore pada quasar ada vuex modular dengan namespace true, jadi pemanggilan action/mutation harus disertai dengan     dengan modulnya : ex action `this.store.dispatch('auth/FETCH')`
     -
  2. manage state
     - jika bisa, untuk menghindari `undefined` atau penambahan property atau object baru kedalam state pada saat mutation, lebih baik
       state dideclare semua pada saat awal pembuatan, selanjutnya kita copy saja response api kedalam state, buang property yang tidak dibutuhkan, bisa ditambah dengan yang perlu, ex: loading state, etc
     - jalankan `.http` REST CLIENT lihat hasil json dari `api/auth` kemudian paste pada `store/auth/state.js`
       ```js
       //example of state.js from http response
       {
          'id': null,
          'username': null,
          'email': null,
          'password': null,
          'created_at': null,
          'updated_at': null,
          'type': null,
          'token': null,
          'refreshToken': null
        }
       ```

  3. manage mutation
       - secara default sample mutation.js meng export sebuah function
           ```js
             /*export function someMutation (state) {}*/
           ```


         - jika banyak mutation yang akan kita buat maka kita harus meng export semua function nya ex :

            ```js
               export function loading (state, payload = true) {
                 state.loading = payload
               }

               export function success (state, payload) {
                 payload.loading = false
                 payload.status = 'success'
                 Object.assign(state, payload)
               }

               export function failed (state, payload) {
                 payload.loading = false
                 payload.status = 'failed'
                 Object.assign(state, payload)
               }

               export function error (state, payload) {
                 payload.loading = false
                 payload.status = 'error'
                 Object.assign(state, payload)
               }
            ```
          - gunakan Ojbect.assign untuk merge object baru, tanpa menhilangkan referencenya

           ```js
             state = {...state, ...payload}
             console.log('new state', state)
           ```

          - jika kita gunakan desctructive opertor untuk menggabung 2 object menjadi object yang baru, maka referencenya hilang dan state menjadi tidak reactive, fungsi watch pada lifecycle tidak akan mendeteksi perubahan pada component, walaupun console.log diatas sudah menampilkan object yang baru.



  4. manage action
      implementasi fetch auth dengan vuex action
      ```js
          export function login ({ state, commit }, params) {
            commit('loading')
            fetch(`/api/auth/login`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify(params)
            }).then(i => i.json()).then(i => {
              console.log('RESPONSE FROM SERVER', i)
              if (!Array.isArray(i)) {
                i.message = 'Login Success'
                commit('success', i)
              } else {
                console.log('failed', i)
                i.message = i[0].message
                commit('failed', { message: i.message })
              }
            }).catch(e => {
              commit('error', { message: e.error })
            })
          }
      ```
    1. deteksi perubahan dengan watch, untuk vuex module watch perubahan membutuhkan handler dan deep true.

        ```js
            watch: {
              auth: {
                handler (val) {
                console.log('FIRE', val);
                },
                deep: true
              }
            }
        ```

    2. pada saat kita jalankan script diatas, watch tidak akan terdektesi pada console.log, knp ? karna kita lupa menambahkan property loading pada state, loading adalah property baru yang blm ada pada state yang sudah kita define diawal, jadi fungsi `watch` tidak ter triger. solusinya tambahkan property loading pada state.
        ```js
          {
            'loading': false,
            'message': null,
            ...
          }
        ```
    3. udpdate script watch untuk menampilkan toast error atau success jika state berubah
          ```js
            if (val.loading) return false
                this.$q.notify({
                  color: val.status === 'success' ? 'green-4' : 'red-4',
                  textColor: 'white',
                  icon: 'fas fa-check-circle',
                  message: val.message
                })
          ```
    4. done

### navigation guard pada route
  - create page user  dan tambahkan routes
    ```js
      quasar new page User
      app:new Generated page: src/pages/User.vue +0ms
      app:new Make sure to reference it in src/router/routes.js +3ms
    ```

  - update simple page untuk menampilkan data authentication dari vuex store
    ```js
        <template>
          <q-page padding>
            <h3>User</h3>
            <pre>{{auth}}</pre>
          </q-page>
        </template>

        <script>
        import { mapState } from 'vuex'
        export default {
          name: 'User',
          computed: {
            ...mapState(['auth'])
          }
        }
        </script>
    ```
    - access page `/#/user`

    - add navigation guard pada routes

      -   note: sebelumnya sudah menjoba guard per-route, dengan before enter, hanya saja jika berpindah halaman muncul error , karna munkin saya menginisialisai store() baru ada fungsi midlewarenya, jadi timbul error
            ```js
            vue.runtime.esm.js?2b0e:619 [Vue warn]: Error in callback for watcher "function () { return this._data.$$state }": "Error: [vuex] do not mutate vuex store state outside mutation handlers."
            ```

    - solusinya kita akan menggunakan global routes `https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards`. next update `/router/index.js` untuk membuat globar route dengan `beforeEach`, untuk global route, disini ada variable store bawaan, yang bisa digunakan untuk mengambil nilai authentikiasi pada store. daripada harus mengimport dan mengiinisialisaiskan store baru.
    ```js
          Router.beforeEach((to, from, next) => {
            let allowedToEnter = true
            let isLoggedIn = store.getters['auth/isLogin']

            to.matched.some((record) => {
              //jika halama pertama yang dibuka adalah /user maka redirect ke login
              if (!isLoggedIn && record.name === 'user') {
                next({
                  path: '/login',
                  replace: true
                })
              }

              //jika routes mengandung propery meta dan requireAuth maka di proses.
              if ('meta' in record) {
                if ('requiresAuth' in record.meta) {
                  if (record.meta.requiresAuth) {
                    // this route requires auth, check if user is logged in
                    // if not, redirect to login page.
                    if (!isLoggedIn) {
                      // User is not logged in, redirect to signin page
                      allowedToEnter = false
                      next({
                        path: '/login',
                        replace: true,
                        // redirect back to original path when done signing in
                        query: { redirect: to.fullPath }
                      })
                    }
                  }
                }
              }
            })

            if (allowedToEnter) next()
          })
       ```

      - update `routes.js` tambahkan attribute meta, bagi route yang ingin di protect.
          ```js
              {
                path: '/user',
                beforeEnter: auth,
                name: 'user',
                meta: {
                  requiresAuth: true
                },
                component: () => import('pages/User.vue')
              }
          ```

  - navigation guard selesai. testing route, halaman page login, logout, dan user.


### Persistensi Store
  - setelah kita mengimplementasikan store dan navigation guard, selanjutnya kita akan menambahkan vuex-persistade untuk menyimpan store pada localstorage, jadi sesudah login
  jika halamannya di refres storenya gak hilang. `yarn add vuex-persistedstate`
   - update store/index.js
   ```js
   import createPersistedState from 'vuex-persistedstate'

    const Store = new Vuex.Store({
    plugins: [createPersistedState({
      paths: ['auth']
    })],
    ...
    })
   ```
   - parameter paths: hanya store auth saja yang disimpan pada local storage


