1. To install it, open a command prompt or terminal then type `npm install -g code-push-cli`
2. First you need to create a CodePush account `code-push register`
3. Register your app with the service :
    ```js
      code-push app add BuatinAPI-iOS ios cordova
      code-push app add BuatinAPI-Android android cordova

      buatin-api master ✗ 3d △ ◒ ⍉ ➜ code-push app add BuatinAPI-Android android cordova
          Successfully added the "BuatinAPI-Android" app, along with the following default deployments:
          ┌────────────┬──────────────────────────────────────────────────────────────────┐
          │ Name       │ Deployment Key                                                   │
          ├────────────┼──────────────────────────────────────────────────────────────────┤
          │ Production │ PKJoms7Dbt07p0iPAGo81_Ju1oqg6cbd8287-665d-4b3b-8629-8c035156e296 │
          ├────────────┼──────────────────────────────────────────────────────────────────┤
          │ Staging    │ bmHtcudrGfd5RB1taRrftJCLtuvt6cbd8287-665d-4b3b-8629-8c035156e296 │
          └────────────┴──────────────────────────────────────────────────────────────────┘

          Successfully added the "BuatinAPI-iOS" app, along with the following default deployments:
          ┌────────────┬──────────────────────────────────────────────────────────────────┐
          │ Name       │ Deployment Key                                                   │
          ├────────────┼──────────────────────────────────────────────────────────────────┤
          │ Production │ brBHYil5WUEp7spNzZhhe7QGJ2Bl6cbd8287-665d-4b3b-8629-8c035156e296 │
          ├────────────┼──────────────────────────────────────────────────────────────────┤
          │ Staging    │ oJLLqv8y6hUbJgIqPjElbPteuNKd6cbd8287-665d-4b3b-8629-8c035156e296 │
          └────────────┴──────────────────────────────────────────────────────────────────┘
    ```
4.Add CodePush plugin into your app : `cordova plugin add cordova-plugin-code-push@latest`
5. see development keys : `code-push deployment ls BuatinApi-Android -k`
6. update version
   ```js
    code-push release-cordova BuatinApi android -d Production
    code-push release-cordova BuatinApi android -d Production --description \"update versioning\" --mandatory true
   ```
