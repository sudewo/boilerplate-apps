/* eslint-disable no-throw-literal */
/**
 * Oauth object literal
 *
 * @author Sudewo <sudewo@mmakmur.co.id>
 * @license free
 */

export default {
  bus: null,
  data: {
    igCurrentState: null,
    loginProcess: false
  },
  init (params, bus) {
    Object.assign(this.data, params)
    this.bus = bus

    if (!('cordova' in window)) {
      // handling login, we detect url for login with browser.
      this.request('facebook', window.location.href)
      this.request('google', window.location.href)
      this.request('instagram', window.location.href)
    }
  },
  loginWith (type, options = { state: {} }) {
    // get oauth url
    let url = this.getUrl(type, options)

    // reset loginProcess
    this.data.loginProcess = false

    if ('cordova' in window) {
      let opt = this.data.clearCacheInappBrowserBeforeLogin ? `clearcache=yes,clearsessioncache=yes,cleardata=yes` : ``
      let ref = cordova.InAppBrowser.open(
        url,
        '_blank',
        `location=yes,usewkwebview=yes,hardwareback=yes,toolbar=yes,toolbarcolor=#000000,${opt}`
      )
      ref.show()
      ref.addEventListener('loadstart', params =>
        this.request(type, params.url, ref, 'loadstart')
      )
      ref.addEventListener('loadstop', params =>
        this.request(type, params.url, ref, 'loadstop')
      )
      ref.addEventListener('loaderror', params =>
        this.request(type, params.url, ref, 'loaderror')
      )
      ref.addEventListener('message', params =>
        console.log(params, ref, 'loadmessage')
      )

      ref.addEventListener('exit', params => {
        let getUrlState = localStorage.getItem('urlState')
        if (getUrlState) {
          getUrlState = JSON.parse(getUrlState)
        }
        localStorage.removeItem('urlState')
        console.log('get state : ', getUrlState)

        this.bus.$emit('OAUTH_RESPONSE', { status: 'error', message: 'close', errorMessage: 'close', state: getUrlState, response: null })
      })
    } else {
      location.href = url
    }
  },
  getUrl (type, options) {
    let url
    let { facebook, google, instagram } = this.data

    let defaultState = { 'type': type }
    if (options.state) {
      Object.assign(defaultState, options.state)
    }
    let urlState = JSON.stringify(defaultState)
    localStorage.setItem('urlState', urlState)

    if (type === 'facebook') {
      // when we want to switch acccount we must logout account, I set redirect with query string "?reloginFb=true"
      // when callback happened http://localhost:8080/login?reloginFb=true, we detect keyword "?reloginFb=true" line(154) then redirect to facebook login to login with different account.
      // make sure urlCallback same with redirect uri in (facebook developers -> setting -> basic -> Site URL), if not match logout redirect not work.

      if (options.relogin) {
        let urlCallback = facebook.redirect_uri + `?reloginFb=true`
        url = encodeURI(`http://m.facebook.com/logout.php?confirm=1&next=${urlCallback}&access_token=${options.token}`)
        console.log('URL RELOGIN : ', url)
        localStorage.setItem('fbState', urlState)
      } else {
        url = encodeURI(
          `https://graph.facebook.com/v4.0/oauth/authorize?client_id=${
            facebook.client_id
          }&state=${urlState}&redirect_uri=${
            facebook.redirect_uri
          }&auth_type=rerequest`
        )
      }
    } else if (type === 'google') {
      let scope = google.scope ? google.scope.join(' ') + ' https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email' : 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'

      url = encodeURI(
        `https://accounts.google.com/o/oauth2/v2/auth?client_id=${
          google.client_id
        }&redirect_uri=${
          google.redirect_uri
        }&response_type=code&scope=${scope}&include_granted_scopes=true&state=${urlState}&prompt=select_account` // select_account|consent|none
      )
    } else if (type === 'instagram') {
      localStorage.setItem('igState', urlState)
      // handling supicious login
      if (options.relogin) {
        // with this link user can switch account
        let igRedirectUri = encodeURIComponent(instagram.redirect_uri + `?state=${urlState}`)
        let uri = encodeURIComponent(`/oauth/authorize/?response_type=token&client_id=${instagram.client_id}&redirect_uri=${igRedirectUri}`)
        url = `https://instagram.com/accounts/logoutin/?force_classic_login=&next=${uri}`
      } else {
        // because instagram oauth is automatically login if they already login in the past. so we can't switch account.
        url = encodeURI(`https://api.instagram.com/oauth/authorize/?client_id=${instagram.client_id}&redirect_uri=${instagram.redirect_uri}&response_type=token&state=${urlState}`)
      }
    }
    return url
  },
  getState (urlCallbackState) {
    let getState = urlCallbackState.split('#')
    let state = { type: '' }

    if (getState.length > 0) {
      state = JSON.parse(getState[0])
    }
    return state
  },
  urlToObject (url) {
    return decodeURIComponent(url)
      .split('?')[1]
      .split('&')
      .map(p => p.split('='))
      .reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {})
  },
  validateRequest (type, url, loginProcess) {
    let matchUrl = url.match(/login\?state=|login\?code=|#access_token=|login\?error=/gi)
    if (!matchUrl) {
      return false
    }

    let urlCallback = this.urlToObject(url)
    let state = this.getState(urlCallback.state)

    switch (type) {
      case 'facebook':
        if (state.type !== 'facebook') return false
        // if (!url.match(/login\?code=/gi)) { return false }
        break
      case 'google':
        if (state.type !== 'google') return false
        // if (!url.match(/login\?state=/gi)) return false
        break
      case 'instagram':
        if (state.type !== 'instagram') return false
        // if (!url.match(/#access_token=/gi)) { return false }
        break
    }

    // if login is being process, dont process again.
    if (loginProcess) {
      return false
    }

    return true
  },
  async request (type, url, ref = '', event = '') {
    // console.log('REQUEST : ', type, url, event)

    // handling facebook switch account after logout.
    if (url === `${this.data[type].redirect_uri}?reloginFb=true`) {
      let getfbState = localStorage.getItem('fbState')
      if (getfbState) {
        getfbState = JSON.parse(getfbState)
      }
      this.loginWith('facebook', { state: getfbState })
    }

    // handling suppicious instagram oauth login
    if (event === 'loadstop' && url === 'https://www.instagram.com/' && !this.data.igCurrentState) {
      let getIgState = localStorage.getItem('igState')
      if (getIgState) {
        getIgState = JSON.parse(getIgState)
      }
      // console.log('ig state', getIgState)
      // console.log('igCurrentState', this.data.igCurrentState)
      // localStorage.removeItem('igState')
      this.loginWith('instagram', { state: getIgState })

      this.data.igCurrentState = 1
    }
    // if url doesnt contain 'code' or access token dont process it.
    if (!this.validateRequest(type, url, this.data.loginProcess)) {
      return false
    }

    // only for cordova
    if (event === 'loadstart') {
      var spinner = "<!DOCTYPE html><html><head><meta name='viewport' content='width=device-width,height=device-height,initial-scale=1'><style>.loader {position: absolute;    margin-left: -2em;    left: 50%;    top: 50%;    margin-top: -2em;    border: 5px solid #f3f3f3;    border-radius: 50%;    border-top: 5px solid #3498db;    width: 50px;    height: 50px;    -webkit-animation: spin 1.5s linear infinite;    animation: spin 1.5s linear infinite;}@-webkit-keyframes spin {  0% { -webkit-transform: rotate(0deg); } 100% { -webkit-transform: rotate(360deg); }}@keyframes spin {  0% { transform: rotate(0deg); }  100% { transform:rotate(360deg); }}</style></head><body><div class='loader'></div></body></html>"
      ref.executeScript({ code: '(function() {document.write("' + spinner + '");})()' })
    }

    // set global processLogin true
    this.data.loginProcess = true
    let method = `load${type.charAt(0).toUpperCase() + type.slice(1)}`

    let response = {
      status: '',
      type: type,
      data: ''
    }

    try {
      let result = await this[method](url)
      response.status = 'success'
      response.data = result

      this.bus.$emit('OAUTH_RESPONSE', response)
    } catch (e) {
      Object.assign(response, e)

      // use setTimout temporary.
      // I dont know why, when error redirect happened, sometimes emit not fire
      setTimeout(() => {
        this.bus.$emit('OAUTH_RESPONSE', response)
      }, 500)
    }

    localStorage.removeItem('fbState')
    localStorage.removeItem('igState')

    if (ref) ref.close()
  },
  async loadFacebook (url) {
    let { facebook } = this.data

    let urlCallback = this.urlToObject(url)
    let getState = this.getState(urlCallback.state)

    if (!urlCallback.code) {
      throw ({ status: 'error', message: `facebook code is ${urlCallback.code}`, query: urlCallback, url: url, state: getState, response: null })
    }

    let getAccessToken = await fetch(`https://graph.facebook.com/v4.0/oauth/access_token?client_id=${facebook.client_id}&redirect_uri=${facebook.redirect_uri}&client_secret=${facebook.client_secret}&code=${urlCallback.code}`)
    let responseToken = await getAccessToken.json()
    if (responseToken.error) throw ({ status: 'error', message: `get facebook access token error`, query: urlCallback, url: url, state: getState, response: responseToken })

    let getProfile = await fetch(`https://graph.facebook.com/v4.0/me?fields=id%2Cname%2Cemail%2Cpicture&access_token=${responseToken.access_token}`)
    let responseProfile = await getProfile.json()
    if (responseProfile.error) throw ({ status: 'error', message: `get facebook profile error`, query: urlCallback, url: url, state: getState, response: responseProfile })

    responseProfile.access_token = responseToken.access_token
    responseProfile.state = getState
    return responseProfile
  },
  async loadGoogle (url) {
    let { google } = this.data

    let urlCallback = this.urlToObject(url)
    let getState = this.getState(urlCallback.state)

    if (!urlCallback.code) {
      throw ({ status: 'error', message: `get google code error`, query: urlCallback, url: url, state: getState, response: null })
    }

    /*
    // GET GOOGLE TOKEN
    // GET GOOGLE TOKEN
    */
    let googleParams = {
      code: urlCallback.code,
      client_secret: google.client_secret,
      client_id: google.client_id,
      redirect_uri: google.redirect_uri,
      grant_type: 'authorization_code'
    }

    let getGoogleToken = await fetch(`https://www.googleapis.com/oauth2/v4/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(googleParams)
    })
    let responseToken = await getGoogleToken.json()
    // console.log('GOOGLE token RESPONSE', responseToken)
    if (responseToken.error) throw ({ status: 'error', message: `get google access token error`, query: urlCallback, url: url, state: getState, response: responseToken })

    /*
    // GET GOOGLE BASIC PROFILE
    // GET GOOGLE BASIC PROFILE
    */
    let getGoogleProfile = await fetch(`https://www.googleapis.com/userinfo/v2/me?access_token=${responseToken.access_token}`)
    let googleResponse = await getGoogleProfile.json()
    // console.log('GOOGLE RESPONSE', googleResponse)
    if (googleResponse.error) throw ({ status: 'error', message: `get google api error`, query: urlCallback, url: url, state: getState, response: googleResponse })

    googleResponse.state = this.getState(urlCallback.state)
    googleResponse.access_token = responseToken.access_token
    return googleResponse
  },
  async loadInstagram (url) {
    let urlCallback = this.urlToObject(url.replace(/#access_token/gi, '&access_token'))
    let getState = this.getState(urlCallback.state)

    if (!urlCallback.access_token) {
      throw ({ status: 'error', message: `get instagram access token error`, query: urlCallback, url: url, state: getState, response: null })
    }

    let igProfile = await fetch(`https://api.instagram.com/v1/users/self/?access_token=${urlCallback.access_token}`)
    let igResponse = await igProfile.json()
    if (igResponse.error) throw ({ status: 'error', message: `get instagram api error`, query: urlCallback, url: url, state: getState, response: igResponse })

    igResponse.data.access_token = urlCallback.access_token
    igResponse.data.state = getState
    return igResponse.data
  }
}
