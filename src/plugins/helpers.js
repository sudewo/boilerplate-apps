export const validation = (label, e) => {
  let errors = []
  e.split('|').map(v => {
    switch (v) {
      case 'required':
        errors.push(val => {
          if (!val || val.length === 0) {
            return `${label} is required`
          } else {
            return true
          }
        })
        break
      case 'email':
        let emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
        errors.push(val => emailRegex.test(val) || `Email is not valid`)
        break
    }
  })
  return errors
}

export const vuexSync = (module, state) => {
  let ob = {}
  for (let i in state) {
    ob[state[i]] = {
      get () {
        return this.$store.state[module][state[i]]
      },
      set (value) {
        return this.$store.commit(
          `${module}/SET_${state[i].toUpperCase()}`,
          value
        )
      }
    }
  }
  return ob
}

export const mutations = (module, state, getDefaultState) => {
  let ob = {}
  for (let i in state) {
    ob[`SET_${i.toUpperCase()}`] = (state, value) => {
      state[i] = value
    }
  }
  ob['SET_RESPONSE'] = (state, value) => {
    Object.assign(state, value)
  }
  ob['RESET'] = (state, value) => {
    Object.assign(state, getDefaultState())
  }
  return ob
}

export const requestApi = (
  { commit, dispatch },
  url,
  params,
  isAuth = false,
  method = 'POST'
) => {
  const FETCH_TIMEOUT = 20000
  let didTimeOut = false
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }

  let { auth } = JSON.parse(localStorage[process.env.VUEX_STORE_KEY])
  if (isAuth) {
    headers.authorization = `Bearer ${auth.token}`
  }

  let body
  if (params instanceof FormData) {
    delete headers['Content-Type']
    body = params
  } else {
    if (method !== 'GET') {
      body = JSON.stringify(params)
    } else {
      if (params) {
        let objToQuerySring = Object.keys(params)
          .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
          .join('&')
        url = `${url}?${objToQuerySring}`
      }
    }
  }

  if (process.env.PROD) {
    url = url.replace('/api', process.env.API_URL)
  }

  new Promise(function (resolve, reject) {
    const timeout = setTimeout(function () {
      didTimeOut = true
      reject('Request time out, please try again')
    }, FETCH_TIMEOUT)

    console.log(`### LOADING : ${url}`, params)
    commit('SET_RESPONSE', { __status: 'loading' })
    fetch(url, {
      method: method,
      headers,
      body
    })
      .then(i => {
        if (i.status === 401) {
          reject('unauthorized')
        }
        return i.json()
      })
      .then(i => {
        clearTimeout(timeout)
        if (!didTimeOut) {
          if (i.error && i.error.status) {
            reject(i.error.message)
          } else {
            resolve(i)
          }
        }
      })
      .catch(e => {
        // Rejection already happened with setTimeout
        if (didTimeOut) return
        reject(e)
      })
  })
    .then(function (i) {
      console.log('auth version', auth.__version, '===', i.__version)
      if (auth.__version) {
        if (auth.__version !== i.__version) {
          // commit('auth/SET_RESPONSE', { '__status': '' }, { root: true })
          commit(
            'auth/SET_RESPONSE',
            { __status: 'api_need_update' },
            { root: true }
          )
          return false
        }
      }
      // console.info(`### ${url} success  : `, params)
      console.info(`### RESPONSE ${i.__status.toUpperCase()} : `, i)
      commit('SET_RESPONSE', i)
    })
    .catch(function (e) {
      // console.error(`### ${url} error  : `, e)
      console.error(`### ERROR : `, e)
      if (e === 'unauthorized') {
        // commit('auth/SET_RESPONSE', { '__status': '' }, { root: true })
        commit(
          'auth/SET_RESPONSE',
          { __status: 'unauthorized' },
          { root: true }
        )
        return false
      }
      commit('SET_RESPONSE', { __status: 'error', __message: e, __error: e })
    })
}

const helpers = {
  validation,
  vuexSync,
  mutations,
  requestApi
}

export default {
  install (Vue, router) {
    Vue.prototype.$helpers = helpers
  }
}
