// import something here
import oauth from '../plugins/oauth'
// import oauth from 'vue-oauth-cordova'
// "async" is optional
export default async ({ Vue }) => {
  // something to do
  Vue.use(oauth, {
    clearCacheInappBrowserBeforeLogin: false,
    facebook: {
      client_id: process.env.FACEBOOK_CLIENT_ID,
      client_secret: process.env.FACEBOOK_CLIENT_SECRET,
      redirect_uri: process.env.FACEBOOK_REDIRECT_URI
    },
    google: {
      client_id: process.env.GOOGLE_CLIENT_ID,
      client_secret: process.env.GOOGLE_CLIENT_SECRET,
      redirect_uri: process.env.GOOGLE_REDIRECT_URI,
      // others : 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email',
      scope: ['https://www.googleapis.com/auth/youtube.readonly']
    },
    instagram: {
      client_id: process.env.IG_CLIENT_ID,
      client_secret: process.env.IG_CLIENT_SECRET,
      redirect_uri: process.env.IG_REDIRECT_URI
    }
  })
}
