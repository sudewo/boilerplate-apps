/* eslint-disable no-useless-escape */
/* eslint-disable no-template-curly-in-string */
// import something here
import helpers, {
  validation,
  requestApi,
  vuexSync,
  mutations
} from '../plugins/helpers'
import VueCurrencyInput from 'vue-currency-input'
import filterTable from '../components/filter.vue'
import JsonExcel from 'vue-json-excel'
import download from 'downloadjs'
import Chart from 'vue2-frappe'

export { validation, requestApi, vuexSync, mutations }

export default async ({ Vue, router, store }) => {
  Vue.use(helpers, router)
  Vue.use(Chart)

  Vue.component('filter-table', filterTable)
  Vue.component('downloadExcel', JsonExcel)

  const pluginOptions = {
    globalOptions: {
      currency: undefined,
      locale: undefined,
      // autoDecimalMode: false,
      decimalLength: 0,
      distractionFree: {
        hideNegligibleDecimalDigits: false,
        hideCurrencySymbol: true,
        hideGroupingSymbol: false
      }
      // min: null,
      // max: null
    }
  }
  Vue.use(VueCurrencyInput, pluginOptions)

  Vue.prototype.$validation = validation
  Vue.mixin({
    data () {
      return {
        userRoleOptions: [
          { label: 'Member', value: 0 },
          { label: 'Admin', value: 2 },
          { label: 'Superuser', value: 4 }
        ],
        filterSearchQTable: '',
        API_URL: process.env.API_URL,
        IMG_URL: process.env.IMG_URL,
        removedFile: [],
        notify: {
          showLoading: true,
          enable: false,
          success: false,
          error: true,
          failed: true,
          redirect: null
        }
      }
    },
    created () {},
    watch: {
      __status (status) {
        if (!status) return false
        if (!this.notify.enable) {
          return false
        }

        if (status === 'loading') {
          if (this.notify.showLoading) {
            this.$q.loading.show()
          }
          return false
        }

        console.log('statttus ', status)
        if (this.notify.showLoading) {
          this.$q.loading.hide()
        }

        let color,
          showNotify = true
        if (status.indexOf('success') !== -1) {
          color = 'green-4'
          if (!this.notify.success) {
            showNotify = false
          }
        } else if (status.indexOf('failed') !== -1) {
          color = 'orange'
          if (!this.notify.failed) {
            showNotify = false
          }
        } else if (status.indexOf('error') !== -1) {
          color = 'red-4'
          if (!this.notify.error) {
            showNotify = false
          }
        }

        if (showNotify) {
          var matcher = new RegExp(/success_load|success_edit/, 'g')
          if (!matcher.test(status)) {
            this.$q.notify({
              color,
              textColor: 'white',
              icon: 'fas fa-check-circle',
              message: this.__message,
              html: true
            })
          }
        }

        if (this.notify.redirect) {
          this.notify.redirect(status)
        }
      }
    },
    methods: {
      getImgUrl (url) {
        if (process.env.PROD) {
          url = url.replace(`api`, process.env.IMG_URL)
        }
        return url
      },
      getUrl (url) {
        if (process.env.PROD) {
          url = url.replace(`api`, process.env.API_URL)
        }
        return url
      },
      base64ToBlob (data, mime) {
        let base64 = window.btoa(window.unescape(encodeURIComponent(data)))
        let bstr = atob(base64)
        let n = bstr.length
        let u8arr = new Uint8ClampedArray(n)
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n)
        }
        return new Blob([u8arr], { type: mime })
      },
      convertToCSV (objArray) {
        var array =
          typeof objArray !== 'object' ? JSON.parse(objArray) : objArray

        let keys = {}
        for (let key in array[0]) {
          keys[key] = key
        }

        var str = ''
        str += Object.keys(keys).join(',') + '\r\n'

        for (var i = 0; i < array.length; i++) {
          var line = ''
          for (var index in array[i]) {
            if (line !== '') line += ','

            line +=
              typeof array[i][index] === 'object'
                ? JSON.stringify(array[i][index])
                : array[i][index]
          }

          str += line + '\r\n'
        }
        return str
      },
      parseExtraData (extraData, format) {
        let parseData = ''
        if (Array.isArray(extraData)) {
          for (var i = 0; i < extraData.length; i++) {
            parseData += format.replace('${data}', extraData[i])
          }
        } else {
          parseData += format.replace('${data}', extraData)
        }
        return parseData
      },
      async export (data, filename, mime) {
        let blob = this.base64ToBlob(data, mime)
        download(blob, filename, mime)
      },
      jsonToCSV (data) {
        var csvData = []
        // Header
        if (this.title != null) {
          csvData.push(this.parseExtraData(this.title, '${data}\r\n'))
        }
        // Fields
        for (let key in data[0]) {
          csvData.push(key)
          csvData.push(',')
        }
        csvData.pop()
        csvData.push('\r\n')
        // Data
        data.map(function (item) {
          for (let key in item) {
            let escapedCSV = '="' + item[key] + '"' // cast Numbers to string
            if (escapedCSV.match(/[,"\n]/)) {
              escapedCSV = '"' + escapedCSV.replace(/\"/g, '""') + '"'
            }
            csvData.push(escapedCSV)
            csvData.push(',')
          }
          csvData.pop()
          csvData.push('\r\n')
        })
        // Footer
        if (this.footer != null) {
          csvData.push(this.parseExtraData(this.footer, '${data}\r\n'))
        }
        return csvData.join('')
      },
      getProcessedJson (data, header) {
        let keys = this.getKeys(data, header)
        let newData = []
        let _self = this
        data.map(function (item, index) {
          let newItem = {}
          for (let label in keys) {
            let property = keys[label]
            newItem[label] = _self.getValue(property, item)
          }
          newData.push(newItem)
        })
        return newData
      },
      getKeys (data, header) {
        if (header) {
          return header
        }
        let keys = {}
        for (let key in data[0]) {
          keys[key] = key
        }
        return keys
      },
      getValue (key, item) {
        const field = typeof key !== 'object' ? key : key.field
        let indexes = typeof field !== 'string' ? [] : field.split('.')
        let value = this.defaultValue

        if (!field) value = item
        else if (indexes.length > 1) {
          value = this.getValueFromNestedItem(item, indexes)
        } else value = this.parseValue(item[field])

        if (typeof value === 'object') {
          let gk = Object.keys(value)
          value = value[gk[1]]
        }

        value = typeof value === 'undefined' ? '' : value
        return value
      },
      parseValue (value) {
        return value || value === 0 || typeof value === 'boolean'
          ? value
          : this.defaultValue
      },
      getValueFromNestedItem (item, indexes) {
        let nestedItem = item
        for (let index of indexes) {
          if (nestedItem) {
            nestedItem = nestedItem[index]
          }
        }
        return this.parseValue(nestedItem)
      },
      async fetchData (tbl, search) {
        let fetchTbl = tbl.replace(/s$/gi, '').replace(/_/gi, '-')
        let url = `${process.env.API_URL}/admin/${fetchTbl}?show=all&report=true&search=${search}`
        // if (process.env.PROD) {
        //   url = url.replace('/api', process.env.API_URL)
        // }
        try {
          let response = await fetch(url, {
            method: 'get',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              authorization: `Bearer ${this.$store.state.auth.token}`
            }
          })
          let result = await response.json()
          return result[tbl]
        } catch (e) {
          alert(e)
        }
      },
      async exports (tbl, source, search = null) {
        let data = this[tbl].data

        if (source === 'all') {
          this.$q.loading.show()
          data = await this.fetchData(tbl, search)
          this.$q.loading.hide()
        }

        let timestamp = Math.floor(Date.now() / 1000)
        let filename = `${tbl}_${timestamp}.csv`
        let json = this.getProcessedJson(data)
        let fileCsv = this.jsonToCSV(json)
        let mime = 'text/csv'

        if (!this.$q.platform.is.cordova) {
          let blob = this.base64ToBlob(fileCsv, mime)
          download(blob, filename, mime)
        } else {
          let downloadDir = 'file:///storage/emulated/0/Download'
          // let downloadDir = cordova.file.applicationDirectory
          window.resolveLocalFileSystemURL(downloadDir, dir => {
            console.log('got main dir', dir)
            dir.getFile(
              filename,
              {
                create: true
              },
              file => {
                file.createWriter(
                  fileWriter => {
                    fileWriter.seek(fileWriter.length)
                    var blob = new Blob([fileCsv], {
                      type: 'text/csv'
                    })
                    fileWriter.write(blob)
                    // console.log('GET FILE ', file.nativeURL)
                    this.$q.notify(
                      `${file.nativeURL} file saved in Download folder`
                    )
                    cordova.plugins.fileOpener2.open(file.nativeURL, mime, {
                      error: function () {},
                      success: function () {}
                    })
                  },
                  fail => {
                    console.log('failed ')
                  }
                )
              }
            )
          })
        }
      },
      onChangeFile (files, type, store) {
        let gettype = this[type] !== 'null' && this[type] ? this[type] : []
        this.$store.commit(
          `${store}/SET_${type.toUpperCase()}`,
          files.concat(gettype).filter(i => i)
        )
      },
      removeFile (file, type, store) {
        let removeFile = this[type].filter(i =>
          i instanceof File ? i.name !== file.name : i !== file
        )
        console.log(
          'RREMOVE FIILE',
          `${store}/SET_${type.toUpperCase()}`,
          removeFile
        )
        this.removedFile.push(file)
        this.$store.commit(`${store}/SET_${type.toUpperCase()}`, removeFile)
      },
      /** clean empty object */
      clean (obj) {
        const newObj = {}
        Object.keys(obj).forEach(key => {
          if (obj[key] && typeof obj[key] === 'object') {
            newObj[key] = this.clean(obj[key]) // recurse
          } else if (obj[key] !== '' && obj[key] !== null) {
            newObj[key] = obj[key] // copy value
          }
        })
        return newObj
      },
      formatCell (col) {
        if (col.name === 'incentive') {
          return this.formatMoney(col.value, 0)
        } else if (col.type === 'date') {
          return new Date(col.value).toLocaleString('id-ID', {
            day: 'numeric',
            month: 'short',
            weekday: 'long',
            year: 'numeric'
          })
        } else if (String(col.value).match(/(.png|.jpeg|.jpg)/gi)) {
          return `<img src="api/uploads/${col.value}" style="object-fit:contain;width:50px;"/>`
        } else {
          return col.value
        }

        // else if (col.name.indexOf('status') !== -1) {
        //   return `<span class="${col.value === 0 ? 'text-red' : 'text-green'} bg-grey-1 q-pa-xs block">${['inactive', 'active'][col.value]}</span>`
        // }
      },
      filterDropdown (val, update, type, loc, label) {
        if (val === '') {
          update(() => {
            this[type] = this[loc]
          })
          return
        }

        update(() => {
          const needle = val.toLowerCase()
          this[type] = this[loc].filter(v => {
            return v[label].toLowerCase().indexOf(needle) > -1
          })
        })
      },
      formatMoney (amount, decimalCount = 2, decimal = '.', thousands = ',') {
        try {
          decimalCount = Math.abs(decimalCount)
          decimalCount = isNaN(decimalCount) ? 2 : decimalCount

          const negativeSign = amount < 0 ? '-' : ''

          let i = parseInt(
            (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
          ).toString()
          let j = i.length > 3 ? i.length % 3 : 0

          return (
            negativeSign +
            (j ? i.substr(0, j) + thousands : '') +
            i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
            (decimalCount
              ? decimal +
                Math.abs(amount - i)
                  .toFixed(decimalCount)
                  .slice(2)
              : '')
          )
        } catch (e) {
          console.log(e)
        }
      },
      convertInputToNumber (input) {
        if (typeof input === 'number') {
          return input
        }
        // input = String(input)
        const thousandFixed = input
          .replace(/(kr|\$|£|€)/g, '') // getting rid of currency
          .trim()
          .replace(/(.+)[.,](\d+)$/g, '$1x$2') // stripping number into $1: integer and $2: decimal part and putting it together with x as decimal point
          .replace(/[.,]/g, '') // getting rid of . AND ,
          .replace('x', '.') // replacing x with .

        return parseFloat(thousandFixed)
      },
      putSpaceInCamelCase (val) {
        if (val) {
          let rex = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])/g
          return val.replace(rex, '$1$4 $2$3$5').replace('Form', '')
        } else {
          return ''
        }
      }
    }
  })
}
