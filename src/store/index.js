import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import config from './config'
import auth from './auth'
import car from './car'

import area from './area'
import branch from './branch'
import user from './user'
import token from './token'
import program from './program'
import programOrder from './programOrder'
import programPackage from './programPackage'
import notification from './notification'
import programReport from './programReport'
Vue.use(Vuex)
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */
export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [
      createPersistedState({
        key: process.env.VUEX_STORE_KEY,
        paths: ['auth', 'config']
      })
    ],
    modules: {
      programReport,
      notification,
      programPackage,
      programOrder,
      program,
      token,
      user,
      branch,
      area,
      auth,
      config,
      car
    },
    strict: true
    // strict: false
  })

  return Store
}
