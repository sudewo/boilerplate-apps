
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    area_name: null,
    area_status: 1,
    id: null,
    created_at: null,
    updated_at: null,
    areas: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('area', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/area`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/area/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/area/${id}`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/area`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/area/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
