import { mutations, requestApi } from 'boot/helpers'

export const defaultState = {
  // default response
  __status: '',
  __message: '',
  __error: '',

  // custom response
  created_at: null,
  updated_at: null,
  cars: []
}

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('car', defaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/car`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/car/${params.id}/edit`,
        params,
        false,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/car/${params.id}`,
        params,
        false,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/car`, params, false)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/olx/destroy/${params.id}`,
        params,
        false,
        'DELETE'
      )
    },
    reset ({ state, commit, dispatch }, params) {
      params.forEach(i => {
        commit(`SET_${i.toUpperCase()}`, '')
      })
      commit(`SET_RESPONSE`, {
        __status: '',
        __error: '',
        __message: ''
      })
    }
  },
  getters: {}
}
