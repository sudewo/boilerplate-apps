import { mutations, requestApi } from 'boot/helpers'

export const defaultState = {
  // default response
  '__status': '',
  '__message': '',
  '__error': '',
  '__version': '',
  '__validation': '',

  // custom reseponse
  'id': '',
  'dealer_name': '',
  'username': '',
  'email': '',
  'password': '',
  'created_at': '',
  'updated_at': '',
  'type': '',
  'token': '',
  'user_role': 0,
  'refreshToken': '',
  'loginWith': '',
  'resetPasswordStep': '', // send_code_verification, code_verified
  'facebook': {
    'email': '',
    'id': '',
    'name': '',
    'picture': ''
  },
  'google': {
    'email': '',
    'id': '',
    'name': '',
    'picture': ''
  },
  'instagram': {
    'username': '',
    'id': '',
    'name': '',
    'picture': ''
  },
  // profile
  gender: '',
  bio: '',
  phone: '',
  birthdate: '',
  avatar: '',
  cover: '',
  country_id: '',
  state_id: '',
  city_id: '',
  country: {
    'id': null,
    'name': null
  },
  state: {
    'id': null,
    'name': null
  },
  city: {
    'id': null,
    'name': null
  }
}

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('auth', defaultState)
  },
  actions: {
    login ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/login`, params)
    },
    loginWithSocialMedia ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/login-with-social-media`, params)
    },
    saveAuthStore ({ state, commit, dispatch }, params) {
      commit(params)
    },
    completeAccount ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/complete-account`, params)
    },

    emailVerification ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/email-verification`, params)
    },
    codeVerification ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/code-verification`, params)
    },
    resetPassword ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/reset-password`, params)
    },

    changeAvatar ({ state, commit, dispatch }, params) {
      var data = new FormData()
      data.append('file', params.target.files[0])
      data.append('id', state.id)
      data.append('type', 'avatar')
      requestApi({ state, commit, dispatch }, `/api/auth/change-avatar`, data, true)
    },
    changeCover ({ state, commit, dispatch }, params) {
      var data = new FormData()
      data.append('file', params.target.files[0])
      data.append('id', state.id)
      data.append('type', 'cover')
      requestApi({ state, commit, dispatch }, `/api/auth/change-avatar`, data, true)
    },
    updateProfile ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/auth/update-profile`, params, true)
    },

    logout ({ state, commit, dispatch }) {
      commit('SET_RESPONSE', defaultState)
    },
    reset ({ state, commit, dispatch }, params) {
      params.forEach((i) => {
        commit(`SET_${i.toUpperCase()}`, '')
      })
    }
  },
  getters: {
    isLogin (state) {
      return state.token
    },
    location (state) {
      return state.country.name + ' ' + state.state.name + ' ' + state.city.name
    }
  }
}
