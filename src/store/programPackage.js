
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    program_id: null,
    program_package_name: null,
    program_package_type: 1,
    program_package_reward_money: null,
    program_package_reward_trip: null,
    program_package_target: null,
    program_package_status: 1,
    id: null,
    created_at: null,
    updated_at: null,
    program_packages: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('program_package', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-package`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-package/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-package/${id}`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/program-package`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-package/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
