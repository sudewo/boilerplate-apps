
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    area_id: null,
    branch_name: null,
    branch_status: 1,
    id: null,
    created_at: null,
    updated_at: null,
    branches: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('branch', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/branch`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/branch/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/branch/${id}`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/branch`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/branch/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
