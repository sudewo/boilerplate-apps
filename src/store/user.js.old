import { mutations, requestApi } from 'boot/helpers'

export const defaultState = {
  // default response
  __status: '',
  __message: '',
  __error: '',

  // custom response
  username: '',
  email: '',
  password: '',
  code: '',
  status: '',
  gender: '',
  birthdate: '',
  phone: '',
  bio: '',
  avatar: '',
  cover: '',
  role: '',
  country_id: '',
  state_id: '',
  city_id: '',
  location_detail: '',
  created_at: null,
  updated_at: null,
  users: []
}

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('user', defaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${params.id}/edit`,
        params,
        false,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${params.id}`,
        params,
        false,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/user`, params, false)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${params.id}`,
        params,
        false,
        'DELETE'
      )
    },
    reset ({ state, commit, dispatch }, params) {
      params.forEach(i => {
        commit(`SET_${i.toUpperCase()}`, '')
      })
      commit(`SET_RESPONSE`, {
        __status: '',
        __error: '',
        __message: ''
      })
    }
  },
  getters: {}
}
