
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    user_id: null,
    program_id: null,
    program_package_id: null,
    program_order_id: null,
    program_report_date: null,
    program_report_total: null,
    program_report_fid: null,
    program_report_status: 1,
    id: null,
    created_at: null,
    updated_at: null,
    program_reports: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('program_report', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report/${id}`,
        params,
        true,
        'PUT'
      )
    },
    updateReport ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report/update-report`,
        params,
        true
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report`,
        params,
        true
      )
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program-report/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
