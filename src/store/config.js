
import { mutations } from 'boot/helpers'
// import Vue from 'vue'

const getDefaultState = () => {
  return {
    // invisibleColumn
    invisibleColumns: {}
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('config', defaultState, getDefaultState),
    update (state, params) {
      let newObj = {}
      newObj[params.table] = params.data
      state.invisibleColumns = Object.assign({}, state.invisibleColumns, newObj)
    },
    updateVisibility (state, params) {
      if (state.invisibleColumns[params.table].invisibleColumns.includes(params.data)) {
        state.invisibleColumns[params.table].invisibleColumns.splice(
          state.invisibleColumns[params.table].invisibleColumns.indexOf(
            params.data
          ), 1
        )
      } else {
        state.invisibleColumns[params.table].invisibleColumns.push(params.data)
      }
    }

  },
  actions: {
    store ({ state, commit, dispatch }, params) {
      console.log('params ', params)
      commit('update', params)
    },
    update ({ state, commit, dispatch }, params) {
      commit('updateVisibility', params)
    }
  },
  getters: {}
}
