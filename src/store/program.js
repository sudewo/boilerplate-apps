
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    program_name: null,
    area_id: null,
    program_period_start: new Date().toISOString().substring(0, 10),
    program_period_finish: new Date().toISOString().substring(0, 10),
    program_description: null,
    program_term_condition: null,
    program_growth: null,
    program_roa: null,
    program_fid: null,
    program_status: 1,
    program_type: null,
    program_semester: null,
    program_dealer_type: null,
    program_picture: null,
    id: null,
    created_at: null,
    updated_at: null,
    programs: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('program', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program/${id}`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/program`, params, true)
    },
    daftar ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/program/daftar`, params, true)
    },
    upgrade ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/program/upgrade`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/program/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
