
import { mutations, requestApi } from 'boot/helpers'

const getDefaultState = () => {
  return {
    // default response
    __status: null,
    __message: null,
    __error: null,
    __version: null,
    __validation: null,

    // custom response
    area_id: null,
    branch_id: null,
    dealer_type: null,
    username: null,
    dealer_name: null,
    password: null,
    email: null,
    avatar: null,
    user_status: 1,
    position_status: 1,
    user_role: 0,
    parent: 0,
    code: null,
    group_code: null,
    gender: 0,
    business_type: 0,
    birthdate: null,
    join_date: null,
    phone: null,
    bio: null,
    cover: null,
    address: null,
    id: null,
    created_at: null,
    updated_at: null,
    users: []
  }
}

// initial state
const defaultState = getDefaultState()

export default {
  namespaced: true,
  state: defaultState,
  mutations: {
    ...mutations('user', defaultState, getDefaultState)
  },
  actions: {
    index ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user`,
        params,
        true,
        'GET'
      )
    },
    edit ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${params.id}/edit`,
        params,
        true,
        'GET'
      )
    },
    update ({ state, commit, dispatch }, params) {
      let id = params instanceof FormData ? params.get('id') : params.id
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${id}`,
        params,
        true,
        'PUT'
      )
    },
    store ({ state, commit, dispatch }, params) {
      requestApi({ state, commit, dispatch }, `/api/admin/user`, params, true)
    },
    destroy ({ state, commit, dispatch }, params) {
      requestApi(
        { state, commit, dispatch },
        `/api/admin/user/${params.id}`,
        params,
        true,
        'DELETE'
      )
    }
  },
  getters: {}
}
