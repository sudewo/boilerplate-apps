const pageRoute = [
  {
    path: '/',
    name: 'AdminIndex',
    label: 'Dashboard',
    meta: {
      title: 'Dashboard'
    },
    component: () => import('pages/admin/Index.vue')
  },
  {
    path: '/trenharga',
    name: 'Trenharga',
    label: 'Trenharga',
    meta: {
      title: 'Trenharga'
    },
    component: () => import('pages/admin/Trenharga.vue')
  },

  {
    path: 'area',
    name: 'Area',
    label: 'Area',
    meta: {
      title: 'Area'
    },
    component: () => import('pages/admin/Area.vue')
  },
  {
    path: 'area/form/:id?',
    name: 'AreaForm',
    label: 'Update Area',
    meta: {
      title: 'Update Area'
    },
    component: () => import('pages/admin/AreaForm.vue')
  },

  {
    path: 'branch',
    name: 'Branch',
    label: 'Branch',
    meta: {
      title: 'Branch'
    },
    component: () => import('pages/admin/Branch.vue')
  },
  {
    path: 'branch/form/:id?',
    name: 'BranchForm',
    label: 'Update Branch',
    meta: {
      title: 'Update Branch'
    },
    component: () => import('pages/admin/BranchForm.vue')
  },

  {
    path: 'user',
    name: 'User',
    label: 'User',
    meta: {
      title: 'User'
    },
    component: () => import('pages/admin/User.vue')
  },
  {
    path: 'user/form/:id?',
    name: 'UserForm',
    label: 'Update User',
    meta: {
      title: 'Update User'
    },
    component: () => import('pages/admin/UserForm.vue')
  },

  {
    path: 'token',
    name: 'Token',
    label: 'Token',
    meta: {
      title: 'Token'
    },
    component: () => import('pages/admin/Token.vue')
  },
  {
    path: 'token/form/:id?',
    name: 'TokenForm',
    label: 'Update Token',
    meta: {
      title: 'Update Token'
    },
    component: () => import('pages/admin/TokenForm.vue')
  },

  {
    path: 'program',
    name: 'Program',
    label: 'Program',
    meta: {
      title: 'Program'
    },
    component: () => import('pages/admin/Program.vue')
  },
  {
    path: 'program/form/:id?',
    name: 'ProgramForm',
    label: 'Update Program',
    meta: {
      title: 'Update Program'
    },
    component: () => import('pages/admin/ProgramForm.vue')
  },

  {
    path: 'program-order',
    name: 'ProgramOrder',
    label: 'ProgramOrder',
    meta: {
      title: 'ProgramOrder'
    },
    component: () => import('pages/admin/ProgramOrder.vue')
  },
  {
    path: 'program-order/form/:id?',
    name: 'ProgramOrderForm',
    label: 'Update ProgramOrder',
    meta: {
      title: 'Update ProgramOrder'
    },
    component: () => import('pages/admin/ProgramOrderForm.vue')
  },
  {
    path: 'program-order/form/:id/report',
    name: 'ProgramOrderFormReport',
    label: 'Update Report',
    meta: {
      title: 'Update Report'
    },
    component: () => import('pages/admin/ProgramOrderFormReport.vue')
  },

  {
    path: 'program-package',
    name: 'ProgramPackage',
    label: 'ProgramPackage',
    meta: {
      title: 'ProgramPackage'
    },
    component: () => import('pages/admin/ProgramPackage.vue')
  }, {
    path: 'program-package/form/:id?',
    name: 'ProgramPackageForm',
    label: 'Update ProgramPackage',
    meta: {
      title: 'Update ProgramPackage'
    },
    component: () => import('pages/admin/ProgramPackageForm.vue')
  },

  {
    path: 'notification',
    name: 'Notification',
    label: 'Notification',
    meta: {
      title: 'Notification'
    },
    component: () => import('pages/admin/Notification.vue')
  }, {
    path: 'notification/form/:id?',
    name: 'NotificationForm',
    label: 'Update Notification',
    meta: {
      title: 'Update Notification'
    },
    component: () => import('pages/admin/NotificationForm.vue')
  },

  {
    path: 'program-report',
    name: 'ProgramReport',
    label: 'ProgramReport',
    meta: {
      title: 'ProgramReport'
    },
    component: () => import('pages/admin/ProgramReport.vue')
  }, {
    path: 'program-report/form/:id?',
    name: 'ProgramReportForm',
    label: 'Update ProgramReport',
    meta: {
      title: 'Update ProgramReport'
    },
    component: () => import('pages/admin/ProgramReportForm.vue')
  }

]

const adminRoutes = [
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    meta: {
      requiresAuth: true
    },
    children: pageRoute
  },
  {
    path: '/admin/login',
    component: () => import('layouts/BlankLayout.vue'),
    meta: {
      requiresAuth: false
    },
    children: [
      {
        path: '/',
        name: 'AdminLogin',
        label: 'AdminLogin',
        component: () => import('pages/admin/Login.vue')
      }
    ]
  }
]

export default adminRoutes
