import adminRoutes from './adminRoutes'
let frontendRoutes = [
  /** DASHBOARD LAYOUT NEED AUTHENTICATION */
  {
    path: '/',
    component: () => import('layouts/DrawerLayout.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import('pages/Index.vue')
      },
      {
        path: '/daftar-program',
        name: 'DaftarProgram',
        component: () => import('pages/DaftarProgram.vue')
      },
      {
        path: '/my-program',
        name: 'MyProgram',
        component: () => import('pages/MyProgram.vue')
      },
      {
        path: '/thank-you',
        name: 'ThankYou',
        component: () => import('pages/ThankYou.vue')
      },
      {
        path: '/notifikasi',
        name: 'Notifikasi',
        component: () => import('pages/Notification.vue')
      }
    ]
  },
  {
    path: '/:type/form/:id?',
    component: () => import('layouts/BlankLayout.vue'),
    meta: {
      requiresAuth: false
    },
    children: []
  },
  /** SINGLE LAYOUT USING AUTH */
  {
    path: '/',
    component: () => import('layouts/BlankLayout.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '/profile',
        name: 'Profile',
        label: 'Profile',
        component: () => import('pages/Profile.vue')
      },
      {
        path: '/edit-profile',
        name: 'EditProfile',
        label: 'Edit Profile',
        component: () => import('pages/EditProfile.vue')
      },
      {
        path: '/location',
        name: 'UserLocation',
        label: 'Location',
        component: () => import('pages/Location.vue')
      }
    ]
  },
  /** SINGLE LAYOUT WITHOUT AUTH */
  {
    path: '/',
    component: () => import('layouts/BlankLayout.vue'),
    children: [
      {
        path: '/login',
        name: 'Login',
        component: () => import('pages/Login.vue')
      },
      {
        path: '/login-with-email',
        name: 'LoginWithEmail',
        component: () => import('pages/LoginWithEmail.vue')
      },
      {
        path: '/login-with-phone',
        name: 'LoginWithPhone',
        component: () => import('pages/LoginWithPhone.vue')
      },
      {
        path: '/complete-account',
        name: 'CompleteAccount',
        component: () => import('pages/CompleteAccount.vue')
      },
      {
        path: '/user-verification',
        name: 'UserVerification',
        component: () => import('pages/UserVerification.vue')
      },
      {
        path: '/email-verification',
        name: 'EmailVerification',
        component: () => import('pages/EmailVerification.vue')
      },
      {
        path: '/code-verification',
        name: 'CodeVerification',
        component: () => import('pages/CodeVerification.vue')
      },
      {
        path: '/reset-password',
        name: 'ResetPassword',
        component: () => import('pages/ResetPassword.vue')
      }
    ]
  }
]

let routes = [...frontendRoutes, ...adminRoutes]
// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
