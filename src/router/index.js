import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function ({ store, ssrContext }) {
  const Router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve({ x: 0, y: 0 })
        }, 500)
      })
    },
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    let allowedToEnter = true
    const isLoggedIn = store.getters['auth/isLogin']

    to.matched.some(record => {
      // if (!isLoggedIn && record.name === 'user') {
      //   next({
      //     path: '/login',
      //     replace: true
      //   })
      // }

      if ('meta' in record) {
        if ('requiresAuth' in record.meta) {
          if (record.meta.requiresAuth) {
            // this route requires auth, check if user is logged in
            // if not, redirect to login page.
            if (!isLoggedIn) {
              // User is not logged in, redirect to signin page
              allowedToEnter = false

              // when user not login access path '/' just redirect to login screen without "redirect parameter"
              // console.log('toxx : ', to.path)
              // let redirectTo = (to.path).indexOf('admin') !== -1 ? '/admin/login' : '/login'
              let redirectTo =
                to.path.indexOf('admin') !== -1
                  ? '/admin/login'
                  : '/login'
              if (to.fullPath === '/') {
                next({ path: redirectTo })
                return false
              }

              next({
                path: redirectTo,
                replace: true,
                // redirect back to original path when done signing in
                query: { redirect: to.fullPath }
              })
            }
          }
        }
      }
    })

    if (allowedToEnter) next()
  })

  return Router
}
